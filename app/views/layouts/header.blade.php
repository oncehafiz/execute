<body class="home-body">
<div class="shoji" id="shoji">
<div class="shoji__fixtures" id="shoji-fixtures"></div>
<div class="shoji__door">
<div class="page shoji__washi">

<div class="main-nav-screen" id="main-nav-screen"></div>
<header class="home-header" role="banner">

<div class="home-header__intro">
    <div class="container">
        <a class="g-logo-medium home-header__logo" href="{{ URL::to('/') }}">execYoution</a>
            <span class="home-header__motto">
#Exec<span class="home-header__motto__asterisk">u</span>te Better. Simple.</span>
    </div>
</div>


<div class="banner">
<div class="banner__container">
<a class="g-menu banner__main-nav-toggle" href="#main-nav" id="main-nav-toggle">Navigation</a>
<a class="g-logo-small banner__logo" href="{{ URL::to('/') }}">execYoution</a>
            
            <span class="banner__account">
                <nav class="nav nav--main banner__account__nav">
                    <ul class="nav__menu">
                        <li class="nav__item nav__item--disabled">
                            <a class="nav__link nav__link--placeheld banner__account__link banner__account__link--profile" href="#dashboard" id="account-link">
                                <div class="banner__account__image" id="account-image"></div>
                                <span class="badge banner__account__badge" id="account-badge">1</span>
                            </a>
                            <span class="nav__link nav__link--placeholder"></span>
                            <div class="nav__folder"></div>
                        </li>
                    </ul>
                </nav>
                
                
                <div class="dropdown exec-dropdown">
                @if (Sentry::check())
                <a class="banner__account__link banner__account__link--login" href="{{ URL::to('appUser') }}">{{ Session::get('username') }}</a>
                <a class="banner__account__link banner__account__link--signup" href="{{ URL::to('logout') }}">{{trans('pages.logout')}}</a>
                @else
                <a class="banner__account__link banner__account__link--login dropdown-toggle" data-toggle="dropdown" href="#" >Log in</a>
                <a class="banner__account__link banner__account__link--signup dropdown-toggle" data-toggle="dropdown" href="#">Sign up</a>

                @endif                    
                    <div class="dropdown-menu" style="padding:17px;">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h2 class="auth-prompt__header__title">New Customer</h2>
                                <p>New to Execyoution? Create an account to get started today.</p>
                                <button type="button" class="button button--dark" data-toggle="modal" data-target="#myModal">Create My Account</button>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                {{ Form::open(array('action' => 'SessionController@store', 'class' => 'form exec-form', 'id' => 'loginForm')) }}
                                <h2 class="auth-prompt__header__title">Sign In</h2>
                                <span class="errors"></span>
                                <div class="m3 {{ ($errors->has('email')) ? 'has-error' : '' }}">
                                    {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Username', 'autofocus')) }}
                                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                </div>

                                <div class="m3 {{ ($errors->has('password')) ? 'has-error' : '' }}">
                                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('admin.pword')))}}
                                    {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                                </div>

                                <a class="forgot-pass" data-toggle="modal" data-target="#myModal" href="#">Forgot your password?</a>
                                <button type="button" class="button button--dark" onclick="modelBox.login_request('loginForm')">Login</button>
                                {{ Form::close() }}

                            </div>

                        </div>
                    </div>
                </div>                
                
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Create Account</h4>
                        </div>
                        <div class="modal-body">


                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab">

                                <li><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Sign In</a></li>
                                <li class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Sign Up</a></li>
                                <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Forget Password</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                <div class="tab-pane fade" id="home">
                                   {{ Form::open(array('action' => 'SessionController@store', 'class' => 'form exec-form', 'id' => 'loginForm1')) }}
                                    <span class="errors"></span>

                                    <div class="m3 {{ ($errors->has('email')) ? 'has-error' : '' }}">
                                        {{ Form::label('email','Username', array('class' => 'form-label')) }}
                                        {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Username', 'autofocus')) }}
                                        {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                    </div>

                                    <div class="m3 {{ ($errors->has('password')) ? 'has-error' : '' }}">
                                        {{ Form::label('password','Password', array('class' => 'form-label')) }}
                                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('admin.pword')))}}
                                        {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                                    </div>

                                    <a class="forgot-pass" href="#">Forgot your password?</a>
                                    <button type="button" class="button button--dark" onclick="modelBox.login_request('loginForm1')">Login</button>
                                    {{ Form::close() }}
                                </div>

                                <!--Register Form-->
                                <div class="tab-pane active" id="profile">
                                    {{ Form::open(array('action' => 'UserController@register','class' => 'form exec-form', 'id' => 'registerForm')) }}
                                    <div class="errors"></div>
                                    <div class="m3 {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
                                        {{ Form::label('first_name', 'First Name', array('class' => 'form-label')) }}
                                        {{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => trans('users.fname'))) }}
                                        {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                                    </div>

                                    <div class="m3 {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
                                        {{ Form::label('last_name', 'Last name', array('class' => 'form-label')) }}
                                        {{ Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => trans('users.lname'))) }}
                                        {{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}
                                    </div>

                                    <div class="m3 {{ ($errors->has('company_name')) ? 'has-error' : '' }}">
                                        {{ Form::label('company_name', 'Company Name', array('class' => 'form-label')) }}
                                        {{ Form::text('company_name', null, array('class' => 'form-control', 'placeholder' => 'Company Name')) }}
                                        {{ ($errors->has('company_name') ? $errors->first('company_name') : '') }}
                                    </div>

                                    <div class="m3 {{ ($errors->has('email')) ? 'has-error' : '' }}">
                                        {{ Form::label('email', 'Email', array('class' => 'form-label')) }}
                                        {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email'))) }}
                                        {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                    </div>

                                    <div class="m3 {{ ($errors->has('password')) ? 'has-error' : '' }}">
                                        {{ Form::label('password', 'Password', array('class' => 'form-label')) }}
                                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                                        {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                                    </div>

                                    <div class="m3 {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                                        {{ Form::label('password_confirmation', 'Password Confirmation', array('class' => 'form-label')) }}
                                        {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm Password')) }}
                                        {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation') : '') }}
                                    </div>
                                    <button type="button" class="button button--dark" onclick="modelBox.register_request('registerForm')">Create Account</button>
                                    {{ Form::close() }}
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="messages">

                                    {{ Form::open(array('action' => 'UserController@forgot', 'method' => 'post', 'class' => 'form exec-form', 'id' => 'forgotPassword')) }}

                                    <div class="errors"></div>

                                    <div class="m3 {{ ($errors->has('email')) ? 'has-error' : '' }}">
                                        {{ Form::label('email', 'Email', array('class' => 'form-label')); }}
                                        {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email'), 'autofocus')) }}
                                        {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                    </div>

                                    <button type="button" class="button button--dark" onclick="modelBox.forgot_request('forgotPassword')">Reset</button>
                                    {{ Form::close() }}

                                </div>

                            </div>


                        </div>

                    </div>
                </div>
                </div>
                
                
            </span>

<div id="banner-core">
    <form role="search" id="main-search" class="main-search" action="/search">
        <div class="main-search__content">
            <input value="" placeholder="Search..." name="q" class="main-search__input">
            <input type="submit" value="Search" class="main-search__button g-search">
        </div>
    </form>


    <nav class="nav nav--main" id="main-nav">
        <a class="nav__link nav__skip" href="#main-nav-skip">Skip to contentâ€¦</a>
        <ul class="nav__menu">
            <li class="nav__item nav__item--foldable">
                <a class="nav__link nav__link--placeheld" href="#"  >
                    Join the Conversation
                </a>
                            <span aria-hidden="" class="nav__link nav__link--placeholder">
                            Join the Conversation
                            </span>

                <div class="nav__folder">
                    <ul class="nav__menu">
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#talks"  >
                                <span class="nav__item__title">Videos</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#playlists"  >
                                <span class="nav__item__title">#Targets</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#watch/ted-ed"  >
                                <span class="nav__item__title">#Behaviors</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#watch/tedx-talks"  >
                                <span class="nav__item__title">Blogs</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#topics"  >
                                <span class="nav__item__title">#Milestones</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>

                    </ul>
                </div>

            </li>
            <li class="nav__item nav__item--foldable">
                <a class="nav__link nav__link--placeheld" href="#">
                    Exec<span class="home-header__motto__asterisk">u</span>te Better
                </a>
                            <span aria-hidden="" class="nav__link nav__link--placeholder">
                                Execute Better
                            </span>

                <div class="nav__folder">
                    <ul class="nav__menu">
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#talks"  >
                                <span class="nav__item__title">Videos</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#playlists"  >
                                <span class="nav__item__title">#Targets</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#watch/ted-ed"  >
                                <span class="nav__item__title">#Behaviors</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>


                    </ul>
                </div>

            </li>

            <li class="nav__item nav__item--foldable">
                <a class="nav__link nav__link--placeheld" href="#">
                    About Us
                </a>
                            <span aria-hidden="" class="nav__link nav__link--placeholder">
                                About Us
                            </span>

                <div class="nav__folder">
                    <ul class="nav__menu">
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#talks"  >
                                <span class="nav__item__title">Videos</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#playlists"  >
                                <span class="nav__item__title">#Targets</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>
                        <li class="nav__item">
                            <a class="ga-link nav__link" href="#watch/ted-ed"  >
                                <span class="nav__item__title">#Behaviors</span>
                                <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                            </a>
                        </li>


                    </ul>
                </div>

            </li>



        </ul>
        <div id="main-nav-skip"></div>
    </nav>

</div>
</div>
</div>


</header>