<body class="dashboard-body">
<div class="shoji" id="shoji">
    <div class="shoji__fixtures" id="shoji-fixtures"></div>
    <div class="shoji__door">
        <div class="page shoji__washi">

            <div class="main-nav-screen" id="main-nav-screen"></div>
            <header role="banner" class="banner">
                <div class="banner__container">
                    <a id="main-nav-toggle" href="#main-nav" class="g-menu banner__main-nav-toggle">Navigation</a>
                    <a href="{{ URL::to('appUser') }}" class="g-logo-small banner__logo">TED</a>
<span class="banner__account">
<nav class="nav nav--main banner__account__nav">
    <ul class="nav__menu">
        <li class="nav__item nav__item--disabled">
            <a id="account-link" href="/dashboard" class="nav__link nav__link--placeheld banner__account__link banner__account__link--profile">
                <div id="account-image" class="banner__account__image"></div>
                <span id="account-badge" class="badge banner__account__badge">1</span>
            </a>
            <span class="nav__link nav__link--placeholder"></span>
            <div class="nav__folder"></div>
        </li>
    </ul>
</nav>
<a class="banner__account__link banner__account__link--login" href="{{ URL::to('admin') }}">Dashboard</a>
<a class="banner__account__link banner__account__link--signup" href="{{ URL::to('logout') }}">{{trans('pages.logout')}}</a>
</span>

                    <div id="banner-core">
                        <form role="search" id="main-search" class="main-search" action="/search">
                            <div class="main-search__content">
                                <input value="" placeholder="Search..." name="q" class="main-search__input">
                                <input type="submit" value="Search" class="main-search__button g-search">
                            </div>
                        </form>

                        <nav class="nav nav--main" id="main-nav">
                            <a class="nav__link nav__skip" href="#main-nav-skip">Skip to contentâ€¦</a>
                            <ul class="nav__menu">
                                <li class="nav__item nav__item--foldable">
                                    <a class="nav__link nav__link--placeheld" href="#"  >
                                        Join the Conversation
                                    </a>
                            <span aria-hidden="" class="nav__link nav__link--placeholder">
                            Join the Conversation
                            </span>

                                    <div class="nav__folder">
                                        <ul class="nav__menu">
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#talks"  >
                                                    <span class="nav__item__title">Videos</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#playlists"  >
                                                    <span class="nav__item__title">#Targets</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#watch/ted-ed"  >
                                                    <span class="nav__item__title">#Behaviors</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#watch/tedx-talks"  >
                                                    <span class="nav__item__title">Blogs</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#topics"  >
                                                    <span class="nav__item__title">#Milestones</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>

                                </li>
                                <li class="nav__item nav__item--foldable">
                                    <a class="nav__link nav__link--placeheld" href="#">
                                        Exec<span class="home-header__motto__asterisk">u</span>te Better
                                    </a>
                            <span aria-hidden="" class="nav__link nav__link--placeholder">
                                Execute Better
                            </span>

                                    <div class="nav__folder">
                                        <ul class="nav__menu">
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#talks"  >
                                                    <span class="nav__item__title">Videos</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#playlists"  >
                                                    <span class="nav__item__title">#Targets</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>
                                            <li class="nav__item">
                                                <a class="ga-link nav__link" href="#watch/ted-ed"  >
                                                    <span class="nav__item__title">#Behaviors</span>
                                                    <div class="nav__item__info">Lorem Ipsum has been the industry's standard dummy text</div>
                                                </a>
                                            </li>


                                        </ul>
                                    </div>

                                </li>



                            </ul>
                            <div id="main-nav-skip"></div>
                        </nav>

                    </div>
                </div>
            </header>