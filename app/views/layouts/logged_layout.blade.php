<!DOCTYPE html>
<!--[if lt IE 8]> <html class="no-js loggedout oldie ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js loggedout oldie ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="js loggedout js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths cors" lang="en"><!--<![endif]--><head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>
            @section('title')
            @show
        </title>
        <!--[if lte IE 8]>
        <link rel="stylesheet" href="http://assets2.tedcdn.com/stylesheets/global-ie.css?1420487848">
        <link rel="stylesheet" href="http://assets2.tedcdn.com/stylesheets/home-ie.css?1420487848">
        <![endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="{{ asset('frontend/css/global.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/home.css') }}">
        <!--<![endif]-->
        <link rel="stylesheet" href="{{ asset('frontend/css/paly-icon.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/animation.css') }}">
        <!--[if IE 7]><link rel="stylesheet" href="{{ asset('frontend/css/paly-icon-ie7.css') }}"><![endif]-->

        <!-- Bootstrap -->
        <link href="{{ asset('frontend/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('frontend/css/styler.css') }}" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="{{ asset('frontend/js/html5shiv.min.js') }}"></script>
        <script src="{{ asset('frontend/js/respond.min.js') }}"></script>
        <![endif]-->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
        <script src="{{ asset('frontend/bootstrap/js/bootstrap.min.js') }}"></script>
        <link href="{{ asset('frontend/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" /> 
        <script src="http://knockoutjs.com/downloads/knockout-2.2.0.js"></script>
       
        <script type="text/javascript">
$(document).ready(function () {
    $('.carousel').each(function () {
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
    $('.nav__item').hover(function () {
        $(this).closest('li').toggleClass('nav__item--active');
    });

});
        </script>


        <script type="text/javascript">
            $(document).ready(function () {
                $('.nav__item').hover(function () {
                    $(this).closest('li').toggleClass('nav__item--active');
                });
                $('.table-body tr').hover(function () {
                    $(this).toggleClass('hover-class');
                });

                $('.collapse').on('show.bs.collapse', function () {
                    $('.collapse.in').collapse('hide');
                });
            });


        </script>
        <script>

            var modelBox = {
                'newsletter_request': function (id) {
                    $.ajax({
                        url: $('#' + id).attr('action'),
                        type: 'post',
                        data: $('#' + id).serialize(),
                        dataType: 'json',
                        beforeSend: function () {
                            $('#' + id + ' .button').attr('disable', true);
                        },
                        success: function (json) {

                            $('#' + id + ' .button').attr('disable', false);

                            if (json.status == 'OK') {
                                $('#' + id + ' .errors').html(json.message);

                            }

                            if (json.status == 'ERROR') {

                                $('#' + id + ' .errors').html(json.message);

                            }
                        }
                    });
                }
            }
        </script>

        <script src="{{ asset('frontend/js/highcharts.js') }}"></script>

        <script type="text/javascript">

            $(function () {
                $('#chart-container').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Monthly Average Rainfall'
                    },
                    subtitle: {
                        text: 'Source: WorldClimate.com'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct',
                            'Nov',
                            'Dec'
                        ]
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Rainfall (mm)'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                            name: 'Tokyo',
                            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

                        }, {
                            name: 'New York',
                            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

                        }, {
                            name: 'London',
                            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

                        }, {
                            name: 'Berlin',
                            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

                        }]
                });



            });



            $(function () {
                $('#bar-chart-container').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Stacked column chart'
                    },
                    xAxis: {
                        categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total fruit consumption'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    },
                    legend: {
                        align: 'right',
                        x: -30,
                        verticalAlign: 'top',
                        y: 25,
                        floating: true,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.x + '</b><br/>' +
                                    this.series.name + ': ' + this.y + '<br/>' +
                                    'Total: ' + this.point.stackTotal;
                        }
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                style: {
                                    textShadow: '0 0 3px black'
                                }
                            }
                        }
                    },
                    series: [{
                            name: 'John',
                            data: [5, 3, 4, 7, 2]
                        }, {
                            name: 'Jane',
                            data: [2, 2, 3, 2, 1]
                        }, {
                            name: 'Joe',
                            data: [3, 4, 4, 2, 5]
                        }]
                });
            });
        </script>
    </head>

    <!-- Header -->
    @include('layouts/logged_header')
    <!-- ./ Header -->

    <!-- Notifications -->
    @include('layouts/notifications')
    <!-- ./ notifications -->

    <!-- Content -->
    @yield('content')
    <!-- /content -->


    <!-- Footer -->
    @include('layouts/logged_footer')
    <!-- /Footer -->