<footer class="footer">
    <div class="container">
        <div class="footer__content">
            <div class="footer__content__links">
                <nav class="footer__section first" role="navigation">
                    <img src="{{ asset('frontend/images/footer-logo.png') }}" />

                </nav>
                <nav class="footer__section" role="navigation">
                    <h3 class="footer__title">
                        Join the Conversation
                    </h3>

                    <ul class="footer__links">
                        <li class="m5"><a href="#" class="footer__link">Videos</a></li>
                        <li class="m5"><a href="#" class="footer__link">#Targets</a></li>
                        <li class="m5"><a href="#" class="footer__link">#Behaviors</a></li>
                        <li class="m5"><a href="#" class="footer__link">Blogs</a></li>
                        <li class="m5"><a href="#" class="footer__link">#Milestones</a></li>
                    </ul>
                </nav>
                <nav class="footer__section" role="navigation">
                    <h3 class="footer__title">
                        Exec<span class="home-header__motto__asterisk">u</span>te Better
                    </h3>
                    <ul class="footer__links">
                        <li class="m5"><a href="#" class="footer__link">Videos</a></li>
                        <li class="m5"><a href="#" class="footer__link">#Targets</a></li>
                        <li class="m5"><a href="#" class="footer__link">#Behaviors</a></li>
                    </ul>

                </nav>
                <nav class="footer__section" role="navigation">
                    <h3 class="footer__title">About Us</h3>
                    <ul class="footer__links">
                        <li class="m5"><a class="footer__link" href="#">Twitter</a></li>
                        <li class="m5"><a class="footer__link" href="#">Google+</a></li>
                        <li class="m5"><a class="footer__link" href="#">Linkedin</a></li>
                    </ul>
                </nav>

            </div>
            <div class="footer__content__forms">                  
                    {{ Form::open(array('action' => 'HomeController@subscribeEmailRequest', 'class' => 'footer__section footer__section--form footer-newsletter', 'id' => 'newsletter-signup')) }}
                    
                    <h3 class="footer__title">Get Execution Email Updates</h3>
                    <div id="newsletter-signup-details">
                        <div class="errors"></div>
                        <p>Subscribe to receive email notifications
                            when ever new talks are published.</p>
                        <ul class="footer-newsletter__options">
                            <li class="footer-newsletter__option">
                                <label>
                                    {{ Form::checkbox('subscription_list', 'daily', true, array('id' => 'newsletter-signup-daily')) }}                                    
                                    Weekly
                                </label>
                            </li>
                            <li class="footer-newsletter__option">
                                <label>                                    
                                    {{ Form::checkbox('subscription_list', 'weekly', false, array('id' => 'newsletter-signup-weekly')) }}
                                    Monthly
                                </label>
                            </li>
                        </ul>
                        <div class="form-group footer-newsletter__signup-email-group" style="margin-bottom: 5px;">
                            <label for="newsletter-signup-email">
                                Your email address
                            </label>
                            {{ Form::text('email_address', null, array('class' => 'form-control', 'placeholder' => 'Email', 'id' => 'newsletter-signup-email', 'size' => '16', 'autofocus')) }}
                            
                        </div>

                        <input class="button" onclick="modelBox.newsletter_request('newsletter-signup')" value="Submit" type="submit">
                    </div>

                 {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="footer__services"></div>
</footer>
</div>
<a class="shoji__lattice" href="#" id="shoji-lattice"></a>
</div>
</div>
</body></html>