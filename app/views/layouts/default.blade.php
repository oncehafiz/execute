<!DOCTYPE html>
<!--[if lt IE 8]> <html class="no-js loggedout oldie ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js loggedout oldie ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="js loggedout js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths cors" lang="en"><!--<![endif]--><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @section('title')
        @show
    </title>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="http://assets2.tedcdn.com/stylesheets/global-ie.css?1420487848">
    <link rel="stylesheet" href="http://assets2.tedcdn.com/stylesheets/home-ie.css?1420487848">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="{{ asset('frontend/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/home.css') }}">
    <!--<![endif]-->
    <link rel="stylesheet" href="{{ asset('frontend/css/paly-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/animation.css') }}">
    <!--[if IE 7]><link rel="stylesheet" href="{{ asset('frontend/css/paly-icon-ie7.css') }}"><![endif]-->

    <!-- Bootstrap -->
    <link href="{{ asset('frontend/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/styler.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('frontend/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('frontend/js/respond.min.js') }}"></script>
    <![endif]-->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- During Development CSS-->

    <link rel="stylesheet" href="{{ asset('frontend/css/site.css') }}">

    <script type="text/javascript">
        $(document).ready(function(){
            $('.carousel').each(function(){
                $(this).carousel({
                    pause: true,
                    interval: false
                });
            });

            $('.nav__item').hover(function(){
                $(this).closest('li').toggleClass('nav__item--active');
            });
            $('#myTab a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            });

        });

    </script>

    <script>

        var modelBox = {
            'login_request': function(id){
                $.ajax({
                    url: $('#'+id).attr('action'),
                    type: 'post',
                    data: $('#'+id).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#'+id+' .button').attr('disable', true);
                    },
                    success: function(json) {
                        $('#'+id+' .button').attr('disable', true);
                        if(json.status == 'OK') {
                            window.location.href = json.redirect_url;
                        }

                        if(json.status == 'ERROR') {

                            $('#'+id+' .errors').html(json.message);

                        }


                    }
                });
            },
            'register_request': function(id){
                $.ajax({
                    url: $('#'+id).attr('action'),
                    type: 'post',
                    data: $('#'+id).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#'+id+' .button').attr('disable', true);
                    },
                    success: function(json) {
                        $('#'+id+' .button').attr('disable', false);
                        if(json.status == 'OK') {
                            $('#'+id).html('<div class="alert alert-success" role="alert">'+json.message+'</div>');
                        }

                        if(json.status == 'ERROR') {

                            $('#'+id+' .errors').html(json.message);

                        }


                    }
                });
            },
            'forgot_request': function(id) {
                $.ajax({
                    url: $('#'+id).attr('action'),
                    type: 'post',
                    data: $('#'+id).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#'+id+' .button').attr('disable', true);
                    },
                    success: function(json) {
                        $('#'+id+' .button').attr('disable', false);
                        if(json.status == 'OK') {
                            $('#'+id).html('<div class="alert alert-success" role="alert">'+json.message+'</div>');
                        }

                        if(json.status == 'ERROR') {

                            $('#'+id+' .errors').html(json.message);

                        }


                    }
                });
            },
            'newsletter_request': function(id){
                $.ajax({
                    url: $('#'+id).attr('action'),
                    type: 'post',
                    data: $('#'+id).serialize(),
                    dataType: 'json',
                    beforeSend: function() {
                        $('#'+id+' .button').attr('disable', true);
                    },
                    success: function(json) {                        
                        $('#'+id+' .button').attr('disable', false);

                        if(json.status == 'OK') {
                            $('#'+id+' .errors').html(json.message);                            
                        }
                        if(json.status == 'ERROR') {                            
                            $('#'+id+' .errors').html(json.message);

                        }
                    }
                });
            }
        }
    </script>

    <!-- Home page Specific files -->
    <script>
        if(top != self) top.location.replace(location);
    </script>
    <script>
        (function(i,r,l,d,o){
            i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
            if(l && d!="yes" && d!="1") o.userId=l[1];
            __ga('create',"UA-40781755-2",'ted.com',o);
            __ga('set',"dimension4","1.0.0-rc.1.1");
            __ga('set',"dimension3",'logged'+(l ? 'In' : 'Out'));
        })(window,"__ga",('; '+document.cookie).match(/; _ted_user_id=(\d+);/),(window.navigator && window.navigator.doNotTrack),{});
    </script>
    <script>
        _q=[];q=function(){_q.push(arguments)};
        _g=[];g=function(){_g.push(arguments)};

        TED = {"assetBuster":1420487849,"playerPath":"/assets/player.swf?ver=","playerBuster":"5312cee3ada422db44f20c930eda0721","assetHost":"http://assets2.tedcdn.com","authHost":"https://auth.ted.com","settingsUrl":"https://www.ted.com/settings/account","signInUrl":"/session/new","signOutUrl":"https://auth.ted.com/session/logout","signInHelpUrl":"https://auth.ted.com/account/password/new","signUpUrl":"https://auth.ted.com/users/new","gaDimensions":{"breakpoint":"dimension1","talkId":"dimension2","authState":"dimension3","version":"dimension4","playlistId":"dimension5","testId":"dimension7"}};
        TED.signOutUrl += '?referer=' + location.protocol + '//' + location.host + '/session/logout';

        require = {"baseUrl":"http://assets2.tedcdn.com/javascripts","map":{"*":{"Handlebars":"hbs/handlebars","underscore":"lodash"}},"deps":["jquery","libs"],"paths":{"Cookies":"Cookies.js?b331f247439cf974333a98958d35790d","talks":"talks.js?329d0c4cc19848f30b4e85a56e740c5e","menuAim":"menuAim.js?2dae90e87689e70126a38328bac9a5ee","waypoints":"waypoints.js?70444276da7d7de57b7c670dc053f1f5","SwipeView.js.src":"SwipeView.js.src.js?9a048263972115536ed447f9c0b7d904","tedx":"tedx.js?cb5edde472d6fc4c3e8588af68692d3f","touchPunch":"touchPunch.js?8d8e80c9502ede60cfdca9f3c654b846","lodash.js.src":"lodash.js.src.js?e42fbb6429290cb8f1b3177a20c86ab8","Swipe.js.src":"Swipe.js.src.js?9ce5a91df31873e94034bafb36759f30","settings":"settings.js?9e6e02e0999be905b691c58e0a90c860","deferLink":"deferLink.js?7c2c69b4b8525c3951ddd0148d300a67","Swipe":"Swipe.js?b0efdd231fcfc83ba3f1cf2074dd4599","pages":"pages.js?8f7ebfc4a27c439a74d26cfc0df41a1e","hbs.js.src":"hbs.js.src.js?383bfd8b45dc085082110336e7a826ab","pages/dynamic/fellows_newsletter":"pages/dynamic/fellows_newsletter.js?24af5a4eb67097121fd50abd78d170d1","pages/dynamic/dummy":"pages/dynamic/dummy.js?db40c2c843286b433bfd906e6c74f1b3","pages/dynamic/tedx_newsletter":"pages/dynamic/tedx_newsletter.js?802eaca7a51b6332dd80f7a554daf78a","touchPunch.js.src":"touchPunch.js.src.js?00c1f406e9b515a126a9a464ea2db914","jwplayer/jwplayer.html5":"jwplayer/jwplayer.html5.js?c0969ca5364f270ed8647d28c59dc9f9","jwplayer/jwplayer":"jwplayer/jwplayer.js?06735c0a9046236cc35a724dc09c5b0d","swfobject":"libs.js?b43746fe30499f7a78f21bea9d8dbf71","SwipeView":"SwipeView.js?4b94a494c40e841c1b64d10c2f43c853","spinjs.js.src":"spinjs.js.src.js?4f5885262e8544079071bf4876566be7","moment.js.src":"moment.js.src.js?49f7c993eafe9006c9121dd4f414aa6d","Mailcheck":"Mailcheck.js?3188371e023df82df4255791b16b4309","Modernizr":"Modernizr.js?08cd3d9d0df1143464d1ca8ce01fed50","deferLink.js.src":"deferLink.js.src.js?62251a9223be6510052e464550b2716f","flot.js.src":"flot.js.src.js?cd34175824ba61510cc84ab527a02ab8","Mailcheck.js.src":"Mailcheck.js.src.js?73fd8de859b896b01c3eda3b211a49cd","jquery.js.src":"jquery.js.src.js?91515770ce8c55de23b306444d8ea998","transit.js.src":"transit.js.src.js?5df97516555623ced0cdebca2e49b18a","ZeroClipboard.js.src":"ZeroClipboard.js.src.js?0debda99a99a8aacce20868f914def7d","dashboard":"dashboard.js?bb56fadabadf8409bf0d038bdf7b4127","core":"core.js?e21e33f8526aa8b3011f628aa5c7205b","libs":"libs.js?b43746fe30499f7a78f21bea9d8dbf71","waypoints.js.src":"waypoints.js.src.js?f302b99ffb6e3606e9eb7ca7d82a0264","dashboard_sessions":"dashboard_sessions.js?ae8be5179ccd105ae0aafaf405ac8d98","playlists":"playlists.js?61eb2e4f4a8c187dd324d67ff223baec","hbs":"hbs.js?81ed2381e266e540509c1124e6c423c0","jquery":"jquery.js?68536979118413dde508955a0465932a","flot":"flot.js?d5f97ad33963eab9a1de935acd21a2b6","autosize":"autosize.js?1d0b25d78083a95f718a6a69bf464fa5","autosize.js.src":"autosize.js.src.js?7ec00b3f7d46dc6064efd13a60c303c6","swatch":"swatch.js?230ccf485b99f1549530b943492cd7de","search":"search.js?2d1c226e49496a81d8a7e234c765d6a4","moment":"moment.js?2b426fc38e37807575b82742f8634203","numeral.js.src":"numeral.js.src.js?c0e90e3e9f126d77a564afce7b9201b3","home":"home.js?cbf625e47ee6967181ec84206daf442f","Backbone":"Backbone.js?15324957f9dc8675b28abe736b5cff10","depot.js.src":"depot.js.src.js?b18fddcb76b2bf61cde8dcf459879846","spinjs":"spinjs.js?a27156cd455804600428f00e724ab532","talk":"talk.js?518f19a7415beb71010e5d048909c8a4","jqueryujs.js.src":"jqueryujs.js.src.js?c0fce2780e510ebff366b58215d71a46","jqueryujs":"libs.js?b43746fe30499f7a78f21bea9d8dbf71","menuAim.js.src":"menuAim.js.src.js?1b5d9275feea9a1d3bb9ff94d25366fe","global":"global.js?21aacf6a645a3abefc24b91d60b1501a","depot":"depot.js?429a4bc85cfbcc083d5278526d66123d","Cookies.js.src":"Cookies.js.src.js?c9dd9a474a04929b74b9d363f8dfcf8e","profiles":"profiles.js?9927bcd97dfac1ca62a181bce4416bdd","Modernizr.js.src":"Modernizr.js.src.js?5bc88f4d7b86ab7371f2426944495bd5","numeral":"numeral.js?a222da10e94e967e928aca44d69ce0f5","lodash":"libs.js?b43746fe30499f7a78f21bea9d8dbf71","transit":"transit.js?3cb9f56818f9edfb48155659995c5c3b","swfobject.js.src":"swfobject.js.src.js?8c2ec4bc2c9a39bc3f4ff92223077cf6","surprise_me":"surprise_me.js?13f203c4a2300adcaed93a59daeed340","ZeroClipboard":"ZeroClipboard.js?c825395dcbb627a0b4169e10b44b0764","Backbone.js.src":"Backbone.js.src.js?bad03324ce6bc1c8effa5d3b4e3c2750","dq.js.src":"dq.js.src.js?20900afc0ab80dbb330fcde67664a6a3","dq":"libs.js?b43746fe30499f7a78f21bea9d8dbf71"}};
    </script>
    <script async="async" data-main="home" src="{{ asset('frontend/js/core.js') }}"></script>
    <script src="{{ asset('frontend/js/home.js') }}" data-requiremodule="home" data-requirecontext="_" async="" charset="utf-8" type="text/javascript"></script>


</head>

<!-- Header -->
@include('layouts/header')
<!-- ./ Header -->

<!-- Notifications -->
@include('layouts/notifications')
<!-- ./ notifications -->

<!-- Content -->
@yield('content')
<!-- /content -->


<!-- Footer -->
@include('layouts/footer')
<!-- /Footer -->