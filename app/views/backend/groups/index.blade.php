@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('groups.groups')}}
@stop
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')

                       <!-- ./ notifications -->

                       <section class="panel">
                           <header class="panel-heading row">
                               <span class="col-lg-10">
                                  {{trans('groups.groups')}}
                               </span>
                               <span class="col-lg-2">
                                    <button class="btn btn-primary" onClick="location.href='{{ URL::to('admin/groups/create') }}'">{{trans('groups.create')}}</button>
                               </span>
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   <table  class="display table table-striped" id="example">
                                       <thead>
                                       <tr>
                                       		<th>Sr.</th>
                                       		<th>{{trans('groups.name')}}</th>
                    											<th>{{trans('groups.permissions')}}</th>
                    											<th>{{trans('pages.options')}}</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @if (isset($groups) && !empty($groups))
                                       	<?php $count = 1; ?>

                                       	@foreach ($groups as $group)
                  											<tr>
                  												<td>{{ $count }}</td>
                  												<td><a href="groups/{{ $group->id }}">{{ $group->name }}</a></td>
                  												<td>{{ (isset($group['permissions']['admin'])) ? '<i class="icon-ok"></i> Admin' : ''}} {{ (isset($group['permissions']['users'])) ? '<i class="icon-ok"></i> Users' : ''}}</td>
                  												<td>
                  													<button class="btn btn-default" onClick="location.href='{{ action('GroupController@edit', array($group->id)) }}'">{{trans('pages.actionedit')}}</button>
                  												 	<button class="btn btn-default action_confirm {{ ($group->id == 2) ? 'disabled' : '' }}" type="button" data-method="delete" href="{{ URL::to('groups') }}/{{ $group->id }}">{{trans('pages.actiondelete')}}</button>
                  												 </td>
                  											</tr>	
                  										@endforeach
                                       
                                       @else
                                       <tr class="gradeA">
                                           <td class="center" colspan="6">Groups are not founds</td>
                                       </tr>
                                       @endif
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop

