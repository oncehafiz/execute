@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('groups.create')}}
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       {{ Breadcrumbs::render() }}
                       <section class="panel">
                           <header class="panel-heading">
                               New Group
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                    {{ Form::open(array('action' => 'GroupController@store')) }}
                                        <h2>{{trans('groups.create')}}</h2>
                                    
                                        <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                            {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => trans('groups.name'))) }}
                                            {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                        </div>

                                        {{ Form::label(trans('groups.permissions')) }}
                                        <div class="form-group">
                                            <label class="checkbox-inline">
                                                {{ Form::checkbox('adminPermissions', 1) }} Admin
                                            </label>
                                            <label class="checkbox-inline">
                                                {{ Form::checkbox('userPermissions', 1) }} User
                                            </label>

                                        </div>

                                        {{ Form::submit(trans('groups.create'), array('class' => 'btn btn-primary')) }}

                                    {{ Form::close() }}
                               </div>
                           </div>
                       </section>


                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop