@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.listutype')}}
@stop
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')

                <!-- ./ notifications -->
               
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-10">
                            {{trans('pages.currentcode')}}:
                        </span>
                        <span class="col-lg-2">
                            <a href="{{ action('AccessTypeController@create', array()) }}" class="btn btn-success">Create New</a>
                        </span>
                    </header>
                    <div class="panel-body">
                         @if( Session::has('message') )
                        <h3 class="alert-success" style="padding:15px;">{{ Session::get('message') }}</h3>
                        @endif 
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>name</th>
                                        
                                        <th>{{trans('pages.options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @if (isset($types) && !empty($types))
                                    <?php $count = 1; ?>
                                    @foreach ($types as $type)
                                    
                                    
                                    
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $type->type }}</td>
                                        
                                        <td>
                                            <span class="col-sm-1">
                                                <button class="btn btn-success btn-xs" onclick="location.href ='{{ action('AccessTypeController@edit', array($type->id)) }}'"><i class="fa fa-pencil-square-o" title="{{trans('pages.actionedit')}}"></i></button>
                                            </span>
                                            
                                            <span class="col-sm-1">
                                                {{ Form::open(array('action' => array('AccessTypeController@destroy', $type->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                <button type="submit" class="btn btn-danger btn-xs action_confirm" onclick="return confirm('Are you sure you want to delete type: {{ $type->type }}');"><i class='fa  fa-trash-o' title="{{trans('pages.actiondelete')}}"></i></button>
                                                {{ Form::close() }}
                                            </span>
                                            
                                        </td>
                                    </tr>
                                    <?php $count++; ?>
                                    @endforeach
                                    @else
                                    <tr class="gradeA">
                                        <td class="center" colspan="6">Types are not founds</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
