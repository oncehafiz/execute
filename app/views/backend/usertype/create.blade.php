@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.register')}}
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                {{ Breadcrumbs::render() }}
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.register_code')}}
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            {{ Form::open(array(
                                            'action' => 'AccessTypeController@store',
                                            'class' => 'cmxform form-horizontal tasi-form')) }}
                                            
                              <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}" for="edit_typename">
                                {{ Form::label('type', trans('pages.edit_typename'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('type', NULL, array('class' => 'form-control','placeholder' => trans('pages.edit_usertypes'), 'id' => 'edit_colorname'))}}
                                    {{ ($errors->has('type') ? $errors->first('type') : '') }} 
                                </div>
                                               
                            </div>
                                          
                                            
                            <div class="form-group">
                                <div class="col-sm-offset-3">
                                   
                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop