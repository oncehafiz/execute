@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Business Perspective
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')

                       <!-- ./ notifications -->

                       <section class="panel">
                           <header class="panel-heading row">
                               <span class="col-lg-10">
                                   Business Perspective:
                               </span>
                               <span class="col-lg-2">
                                   <a href="{{ action('PerspectiveController@create', array()) }}" class="btn btn-success">New Perspective</a>
                               </span>
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   <table  class="display table table-striped" id="example">
                                       <thead>
                                       <tr>
                                           <th>Sr.</th>
                                           <th>Title</th>
                                           <th>Description</th>
                                           <th>Created</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @if (isset($perspective) && !empty($perspective))
                                       <?php $id = 1; ?>
                                       @foreach ($perspective as $persp)
                                       <tr class="gradeA">
                                           <td>{{ $id++ }}</td>
                                           <td>{{ $persp->busp_name }}</td>
                                           <td>{{ $persp->busp_description }} </td>
                                           <td>{{ date("D, d M y",strtotime($persp->created_at)) }} </td>
                                           <td>
                                               
                                                @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                                                <span class="col-sm-2">
                                                   <button class="btn btn-success btn-xs" onclick="window.location.href='{{ action('PerspectiveController@edit', array($persp->id)) }}'"><i class="fa  fa-search-plus"></i></button>
                                               </span>
                                               <span class="col-sm-2">
                                                   {{ Form::open(array('route' => array('admin.perspective.destroy', $persp->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                       <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete Perspective: {{ $persp->busp_name }}');"><i class="fa  fa-trash-o"></i></button>
                                                   {{ Form::close() }}
                                               </span>
                                               @else
                                               <span class="col-sm-2">
                                                   <button class="btn btn-success btn-xs" onclick="window.location.href='{{ action('PerspectiveController@show', array($persp->id)) }}'"><i class="fa  fa-search-plus"></i></button>
                                               </span>
                                               @endif
                                           </td>
                                       </tr>
                                       @endforeach
                                       @else
                                       <tr class="gradeA">
                                           <td class="center" colspan="6">Business Perspective are not founds</td>
                                       </tr>
                                       @endif
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
