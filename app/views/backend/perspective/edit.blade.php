@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Business Perspective
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-10">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       {{ Breadcrumbs::render() }}
                       <section class="panel">
                           <header class="panel-heading">
                               Edit Business Perspective: {{ $perspective->busp_name }}
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                    {{ Form::open(array(
                                           'action' => array('PerspectiveController@update', $perspective->id),
                                           'method' => 'put'
                                        )) }}
                                   <div class="form-group {{ ($errors->has('page_title')) ? 'has-error' : '' }}">
                                       {{ Form::text('busp_name', $perspective->busp_name, array('class' => 'form-control', 'placeholder' => 'Business Perspestive name')) }}
                                       {{ ($errors->has('busp_name') ? $errors->first('busp_name') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('seo_url')) ? 'has-error' : '' }}">
                                       {{ Form::textarea('busp_description', $perspective->busp_description, array('class' => 'form-control', 'placeholder' => 'Business Perspestive Description')) }}
                                       {{ ($errors->has('busp_description') ? $errors->first('busp_description') : '') }}
                                   </div>                                   

                                    {{ Form::submit('Update Perspective', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
