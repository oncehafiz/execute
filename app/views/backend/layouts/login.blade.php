		@include('backend/includes/login_header')
		
		<!-- Notifications -->
		@include('backend/layouts/notifications')
		<!-- ./ notifications -->

		<!-- Content -->
		@yield('content')
		<!-- ./ content -->

		@include('backend/includes/login_footer')