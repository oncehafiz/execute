@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.register')}}
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                {{ Breadcrumbs::render() }}
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.register_new')}}
                    </header>
                    <header class="panel-heading">
                        {{trans('pages.actionedit')}} 

                    </header>
                    <div class="panel-body">
                        
                        <div class="adv-table">
                            {{ Form::open(array(
                                            'action' => 'DepartmentController@storeuserdepartment',
                                            'class' => 'cmxform form-horizontal tasi-form')) }}

                            
                            <div class="form-group">
                                <label for="first_name" class="control-label col-lg-2">{{trans('users.fname')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('first_name')) ? 'has-error' : '' }}">

                                    {{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => trans('users.fname_empty'))) }}
                                    {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="last_name" class="control-label col-lg-2">{{trans('users.lname')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
                                    {{ Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => trans('users.lname_empty'))) }}
                                    {{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="email" class="control-label col-lg-2">{{trans('users.email')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('email')) ? 'has-error' : '' }} ">
                                    {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email_empty'))) }}
                                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                </div>
                            </div>
                            

                            <div class="form-group ">
                                <label for="password" class="control-label col-lg-2">{{trans('users.passwords')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('password')) ? 'has-error' : '' }} ">
                                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('users.passwords_empty'))) }}
                                {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="password_confirmation" class="control-label col-lg-2">{{trans('users.cpasswords')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }} ">
                                 {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm Password')) }}
                                {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation') : '') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type" class="control-label col-lg-2">{{trans('users.type')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('user_type')) ? 'has-error' : '' }}">
                                    {{ Form::select('is_admin', ['0' => 'IS Admin','1'=>'YES', '2' => 'NO'],0, array('class'=>'form-control')) }}
                                    {{ ($errors->has('is_admin') ? '<label for="" class="error">'.$errors->first('is_admin').'</label>' : '') }}
                                </div>
                            </div>
                                            
                            
                            <div class="form-group">
                                {{ Form::hidden('user_type', 4) }}
                                {{ Form::hidden('dpt_id', $id) }}
                                {{ Form::hidden('department_id', $id) }}
                                {{ Form::hidden('redirect', $id) }}
                                {{ Form::submit('Register', array('class' => 'btn btn-primary')) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop