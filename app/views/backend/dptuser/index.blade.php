@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.listwith')}}
@stop
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')


<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')

                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">

                    <header class="panel-heading row">
                        <span class="col-lg-10">
                            {{trans('pages.currentdptusers')}}:
                        </span>
                        <span class="col-lg-2">
                            <a href="{{ action('DepartmentController@userdepartment', array('id' => $id)) }}" class="btn btn-success">Create New User</a>
                        </span>
                    </header>

                    <div class="panel-body">
                        @if( Session::has('message') )
                        <h3 class="alert-success" style="padding:15px;">{{ Session::get('message') }}</h3>
                        @endif 
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        
                                        <th>Email</th>
                                        <th>{{trans('pages.status')}}</th>
                                        <th>{{trans('pages.options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($users) && !empty($users))
                                    <?php $count = 1; ?>
                                    @foreach ($users as $user)
                                    @if($id==$user->dpt_id)
                                     <?php 
                                            /*if($user->user_type==2){
                                                $company = $user->department()->get();
                                                $user_company = $company[0]->company()->get();
                                            }*/
                                            $company = Department::find($user->dpt_id);
                                            
                                            $dptCompnay = Company::find($company->company_id);
                                            
                                          ?>  
                                    <tr>
                                         <td>{{ $count }}</td>
                                        <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                                        <td>{{ $dptCompnay->name }}</td>
                                        
                                        <td><a href="{{ action('DepartmentController@showuserdepartment', array($user->dpt_id,$user->id)) }}">{{ $user->email }}</a></td>
                                        <td>@if ($user->status=='Active')
                                            {{trans('pages.active')}}
                                            @else
                                            {{trans('pages.notactive')}}
                                            @endif
                                        </td>

                                        <td>
                                            <span class="col-sm-1">
                                                <button class="btn btn-success btn-xs" onclick="location.href ='{{ action('DepartmentController@edituserdepartment', array($user->dpt_id,$user->id)) }}'"><i class="fa fa-pencil-square-o" title="{{trans('pages.actionedit')}}"></i></button>
                                            </span>
                                            <span class="col-sm-1">
                                                {{ Form::open(array('action' => array('DepartmentController@deleteuserdepartment', $user->dpt_id,$user->id), 'class' => 'form-inline')) }}
                                                <button type="submit" class="btn btn-danger btn-xs action_confirm" onclick="return confirm('Are you sure you want to delete User: {{ $user->first_name }} {{ $user->last_name }}');"><i class='fa  fa-trash-o' title="{{trans('pages.actiondelete')}}"></i></button>
                                                {{ Form::close() }}
                                            </span>
                                            
                                            @if ($user->status != 'Banned')
                                            <span class="col-sm-1">
                                                <button class="btn btn-info btn-xs" type="button" onClick="location.href ='{{ action('DepartmentController@ban', array($user->id)) }}'"><i class="fa fa-ban" title="{{trans('pages.actionban')}}"></i></button> 
                                            </span>
                                            @else
                                            <span class="col-sm-1">
                                                <button class="btn btn-warning btn-xs" type="button" onClick="location.href ='{{ action('DepartmentController@unban', array($user->id)) }}'"><i class="fa fa-ban" title="{{trans('pages.actionunban')}}"></i></button> 
                                            </span>
                                            @endif
                                            @if ($user->status != 'Suspended')
                                            <span class="col-sm-1">
                                                <button class="btn btn-info btn-xs" type="button" onClick="location.href ='{{ route('suspendUserForm', array($user->id)) }}'">{{trans('pages.actionsuspend')}}</button> 
                                            </span>
                                            @else
                                            <span class="col-sm-1">
                                                <button class="btn btn-warning btn-xs" type="button" onClick="location.href ='{{ action('UserController@unsuspend', array($user->id)) }}'">{{trans('pages.actionunsuspend')}}</button> 
                                            </span>
                                            @endif



                                        </td>
                                    </tr>
                                    @endif
                                    <?php $count++; ?>
                                    @endforeach
                                    @else
                                    <tr class="gradeA">
                                        <td class="center" colspan="6">Users are not founds</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
