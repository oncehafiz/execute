@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.profile')}}
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.profile')}}
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <div class="well clearfix">
                                <div class="col-md-8">

                                    @if ($user->first_name)
                                    <p><strong>{{trans('users.fname')}}:</strong> {{ $user->first_name }} </p>
                                    @endif
                                    @if ($user->last_name)
                                    <p><strong>{{trans('users.lname')}}:</strong> {{ $user->last_name }} </p>
                                    @endif
                                    <?php
                                    /* if($user->user_type==2){
                                      $company = $user->department()->get();
                                      $user_company = $company[0]->company()->get();
                                      } */
                                    $company = Department::find($user->dpt_id);

                                    $dptCompnay = Company::find($company->company_id);
                                    ?> 
                                    @if ($company->dpt_name)
                                    <p><strong>{{trans('users.CandD')}}:</strong> {{ $dptCompnay->name ." » ".$company->dpt_name }} </p>
                                    @endif
                                    <p><strong>{{trans('users.email')}}</strong> {{ $user->email }}</p>

                                </div>
                                <div class="col-md-4">
                                    <p><em>{{trans('pages.profile')}} {{trans('pages.created')}}: {{ $user->created_at }}</em></p>
                                    <p><em>{{trans('pages.modified')}}: {{ $user->updated_at }}</em></p>
                                    <button class="btn btn-primary" onClick="location.href ='{{ action('DepartmentController@edituserdepartment', array($user->dpt_id,$user->id)) }}'">{{trans('pages.actionedit')}}</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
