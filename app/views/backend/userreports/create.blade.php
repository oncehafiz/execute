@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.register')}}
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.register_rep')}}
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            {{ Form::open(array(
                                            'action' => 'UserReportsController@store',
                                            'files' => true,
                                            'class' => 'cmxform form-horizontal tasi-form')) }}

                            
                            <div class="form-group">
                                <label for="report_name" class="control-label col-lg-2">{{trans('pages.re_name')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('report_name')) ? 'has-error' : '' }}">

                                    {{ Form::text('report_name', null, array('class' => 'form-control', 'placeholder' => trans('pages.re_name'))) }}
                                    {{ ($errors->has('report_name') ? $errors->first('report_name') : '') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="report_description" class="control-label col-lg-2">{{trans('pages.re_des')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('report_description')) ? 'has-error' : '' }}">
                                     {{ Form::textarea('report_description', NULL, array('class' => 'form-control ckeditor', 'placeholder' => 'introduce your self', 'rows' => '6')) }}
                                    {{ ($errors->has('report_description') ? '<label for="" class="error">'.$errors->first('report_description').'</label>' : '') }}
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="file" class="control-label col-lg-2">{{trans('pages.re_file')}}</label>
                                <div class="col-lg-10 {{ ($errors->has('file')) ? 'has-error' : '' }} ">
                                    {{ Form::file('file', null, array('class' => 'form-control', 'placeholder' => trans('users.email_empty'))) }}
                                    {{ ($errors->has('file') ? $errors->first('file') : '') }}
                                </div>
                            </div>
                            <div class="form-group ">
                                {{ Form::label('company_id', trans('pages.edit_agacomapny'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-lg-10 {{ ($errors->has('country_id')) ? 'has-error' : '' }} ">
                                    {{ Form::select('company_id', ['' => trans('pages.edit_agacomapny')]+$companies,Null, array('class'=>'form-control companyChange')) }}
                                     {{ ($errors->has('company_id') ? '<label for="" class="error">'.$errors->first('company_id').'</label>' : '') }}
                                </div>



                            </div>
                            <div class="form-group">
                                {{ Form::label('dpt_id', trans('pages.edit_agadpt'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-lg-10 positions {{ ($errors->has('dpt_id')) ? 'has-error' : '' }}">
                                    {{ Form::select('dpt_id[]', [],null, array('class'=>'form-control selectpicker','multiple' => true,'data-selected-text-format' => "count",'data-live-search' => "true")) }}
                                    {{ ($errors->has('dpt_id') ? '<label for="" class="error">'.$errors->first('dpt_id').'</label>' : '') }}
                                </div>
                            </div>
                                            
                                            
           
                            <div class="form-group">
                                {{ Form::submit('Generate', array('class' => 'col-sm-offset-2 btn btn-primary')) }}
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop