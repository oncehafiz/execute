@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
CMS Pages
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')

                       <!-- ./ notifications -->

                       <section class="panel">
                           <header class="panel-heading row">
                               <span class="col-lg-10">
                                   Current CMS Pages:
                               </span>
                               <span class="col-lg-2">
                                   <a href="{{ action('PageController@create', array()) }}" class="btn btn-success">Create New Page</a>
                               </span>
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   <table  class="display table table-striped" id="example">
                                       <thead>
                                       <tr>
                                           <th>Sr.</th>
                                           <th>Page title</th>
                                           <th>Seo Url</th>
                                           <th>Category Type</th>
                                           <th>Status</th>
                                           <th>Created</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @if (isset($pages) && !empty($pages))
                                       <?php $id = 1; ?>
                                       @foreach ($pages as $page)
                                       <tr class="gradeA">
                                           <td>{{ $id++ }}</td>
                                           <td>{{ $page->title }}</td>
                                           <td>{{ $page->seo_url }} </td>
                                           <td>{{ $page->page_category }} </td>
                                           <td>{{ ($page->status == '1') ? 'Enable' : 'Disable' }} </td>
                                           <td>{{ date("D, d M y",strtotime($page->created_at)) }} </td>
                                           <td>
                                               
                                                @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                                                <span class="col-sm-2">
                                                   <button class="btn btn-success btn-xs" onclick="window.location.href='{{ action('PageController@edit', array($page->id)) }}'"><i class="fa  fa-search-plus"></i></button>
                                               </span>
                                               <span class="col-sm-2">
                                                   {{ Form::open(array('route' => array('admin.pages.destroy', $page->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                       <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete Page: {{ $page->title }}');"><i class="fa  fa-trash-o"></i></button>
                                                   {{ Form::close() }}
                                               </span>
                                               @else
                                               <span class="col-sm-2">
                                                   <button class="btn btn-success btn-xs" onclick="window.location.href='{{ action('PageController@show', array($page->id)) }}'"><i class="fa  fa-search-plus"></i></button>
                                               </span>
                                               @endif
                                           </td>
                                       </tr>
                                       @endforeach
                                       @else
                                       <tr class="gradeA">
                                           <td class="center" colspan="6">pages are not founds</td>
                                       </tr>
                                       @endif
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
