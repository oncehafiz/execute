@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
New CMS page
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       <section class="panel">
                          <header class="panel-heading">
                              {{ Breadcrumbs::render() }}
                          </header>
                      </section>

                       <section class="panel">
                           <header class="panel-heading">
                               New static page:
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   {{ Form::open(array('action' => 'PageController@store', 'method' => 'post')) }}

                                   <div class="form-group {{ ($errors->has('page_title')) ? 'has-error' : '' }}">
                                       {{ Form::text('page_title', null, array('class' => 'form-control', 'placeholder' => 'Page title')) }}
                                       {{ ($errors->has('page_title') ? $errors->first('page_title') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('seo_url')) ? 'has-error' : '' }}">
                                       {{ Form::text('seo_url', null, array('class' => 'form-control', 'placeholder' => 'Seo Url')) }}
                                       {{ ($errors->has('seo_url') ? $errors->first('seo_url') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('meta_title')) ? 'has-error' : '' }}">
                                       {{ Form::text('meta_title', null, array('class' => 'form-control', 'placeholder' => 'Meta Title')) }}
                                       {{ ($errors->has('meta_title') ? $errors->first('meta_title') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('meta_description')) ? 'has-error' : '' }}">
                                       {{ Form::textarea('meta_description', null, array('class' => 'form-control', 'placeholder' => 'Meta Description')) }}
                                       {{ ($errors->has('meta_description') ? $errors->first('meta_description') : '') }}
                                   </div>

                                   

                                   <div class="form-group {{ ($errors->has('meta_keywords')) ? 'has-error' : '' }}">
                                       {{ Form::text('meta_keywords', null, array('class' => 'form-control', 'placeholder' => 'Meta Keywords (comma seprates)')) }}
                                       {{ ($errors->has('meta_keywords') ? $errors->first('meta_keywords') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('page_category')) ? 'has-error' : '' }}">
                                       {{ Form::select('page_category', array('meeting' => 'Meeting','regulation' => 'Regulation','page' => 'Page'), 'page',  array('class' => 'form-control')) }}
                                       {{ ($errors->has('page_category') ? $errors->first('page_category') : '') }}
                                   </div>

                                   <div class="form-group">
                                      {{ Form::textarea('content', null, array('class' => 'form-control ckeditor', 'placeholder' => 'Page content', 'rows' => '6')) }}
                                    </div>

                                   <div class="form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
                                       {{ Form::select('status', array('0' => 'Disable', '1' => 'Enable'), null,  array('class' => 'form-control')) }}
                                       {{ ($errors->has('status') ? $errors->first('status') : '') }}
                                   </div>

                                    {{ Form::submit('Create Page', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
