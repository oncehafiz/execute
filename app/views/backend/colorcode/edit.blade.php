@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Color Code
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.actionedit')}} 

                    </header>
                    <div class="panel-body">
                         @if( Session::has('message') )
                        <h3 class="alert-success" style="padding:15px;">{{ Session::get('message') }}</h3>
                        @endif 
                        <div class="adv-table">
                            {{ Form::open(array(
                                        'action' => array('ColorController@update', $color->id), 
                                        'method' => 'put',
                                        'class' => 'form-horizontal', 
                                        'role' => 'form'
                                        )) }}

                            <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}" for="edit_colorname">
                                {{ Form::label('edit_colorname', trans('pages.edit_colorname'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('name', $color->name, array('class' => 'form-control','placeholder' => trans('pages.edit_colorname'), 'id' => 'edit_colorname'))}}
                                    {{ ($errors->has('name') ? $errors->first('name') : '') }} 
                                </div>
                                               
                            </div>
                            


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    {{ Form::hidden('id', $color->id) }}
                                    {{ Form::submit(trans('pages.actionedit'), array('class' => 'btn btn-primary'))}}
                                </div>
                            </div>
                            {{ Form::close()}}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop