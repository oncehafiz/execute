@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Department
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.actionedit')}} 

                    </header>
                    <div class="panel-body">
                         @if( Session::has('message') )
                        <h3 class="alert-success" style="padding:15px;">{{ Session::get('message') }}</h3>
                        @endif 
                        <div class="adv-table">
                            {{ Form::open(array(
                                        'action' => array('DepartmentController@update', $department->id), 
                                        'method' => 'put',
                                        'class' => 'form-horizontal', 
                                        'role' => 'form'
                                        )) }}

                            <div class="form-group {{ ($errors->has('dpt_name')) ? 'has-error' : '' }}" for="edit_departname">
                                {{ Form::label('edit_departname', trans('pages.edit_departname'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('dpt_name', $department->dpt_name, array('class' => 'form-control','disabled' => 'disabled', 'placeholder' => trans('pages.edit_companyname'), 'id' => 'edit_companyname'))}}
                                    {{ ($errors->has('dpt_name') ? $errors->first('dpt_name') : '') }} 
                                </div>
                                               
                            </div>
                            <div class="form-group">
                                <label for="dpt_description" class="control-label col-lg-2">Description</label>
                                <div class="col-lg-10 {{ ($errors->has('dpt_description')) ? 'has-error' : '' }} ">
                                    {{ Form::textarea('dpt_description', $department->dpt_description, array('class' => 'form-control ckeditor', 'placeholder' => 'introduce your self', 'rows' => '6')) }}
                                    {{ ($errors->has('dpt_description') ? '<label for="" class="error">'.$errors->first('dpt_description').'</label>' : '') }}
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    {{ Form::hidden('id', $department->id) }}
                                    {{ Form::submit(trans('pages.actionedit'), array('class' => 'btn btn-primary'))}}
                                </div>
                            </div>
                            {{ Form::close()}}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop