@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.register')}}
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                {{ Breadcrumbs::render() }}
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.register_dpt')}}
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            {{ Form::open(array(
                                            'action' => 'DepartmentController@store',
                                            'class' => 'cmxform form-horizontal tasi-form')) }}
                                            
                              <div class="form-group {{ ($errors->has('dpt_name')) ? 'has-error' : '' }}" for="edit_departname">
                                {{ Form::label('edit_departname', trans('pages.edit_departname'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('dpt_name', NULL, array('class' => 'form-control','placeholder' => trans('pages.edit_departname'), 'id' => 'edit_departname'))}}
                                    {{ ($errors->has('dpt_name') ? $errors->first('dpt_name') : '') }} 
                                </div>
                                               
                            </div>
                            <div class="form-group">
                                <label for="dpt_description" class="control-label col-lg-2">Description</label>
                                <div class="col-lg-10 {{ ($errors->has('dpt_description')) ? 'has-error' : '' }} ">
                                    {{ Form::textarea('dpt_description', NULL, array('class' => 'form-control ckeditor', 'placeholder' => 'introduce your self', 'rows' => '6')) }}
                                    {{ ($errors->has('dpt_description') ? '<label for="" class="error">'.$errors->first('dpt_description').'</label>' : '') }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('company_id', trans('pages.company_id'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-lg-10 positions {{ ($errors->has('company_id')) ? 'has-error' : '' }}">
                                    {{ Form::select('company_id', $company,NULL, array('class'=>'form-control')) }}

                                </div>
                            </div>                
                                            
                            <div class="form-group">
                                <div class="col-sm-offset-3">
                                    {{ Form::hidden('company_id', $id) }}
                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop