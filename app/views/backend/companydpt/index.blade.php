@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.listdepartment')}}
@stop
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')

                <!-- ./ notifications -->
                
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-1">
                            <i class="fa fa-pencil-square-o btn btn-success btn-xs" title="{{trans('pages.actionedit')}}">&nbsp;<b>{{trans('pages.actionedit')}}</b> </i>
                        </span>
                        <span class="col-lg-3">
                            <i class="fa fa-bullseye btn btn-warning btn-xs" title="{{trans('pages.userslists')}}">&nbsp;<b>{{trans('pages.userslists')}}</b> </i>
                        </span>
                        <span class="col-lg-2">
                            <i class="fa fa-trash-o btn btn-danger btn-xs" title="{{trans('pages.userslists')}}">&nbsp;<b>{{trans('pages.actiondeletes')}}</b> </i>
                        </span>
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-10">
                            {{trans('pages.currentdepart')}}:
                        </span>
                        <span class="col-lg-2">
                            <a href="{{ action('DepartmentController@newone', array($user_id)) }}" class="btn btn-success">Create New</a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Department Name</th>                      
                                       
                                        <th>{{trans('pages.options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @if (isset($department) && !empty($department))
                                    <?php $count = 1; ?>
                                    @foreach ($department as $user)
                                    
                                    <?php $department_data = $user->company()->get(); ?>
                                    
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $user->dpt_name }}</td>
                                        
                                        <td>
                                            <span class="col-sm-1">
                                                <button class="btn btn-success btn-xs" onclick="location.href ='{{ action('DepartmentController@edit', array($user->id)) }}'"><i class="fa fa-pencil-square-o" title="{{trans('pages.actionedit')}}"></i></button>
                                            </span>
                                            
                                            <span class="col-sm-1">
                                                {{ Form::open(array('action' => array('DepartmentController@destroy', $user->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                <button type="submit" class="btn btn-danger btn-xs action_confirm" onclick="return confirm('Are you sure you want to delete Department: {{ $user->dpt_name }}');"><i class='fa  fa-trash-o' title="{{trans('pages.actiondelete')}}"></i></button>
                                                {{ Form::close() }}
                                            </span>
                                            <span class="col-sm-1">
                                                <button class="btn btn-warning btn-xs" type="button" onClick="location.href ='{{ route('departmentusersForm', array($user->id)) }}'"><i class="fa fa-bullseye" title="{{trans('pages.userslist')}}"></i></button> 
                                            </span>
                                        </td>
                                    </tr>
                                    <?php $count++; ?>
                                    @endforeach
                                    @else
                                    <tr class="gradeA">
                                        <td class="center" colspan="6">Department are not founds</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
