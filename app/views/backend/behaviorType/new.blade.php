@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
New Behavior Type
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-8">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       <section class="panel">
                          <header class="panel-heading">
                              {{ Breadcrumbs::render() }}
                          </header>
                      </section>

                       <section class="panel">
                           <header class="panel-heading">
                               New Behavior Type:
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   {{ Form::open(array('action' => 'BehaviorsTypeController@store', 'method' => 'post')) }}

                                   <div class="form-group {{ ($errors->has('page_title')) ? 'has-error' : '' }}">
                                       {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Type name *')) }}
                                       {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('seo_url')) ? 'has-error' : '' }}">
                                       {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description *')) }}
                                       {{ ($errors->has('description') ? $errors->first('description') : '') }}
                                   </div>

                                                          

                                    {{ Form::submit('Create Type', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
