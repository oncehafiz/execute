@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Behavior Type
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-10">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       {{ Breadcrumbs::render() }}
                       <section class="panel">
                           <header class="panel-heading">
                               Edit Behavior: {{ $behaviorType->name }}
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                    {{ Form::open(array(
                                           'action' => array('BehaviorsTypeController@update', $behaviorType->id),
                                           'method' => 'put'
                                        )) }}
                                   <div class="form-group {{ ($errors->has('page_title')) ? 'has-error' : '' }}">
                                       {{ Form::text('name', $behaviorType->name, array('class' => 'form-control', 'placeholder' => 'Name *')) }}
                                       {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('seo_url')) ? 'has-error' : '' }}">
                                       {{ Form::textarea('description', $behaviorType->description, array('class' => 'form-control', 'placeholder' => 'Description *')) }}
                                       {{ ($errors->has('description') ? $errors->first('description') : '') }}
                                   </div>                                   

                                    {{ Form::submit('Update Type', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
