@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.listcompany')}}
@stop
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')

                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-1">
                            <i class="fa fa-pencil-square-o btn btn-success btn-xs" title="{{trans('pages.actionedit')}}">&nbsp;<b>{{trans('pages.actionedit')}}</b> </i>
                        </span>
                        <span class="col-lg-2">
                            <i class="fa fa-bullseye btn btn-info btn-xs" title="{{trans('pages.departmentlist')}}">&nbsp;<b>{{trans('pages.departmentlist')}}</b> </i>
                        </span>
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-10">
                            {{trans('pages.currentcompany')}}:
                        </span>
                        <!--<span class="col-lg-2">
                            <a href="{{ action('UserController@create', array()) }}" class="btn btn-success">Create New User</a>
                        </span>-->
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Company Name</th>                      
                                        <th>Contact</th>
                                        <th>Total Headcount</th>
                                        <th>Owner</th>
                                        <th>{{trans('pages.options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @if (isset($company) && !empty($company))
                                    <?php $count = 1; ?>
                                    @foreach ($company as $user)
                                    
                                    <?php $company_data = $user->company()->get(); ?>
                                    
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $company_data[0]->name }}</td>
                                        <td>{{ $company_data[0]->phone_number }}</td>
                                        <td>{{ $company_data[0]->wc_hc + $company_data[0]->bc_hc }}</td>
                                        <td><a href="{{ action('UserController@show', array($user->user_id)) }}">Show</a></td>
                                        <td>
                                            <span class="col-sm-1">
                                                <button class="btn btn-success btn-xs" onclick="location.href ='{{ action('CompanyController@edit', array($company_data[0]->id)) }}'"><i class="fa fa-pencil-square-o" title="{{trans('pages.actionedit')}}"></i></button>
                                            </span>
                                            <span class="col-sm-1">
                                                <button class="btn btn-info btn-xs" type="button" onClick="location.href ='{{ route('companydepartmentForm', array($company_data[0]->id,$user->user_id)) }}'"><i class="fa fa-bullseye" title="{{trans('pages.departmentlist')}}"></i></button> 
                                            </span>
                                            
                                            <!--<span class="col-sm-1">
                                                {{ Form::open(array('action' => array('CompanyController@destroy', $company_data[0]->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                <button type="submit" class="btn btn-danger btn-xs action_confirm" onclick="return confirm('Are you sure you want to delete Company: {{ $company_data[0]->name }}');"><i class='fa  fa-trash-o' title="{{trans('pages.actiondelete')}}"></i></button>
                                                {{ Form::close() }}
                                            </span>-->
                                        </td>
                                    </tr>
                                    <?php $count++; ?>
                                    @endforeach
                                    @else
                                    <tr class="gradeA">
                                        <td class="center" colspan="6">Companies are not founds</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
