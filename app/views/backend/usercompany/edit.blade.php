@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Company
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading">
                        {{ Breadcrumbs::render() }}
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        {{trans('pages.actionedit')}} 

                    </header>
                    <div class="panel-body">
                         @if( Session::has('message') )
                        <h3 class="alert-success" style="padding:15px;">{{ Session::get('message') }}</h3>
                        @endif 
                        <div class="adv-table">
                            {{ Form::open(array(
                                        'action' => array('CompanyController@update', $company->id), 
                                        'method' => 'put',
                                        'class' => 'form-horizontal', 
                                        'role' => 'form'
                                        )) }}

                            <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}" for="name">
                                {{ Form::label('edit_companyname', trans('pages.edit_companyname'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('name', $company->name, array('class' => 'form-control','disabled' => 'disabled', 'placeholder' => trans('pages.edit_companyname'), 'id' => 'edit_companyname'))}}
                                </div>
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}                
                            </div>


                            <div class="form-group {{ ($errors->has('post_code')) ? 'has-error' : '' }}" for="lastName">
                                {{ Form::label('post_code', trans('pages.edit_post_code'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('post_code', $company->post_code, array('class' => 'form-control', 'placeholder' => trans('pages.edit_post_code'), 'id' => 'post_code'))}}
                                </div>
                                {{ ($errors->has('post_code') ? $errors->first('post_code') : '') }}                
                            </div>

                            <div class="form-group {{ ($errors->has('po_box')) ? 'has-error' : '' }}" for="lastName">
                                {{ Form::label('po_box', trans('pages.edit_po_box'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('po_box', $company->po_box, array('class' => 'form-control', 'placeholder' => trans('pages.edit_po_box'), 'id' => 'po_box'))}}
                                </div>
                                {{ ($errors->has('po_box') ? $errors->first('po_box') : '') }}                
                            </div>
                            <div class="form-group {{ ($errors->has('phone_number')) ? 'has-error' : '' }}" for="lastName">
                                {{ Form::label('phone_number', trans('pages.edit_phone_number'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('phone_number', $company->phone_number, array('class' => 'form-control', 'placeholder' => trans('pages.edit_phone_number'), 'id' => ''))}}
                                </div>
                                {{ ($errors->has('phone_number') ? $errors->first('phone_number') : '') }}                
                            </div>
                           
                                        
                             <div class="form-group {{ ($errors->has('address_1')) ? 'has-error' : '' }}" for="address_1">
                                {{ Form::label('address_1', trans('pages.edit_address_1'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('address_1', $company->address_1, array('class' => 'form-control', 'placeholder' => trans('pages.edit_address_1'), 'id' => ''))}}
                                </div>
                                {{ ($errors->has('address_1') ? $errors->first('address_1') : '') }}                
                            </div>
                            <div class="form-group {{ ($errors->has('address_2')) ? 'has-error' : '' }}" for="address_1">
                                {{ Form::label('address_2', trans('pages.edit_address_2'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-sm-10">
                                    {{ Form::text('address_2', $company->address_1, array('class' => 'form-control', 'placeholder' => trans('pages.edit_address_2'), 'id' => ''))}}
                                </div>
                                {{ ($errors->has('address_2') ? $errors->first('address_2') : '') }}                
                            </div>            
                                        
                            <div class="form-group ">
                                {{ Form::label('country_id', trans('pages.edit_country'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-lg-10 {{ ($errors->has('country_id')) ? 'has-error' : '' }} ">
                                    {{ Form::select('country_id', $country,$company->country_id, array('class'=>'form-control contryChange')) }}
                                </div>



                            </div>
                            <div class="form-group">
                                {{ Form::label('city_id', trans('pages.edit_city'), array('class' => 'col-sm-2 control-label')) }}
                                <div class="col-lg-10 positions {{ ($errors->has('city_id')) ? 'has-error' : '' }}">
                                    {{ Form::select('city_id', $city,$company->city_id, array('class'=>'form-control')) }}

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    {{ Form::hidden('id', $company->id) }}
                                   
                                    {{ Form::submit(trans('pages.actionedit'), array('class' => 'btn btn-primary'))}}
                                </div>
                            </div>
                            {{ Form::close()}}
                        </div>
                    </div>
                </section>


            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop