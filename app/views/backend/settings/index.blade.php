@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
General Setting
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       {{ Breadcrumbs::render() }}
                       <section class="panel">
                           <header class="panel-heading">
                               General Setting:
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                     {{ Form::open(array('action' => 'SettingController@store', 'method' => 'post')) }}

                                   <div class="form-group {{ ($errors->has('gm_email')) ? 'has-error' : '' }}">
                                       {{ Form::text('gm_email', $setting->gm_email, array('class' => 'form-control', 'placeholder' => 'GM Email')) }}
                                       {{ ($errors->has('gm_email') ? $errors->first('gm_email') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('contact_email')) ? 'has-error' : '' }}">
                                       {{ Form::text('contact_email', $setting->contact_email, array('class' => 'form-control', 'placeholder' => 'Contact Email')) }}
                                       {{ ($errors->has('contact_email') ? $errors->first('contact_email') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('recommnedation_email')) ? 'has-error' : '' }}">
                                       {{ Form::text('recommnedation_email', $setting->recommnedation_email, array('class' => 'form-control', 'placeholder' => 'Recommnedation Email')) }}
                                       {{ ($errors->has('recommnedation_email') ? $errors->first('recommnedation_email') : '') }}
                                   </div>                                   

                                   

                                    {{ Form::submit('Save Setting', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
