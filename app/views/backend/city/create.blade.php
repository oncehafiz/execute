@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
New City
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       
                       <section class="panel">
                           <header class="panel-heading">
                               New City >  
                        <span class="alert-info"><a href="{{URL::to('/admin/cities')}}">Go back</a></span>
                   
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   {{ Form::open(array('action' => 'CityController@store', 'method' => 'post')) }}

                                   <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                       {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'City Name')) }}
                                       {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                   </div>
                                   <div class="form-group">
                                        {{ Form::select('country_id', $country,'235', array('class'=>'form-control')) }}
                                   </div>
                                   

                                    {{ Form::submit('Create City', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
