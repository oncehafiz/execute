@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Cities list
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')

                       <!-- ./ notifications -->

                       <section class="panel">
                           
                           <header class="panel-heading row">
                               <span class="col-lg-10">
                                   Current City:
                               </span>
                               <span class="col-lg-2">
                                   <a href="{{URL::to('/admin/city/create')}}" class="btn btn-success">Create New city</a>
                               </span>
                           </header>
                          <div class="panel-body">
                              
              <div class="adv-table">
              <table  class="display table table-bordered table-striped dataTable" id="dynamic-table">
              <thead>
              <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th class="hidden-phone">Actions</th>
              </tr>
              </thead>
              <tbody>
                  @if (isset($city) && !empty($city))
                                       <?php $id = 1; ?>
                                       @foreach ($city as $c)
                                       <tr class="gradeA">
                                           <td>{{ $id++ }}</td>
                                           <td>{{ $c->name }}</td>
                                           
                                           <td>
                                               <span class="col-sm-1">
                                                   <button class="btn btn-success btn-xs" onclick="window.location.href='{{ action('CityController@edit', array($c->id)) }}'"><i class="fa  fa-search-plus"></i></button>
                                               </span>
                                               <span class="col-sm-1">
                                                    {{ Form::open(array('action' => array('CityController@delete', $c->id))) }}
                                                       <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete City: {{ $c->name }}');"><i class="fa  fa-trash-o"></i></button>
                                                   {{ Form::close() }}
                                               </span>
                                           </td>
                                           
                                       </tr>
                                       @endforeach
                                       @else
                                       <tr class="gradeA">
                                           <td class="center" colspan="6">City are not available.</td>
                                       </tr>
                                       @endif
                    
              </tbody>
                  
              <tfoot>
              <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th class="hidden-phone"></th>
              </tr>
              </tfoot>
              </table>
              </div>
              </div>
             
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
