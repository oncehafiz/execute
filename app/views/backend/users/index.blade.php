@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.listwith')}}
@stop
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')

                <!-- ./ notifications -->
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-2">
                            <i class="fa fa-pencil-square-o btn btn-success btn-xs" title="{{trans('pages.actionedit')}}">&nbsp;<b>{{trans('pages.actionedits')}}</b> </i>
                        </span>
                        <span class="col-lg-2">
                            <i class="fa fa-trash-o btn btn-danger btn-xs" title="{{trans('pages.userslists')}}">&nbsp;<b>{{trans('pages.actiondeletes')}}</b> </i>
                        </span>
                        <span class="col-lg-3">
                            <i class="fa fa-eye-slash btn btn-warning btn-xs" title="{{trans('pages.departmentlist')}}">&nbsp;<b>{{trans('pages.departmentlists')}}</b> </i>
                        </span>
                        
                    </header>
                </section>
                <section class="panel">
                    <header class="panel-heading row">
                        <span class="col-lg-10">
                            {{trans('pages.currentusers')}}:
                        </span>
                        <span class="col-lg-2">
                            <a href="{{ action('UserController@create', array()) }}" class="btn btn-success">Create New User</a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Type</th>
                                        <th>Email</th>
                                        <th>{{trans('pages.status')}}</th>
                                        <th>{{trans('pages.options')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($users) && !empty($users))
                                    <?php $count = 1; ?>
                                    @foreach ($users as $user)
                                    
                                    @if($user->user_type==1 || $user->user_type==0 || $user->user_type==2)
                                    
                                        <?php 
                                            /*if($user->user_type==2){
                                                $company = $user->department()->get();
                                                $user_company = $company[0]->company()->get();
                                            }*/
                                            $company = Company::where('id',$user->dpt_id)->get();
                                          ?>  
                                    <tr>
                                        <td>{{ $count }}</td>
                                        <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                                        <td>{{ ($user->user_type == 2) ? $company[0]->name : "" }}</td>
                                        <td><abbr title="1/0 For Application Admin 2 For company Admin">{{ $user->user_type }}</abbr></td>
                                        <td><a href="{{ action('UserController@show', array($user->id)) }}">{{ $user->email }}</a></td>
                                        <td>@if ($user->status=='Active')
                                            {{trans('pages.active')}}
                                            @else
                                            {{trans('pages.notactive')}}
                                            @endif
                                        </td>

                                        <td>
                                            <span class="col-sm-1">
                                                <button class="btn btn-success btn-xs" onclick="location.href ='{{ action('UserController@edit', array($user->id)) }}'"><i class="fa fa-pencil-square-o" title="{{trans('pages.actionedit')}}"></i></button>
                                            </span>
                                            <span class="col-sm-1">
                                                {{ Form::open(array('route' => array('admin.users.destroy', $user->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                <button type="submit" class="btn btn-danger btn-xs action_confirm" onclick="return confirm('Are you sure you want to delete User: {{ $user->first_name }} {{ $user->last_name }}');"><i class='fa  fa-trash-o' title="{{trans('pages.actiondelete')}}"></i></button>
                                                {{ Form::close() }}
                                            </span>
                                            @if ($user->user_type == 2)
                                            <span class="col-sm-1">
                                                <button class="btn btn-warning btn-xs" type="button" onClick="location.href ='{{ route('usercompanyForm', array($user->id)) }}'"><i class="fa fa-eye-slash" title="{{trans('pages.departmentlist')}}"></i></button> 
                                            </span>
                                            
                                            @endif
                                            @if ($user->status != 'Banned')
                                            <span class="col-sm-1">
                                                <button class="btn btn-info btn-xs" type="button" onClick="location.href ='{{ action('UserController@ban', array($user->id)) }}'"><i class="fa fa-ban" title="{{trans('pages.actionban')}}"></i></button> 
                                            </span>
                                            @else
                                            <span class="col-sm-1">
                                                <button class="btn btn-warning btn-xs" type="button" onClick="location.href ='{{ action('UserController@unban', array($user->id)) }}'"><i class="fa fa-ban" title="{{trans('pages.actionunban')}}"></i></button> 
                                            </span>
                                            @endif
                                            @if ($user->status != 'Suspended')
                                            <span class="col-sm-1">
                                                <button class="btn btn-info btn-xs" type="button" onClick="location.href ='{{ route('suspendUserForm', array($user->id)) }}'">{{trans('pages.actionsuspend')}}</button> 
                                            </span>
                                            @else
                                            <span class="col-sm-1">
                                                <button class="btn btn-warning btn-xs" type="button" onClick="location.href ='{{ action('UserController@unsuspend', array($user->id)) }}'">{{trans('pages.actionunsuspend')}}</button> 
                                            </span>
                                            @endif
                                            
                                            
                                            
                                            </td>
                                    </tr>
                                   
                                    <?php $count++; ?>
                                     @endif
                                    @endforeach
                                    @else
                                    <tr class="gradeA">
                                        <td class="center" colspan="6">Users are not founds</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
