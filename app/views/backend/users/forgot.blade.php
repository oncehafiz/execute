@extends('backend.layouts.login')

{{-- Web site Title --}}
@section('title')
{{trans('users.forgotupword')}}
@stop

{{-- Content --}}
@section('content')
<div class="container">
        {{ Form::open(array('action' => 'UserController@forgot', 'method' => 'post', 'class' => 'form-signin')) }}

        <h2 class="form-signin-heading">{{trans('users.forgotupword')}}</h2>
        <div class="login-wrap">
          <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
              {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email'), 'autofocus')) }}
              {{ ($errors->has('email') ? $errors->first('email') : '') }}
          </div>

          {{ Form::submit(trans('users.resendpword'), array('class' => 'btn btn-primary'))}}

          {{ Form::close() }}
        </div>


    </div>

@stop