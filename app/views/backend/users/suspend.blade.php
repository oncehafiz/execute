@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.actionsuspend')}} {{trans('pages.user')}}
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       {{ Breadcrumbs::render() }}                     


                       <section class="panel">
                           <header class="panel-heading">
                               {{trans('pages.actionsuspend')}} {{trans('pages.user')}}
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   <div class="row">
                                        <div class="col-md-12">
                                            {{ Form::open(array('action' => array('UserController@suspend', $id), 'method' => 'post')) }}
                                     
                                                <h2>{{trans('pages.actionsuspend')}} {{trans('pages.user')}}</h2>

                                                <div class="form-group {{ ($errors->has('minutes')) ? 'has-error' : '' }}">
                                                    {{ Form::text('minutes', null, array('class' => 'form-control', 'placeholder' => trans('pages.minutes'), 'autofocus')) }}
                                                    {{ ($errors->has('minutes') ? $errors->first('minutes') : '') }}
                                                </div>         

                                                {{ Form::hidden('id', $id) }}

                                                {{ Form::submit(trans('pages.actionsuspend').'!', array('class' => 'btn btn-primary')) }}
                                                
                                            {{ Form::close() }}
                                        </div>
</div>
                               </div>
                           </div>
                       </section>


                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
