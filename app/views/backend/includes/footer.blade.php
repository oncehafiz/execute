<!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2014 &copy; <a href="{{ URL::to('home') }}"> Antique Logic.</a>
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('backend/js/jquery.js') }}"></script>
    <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('backend/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('backend/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('backend/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/js/jquery.sparkline.js') }}" type="text/javascript"></script>
    <script src="{{ asset('backend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>
    <script src="{{ asset('backend/js/owl.carousel.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('backend/assets/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('backend/js/jquery.customSelect.min.js') }}" ></script>
    <script src="{{ asset('backend/js/respond.min.js') }}" ></script>
    <script type="text/javascript" language="javascript" src="{{ asset('backend/assets/advanced-datatable/media/js/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/data-tables/DT_bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ asset('backend/assets/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    
    <script src="{{ asset('backend/js/jquery.fileuploadmulti.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('backend/js/bootstrap-select.js')}}"></script>

    <!--right slidebar-->
    <script src="{{ asset('backend/js/slidebars.min.js') }}"></script>

    <!--common script for all pages-->
    <script src="{{ asset('backend/js/common-scripts.js') }}"></script>

    <!--script for this page-->
    <script src="{{ asset('backend/js/sparkline-chart.js') }}"></script>
    <script src="{{ asset('backend/js/easy-pie-chart.js') }}"></script>


<script type="text/javascript" src="{{ asset('backend/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
      <script type="text/javascript" src="{{ asset('backend/assets/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
      <script type="text/javascript" src="{{ asset('backend/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
  <script type="text/javascript" src="{{ asset('backend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>
    <script src="{{ asset('backend/js/jquery.fileuploadmulti.min.js') }}"></script>
    <script src="{{ asset('backend/js/dynamic-table.js') }}"></script>
    <script src="{{ asset('backend/js/advanced-form-components.js') }}"></script>
    
    <script type="text/javascript">

        $(document).ready(function()
        {

            var image_count = '{{ (isset($image_count) ? $image_count+1 : 0 )}}';

            var settings = {
                url: "{{ URL::to('/') }}/admin/upload",
                method: "POST",
                allowedTypes:"jpg,png,gif",
                fileName: "image",
                multiple: true,
                onSuccess:function(files,data,xhr)
                {
                    var imageObj = jQuery.parseJSON(data);
                    console.log(imageObj);
                    jQuery.each( imageObj['images'], function( i, val ) {
                        var html = '<div class="form-group" id="rowId_'+image_count+'">';
                            html += '<input type="hidden" name="banner['+image_count+'][image]" value="'+val.img+'" />';
                            html += '<div class="col-lg-3"><input name="banner['+image_count+'][name]" value="" class="form-control" placeholder="Title"></div>';
                            html += '<div class="col-lg-3"><input name="banner['+image_count+'][link]" value="" class="form-control" placeholder="Link"></div>';
                            html += '<div class="col-lg-3"><input name="banner['+image_count+'][sort_order]" value="" class="form-control" placeholder="Sort Order" size="5"></div>';
                            html += '<div class="col-lg-3"><img src="{{ URL::to('/') }}/'+val.src+'" width="100" />';
                            html += '<a onclick=\'removeFile("'+val.src+'", "'+image_count+'")\' href="javascript:;" class="btn btn-danger">Remove</a> </div>';

                            html += '</div>';
                            image_count++;
                        $('#banner_images').append(html);
                    });

                    $("#status").html("<p color='green'>Upload is success</p>");

                },
                afterUploadAll:function()
                {
                    $("#status").html("<p color='green'>all images upsloaded</p>");

                },
                onError: function(files,status,errMsg)
                {
                    $("#status").html("<p color='red'>Upload is Failed</p>");
                }
            }
            $("#mulitplefileuploader").uploadFile(settings);

        });

        function removeFile(file_name, rowId)
        {
            var r = confirm("Are you sure you want to delete this Image?")
            if(r == true)
            {
                $.ajax({
                  url: "{{ URL::to('/') }}/admin/file/delete",
                   method: "POST",
                  data: {'file' : file_name },
                  success: function (response) {
                        $('#rowId_'+rowId).remove();
                  }
                });
            }
        }
    </script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });




    function changeStates(country_id){
           $.getJSON("{{URL::to('/')}}/showStates/"+country_id, function(jsonData){
               select = '<select name="state" id="cities_change" class="form-control" onchange="changeCities(this.value)">';
               $.each(jsonData, function(i,data)
                 {
                      select +='<option value="'+data.name+'">'+data.name+'</option>';
                 });
                 select += '</select>';
               $("#state_data").html(select);
           });
      }

      $(document).on('change','#cities_change',function(){
            var state_key = $(this).val();

             $.getJSON("{{URL::to('/')}}/showCities/"+state_key, function(jsonData){
                   select = '<select name="city" class="form-control" id="cities">';
                   $.each(jsonData, function(i,data)
                     {
                          select +='<option value="'+data.city_name+'">'+data.city_name+'</option>';
                     });
                     select += '</select>';
                   $("#cityData").html(select);
               });
        });

  </script>
  <!--dynamic table initialization -->
    <script src="{{ asset('backend/js/dynamic_table_init.js')}}"></script>
  <!--script for this page-->
  <script src="{{ asset('backend/js/form-component.js') }}"></script>
  
  </body>
</html>
