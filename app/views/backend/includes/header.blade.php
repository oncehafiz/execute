<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="WisnetSol">
    <meta name="keyword" content="admin, coaches, network, business coach, family coach">
    <link rel="shortcut icon" href="{{ asset('backend/img/favicon.png') }}">

    <title>
        @section('title')
        @show
    </title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/bootstrap-select.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('backend/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('backend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{ asset('backend/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/bootstrap-datetimepicker/css/datetimepicker.css')}}" />
     <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/bootstrap-datepicker/css/datepicker.css') }}" />

     <link href="{{ asset('backend/css/multifileupload.css') }}" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="{{ asset('backend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" />
    
      <!--right slidebar-->
      <link href="{{ asset('backend/css/slidebars.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style-responsive.css') }}" rel="stylesheet" />

    <!--dynamic table-->
    <link href="{{ asset('backend/assets/advanced-datatable/media/css/demo_page.css')}}" rel="stylesheet" />
    <link href="{{ asset('backend/assets/advanced-datatable/media/css/demo_table.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('backend/assets/data-tables/DT_bootstrap.css')}}" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ asset('backend/js/html5shiv.js') }}"></script>
      <script src="{{ asset('backend/js/respond.min.js') }}"></script>
    <![endif]-->
  </head>

  <body>
   <section id="container">