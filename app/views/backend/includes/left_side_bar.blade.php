<?php

?>
<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a class="active" href="{{ action('PageController@home', array()) }}">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  @if (Sentry::check() && Sentry::getUser()->hasAccess('admin'))
                       <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Settings</span>
                          </a>
                          <ul class="sub">
                              <li><a  href="#">General Setting</a></li>
                              <li><a  href="#">Admin Profile</a></li>                              
                          </ul>
                      </li>                     

                      <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Users</span>
                          </a>
                          <ul class="sub">
                              <li><a  href="{{ action('UserController@index',array()) }}">User List</a></li>
                              <li><a  href="{{ action('GroupController@index',array()) }}">User Groups</a></li>
                              <li><a  href="{{ action('UserReportsController@index',array()) }}">User Reports</a></li>
                          </ul>
                      </li>  

                      <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Perspective</span>
                          </a>
                          <ul class="sub">
                              <li><a  href="{{ action('PerspectiveController@index',array()) }}">View All</a></li>
                              <li><a  href="{{ action('PerspectiveController@create',array()) }}">Create New</a></li>
                              
                          </ul>
                      </li> 

                      <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Behavior Types</span>
                          </a>
                          <ul class="sub">
                              <li><a  href="{{ action('BehaviorsTypeController@index',array()) }}">View All</a></li>
                              <li><a  href="{{ action('BehaviorsTypeController@create',array()) }}">Create New</a></li>
                              
                          </ul>
                      </li>      

                     <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Static PAGES</span>
                          </a>
                         <ul class="sub">
                              <li><a  href="{{ action('PageController@index',array('type' => 'page')) }}">Pages</a></li>                              
                          </ul>
                      </li>
                      
                       <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Banners</span>
                          </a>
                            <ul class="sub">
                                 <li><a  href="{{  URL::to('admin/banners', array())  }}">Banners</a></li>
                            </ul>
                      </li>
                      <li class="sub-menu">
                          <a href="javascript:;" >
                              <i class="fa fa-laptop"></i>
                              <span>Widgets</span>
                          </a>
                            <ul class="sub">
                                 <li><a  href="{{  URL::to('admin/colorcode', array())  }}">Color Code</a></li>
                                 <li><a  href="{{  URL::to('admin/usertypes', array())  }}">User Type</a></li>
                                 <li><a  href="{{  URL::to('admin/countries', array())  }}">Countries</a></li>
                                 <li><a  href="{{  URL::to('admin/cities', array())  }}">Cities</a></li>
                            </ul>
                      </li>

                  @endif

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

