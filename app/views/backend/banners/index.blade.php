@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.banners_list')}}
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')

                       <!-- ./ notifications -->

                       <section class="panel">
                           <header class="panel-heading row">
                               <span class="col-lg-10">
                                   Current Banners
                               </span>
                               <span class="col-lg-2">
                                   <a href="{{  URL::to('admin/banners/create', array())  }} " class="btn btn-success">Create New Banner</a>
                               </span>
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   <table  class="display table table-striped" id="example">
                                       <thead>
                                       <tr>
                                           <th>Sr.</th>
                                           <th>Title</th>
                                           <th>Status</th>
                                           <th>Created</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>
                                       <tbody>
                                       @if (isset($banners) && !empty($banners))
                                       @foreach ($banners as $banner)
                                       <tr class="gradeA">
                                           <td>{{ $banner->banner_id }}</td>
                                           <td>{{ $banner->name }}</td>
                                           <td>{{ ($banner->status == '1' ? 'Enabled' : 'Disabled') }} </td>
                                           <td>{{ date("D, d M y",strtotime($banner->created_at)) }} </td>
                                           <td>
                                               <span class="col-sm-2">
                                                   <button class="btn btn-success btn-xs" onclick="window.location.href='{{ action('BannersController@edit', array($banner->id)) }}'"><i class="fa  fa-search-plus"></i></button>
                                               </span>
                                               <span class="col-sm-2">
                                                   {{ Form::open(array('route' => array('admin.banners.destroy', $banner->id), 'method' => 'delete', 'class' => 'form-inline')) }}
                                                       <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete Banner: {{ $banner->name }}');"><i class="fa  fa-trash-o"></i></button>
                                                   {{ Form::close() }}
                                               </span>
                                           </td>
                                       </tr>
                                       @endforeach
                                       @else
                                       <tr class="gradeA">
                                           <td class="center" colspan="">Banners are not founds</td>
                                       </tr>
                                       @endif
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
