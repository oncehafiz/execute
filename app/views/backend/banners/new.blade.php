@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
{{trans('pages.create_banner')}}
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       <section class="panel">
                          <header class="panel-heading">
                              {{ Breadcrumbs::render() }}
                          </header>
                      </section>

                       <section class="panel">
                           <header class="panel-heading">
                               New Banner:
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   {{ Form::open(array('action' => 'BannersController@store', 'method' => 'post')) }}

                                   <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                       {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Banner Title')) }}
                                       {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
                                       {{ Form::textArea('description', null, array('class' => 'form-control', 'placeholder' => 'Description')) }}
                                       {{ ($errors->has('description') ? $errors->first('description') : '') }}
                                   </div>

                                   <div class="form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
                                       {{ Form::select('status', array('0' => 'Disable', '1' => 'Enable'), null,  array('class' => 'form-control')) }}
                                       {{ ($errors->has('status') ? $errors->first('status') : '') }}
                                   </div>

                                   <div id="banner_images">
                                    @if(Input::old('banner'))

                                        @foreach(Input::old('banner') as $key => $field)
                                            <div class="form-group" id="rowId_{{$key}}">
                                            <input type="hidden" name="banner['{{$key}}'][image]" value="{{$field['image']}}" />
                                            <div class="col-lg-3"><input name="banner['{{$key}}'][name]" value="{{$field['name']}}" class="form-control" placeholder="Title"></div>
                                            <div class="col-lg-3"><input name="banner['{{$key}}'][link]" value="{{$field['link']}}" class="form-control" placeholder="Link"></div>
                                            <div class="col-lg-3"><input name="banner['{{$key}}'][sort_order]" value="{{$field['sort_order']}}" class="form-control" placeholder="Sort Order" size="5"></div>
                                            <div class="col-lg-3"><img src="{{ URL::to('/') }}/{{$field['image']}}" width="100" />
                                            <a onclick="removeFile('{{$field['image']}}', '{{$key}}')" href="javascript:;" class="btn btn-danger">Remove</a> </div>

                                            </div>
                                            <?php $image_count = $key; ?>
                                        @endforeach

                                    @endif

                                   </div>

                                   <div id="status">

                                   </div>

                                   <div class="form-group">
                                       <div id="mulitplefileuploader">Upload</div>
                                   </div>

                                    {{ Form::submit('Create Banner', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
