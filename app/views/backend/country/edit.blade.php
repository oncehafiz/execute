@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Country
@stop

{{-- Content --}}
@section('content')
@include('backend/includes/admin_header_menu')
@include('backend/includes/left_side_bar')
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifications -->
                @include('backend.layouts.notifications')
                <!-- ./ notifications -->

                <section class="panel">
                    <header class="panel-heading">
                        Edit Country: {{ $country->name }} >  
                        <span class="alert-info"><a href="{{URL::to('/admin/countries')}}">Go back</a></span>
                    </header>
                    <div class="panel-body">
                        @if( Session::has('message') )
                        <h3 class="alert-success">{{ Session::get('message') }}</h3>
                        @endif 
                        <div class="adv-table">
                            {{ Form::open(array(
                                           'action' => array('CountryController@update', $country->id),
                                           'method' => 'post'
                                        )) }}
                            <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                {{ Form::text('name', $country->name, array('class' => 'form-control', 'placeholder' => 'Country Name')) }}
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}
                            </div>
                                <input type="hidden" name="id" value="{{$country->id}}" />
                            {{ Form::submit('Update Country', array('class' => 'btn btn-primary')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
@stop
