@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
New Country
@stop

{{-- Content --}}
@section('content')
      @include('backend/includes/admin_header_menu')
       @include('backend/includes/left_side_bar')
       <!--main content start-->
       <section id="main-content">
           <section class="wrapper site-min-height">
               <!-- page start-->
               <div class="row">
                   <div class="col-lg-12">
                       <!-- Notifications -->
                       @include('backend.layouts.notifications')
                       <!-- ./ notifications -->
                       
                       <section class="panel">
                           <header class="panel-heading">
                               New Country >  
                        <span class="alert-info"><a href="{{URL::to('/admin/countries')}}">Go back</a></span>
                   
                           </header>
                           <div class="panel-body">
                               <div class="adv-table">
                                   {{ Form::open(array('action' => 'CountryController@store', 'method' => 'post')) }}

                                   <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                       {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Country Name')) }}
                                       {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                   </div>

                                   

                                    {{ Form::submit('Create Country', array('class' => 'btn btn-primary')) }}
                                   {{ Form::close() }}
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
               <!-- page end-->
           </section>
       </section>
       <!--main content end-->
@stop
