@extends('backend.layouts.login')

{{-- Web site Title --}}
@section('title')
{{trans('pages.login')}}
@stop

{{-- Content --}}
@section('content')
<div class="container">

      {{ Form::open(array('action' => 'SessionController@store', 'class' => 'form-signin')) }}
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
           <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
               {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => trans('admin.email'), 'autofocus')) }}
               {{ ($errors->has('email') ? $errors->first('email') : '') }}
           </div>

           <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
               {{ Form::password('password', array('class' => 'form-control', 'placeholder' => trans('admin.pword')))}}
               {{ ($errors->has('password') ?  $errors->first('password') : '') }}
           </div>
            <label class="checkbox">
                    {{ Form::checkbox('rememberMe', 'rememberMe') }} {{trans('admin.remember')}}

                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            {{ Form::submit(trans('pages.login'), array('class' => 'btn btn-lg btn-login btn-block'))}}
            {{ Form::close() }}
        </div>

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      {{ Form::open(array('action' => 'UserController@forgot', 'method' => 'post')) }}
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                          {{ Form::text('email', null, array('class' => 'form-control placeholder-no-fix', 'placeholder' => trans('users.email'), 'autofocus')) }}
                          {{ ($errors->has('email') ? $errors->first('email') : '') }}
                        </div>
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          {{ Form::submit(trans('users.resendpword'), array('class' => 'btn btn-success'))}}
                      </div>
                  </div>

                {{ Form::close() }}
              </div>
          </div>
          <!-- modal -->

      </form>

    </div>

@stop