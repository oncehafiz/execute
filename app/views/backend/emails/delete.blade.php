@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Coach
@stop

{{-- Content --}}
@section('content')
  @include('backend/_includes/admin_header_menu')
   @include('backend/_includes/left_side_bar')
       <!--main content start-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
         <!--mail inbox start-->
                      <div class="mail-box">
                          <aside class="sm-side">
                              <div class="user-head">
                                  <a href="javascript:;" class="inbox-avatar">
                                      <img src="{{ asset($coach->profile_image) }}" alt="">
                                  </a>
                                  <div class="user-name">
                                      <h5><a href="#">{{ $coach->first_name.' '.$coach->last_name  }}</a></h5>
                                      <span><a href="#">{{ $coach->email  }}</a></span>
                                  </div>
                                  <a href="javascript:;" class="mail-dropdown pull-right">
                                      <i class="fa fa-chevron-down"></i>
                                  </a>
                              </div>

                              <div class="inbox-body">
                                  <a class="btn btn-compose" data-toggle="modal" href="#myModal">
                                      Compose
                                  </a>
                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                  <h4 class="modal-title">Compose</h4>
                                              </div>
                                              <div class="modal-body">
                                                  <form class="form-horizontal" role="form">
                                                      <div class="form-group">
                                                          <label  class="col-lg-2 control-label">To</label>
                                                          <div class="col-lg-10">
                                                              <input type="text" class="form-control" id="inputEmail1" placeholder="">
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label  class="col-lg-2 control-label">Cc / Bcc</label>
                                                          <div class="col-lg-10">
                                                              <input type="text" class="form-control" id="cc" placeholder="">
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-lg-2 control-label">Subject</label>
                                                          <div class="col-lg-10">
                                                              <input type="text" class="form-control" id="inputPassword1" placeholder="">
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-lg-2 control-label">Message</label>
                                                          <div class="col-lg-10">
                                                              <textarea name="" id="" class="form-control" cols="30" rows="10"></textarea>
                                                          </div>
                                                      </div>

                                                      <div class="form-group">
                                                          <div class="col-lg-offset-2 col-lg-10">
                                                              <span class="btn green fileinput-button">
                                                                <i class="fa fa-plus fa fa-white"></i>
                                                                <span>Attachment</span>
                                                                <input type="file" multiple=""  name="files[]">
                                                              </span>
                                                              <button type="submit" class="btn btn-send">Send</button>
                                                          </div>
                                                      </div>
                                                  </form>
                                              </div>
                                          </div><!-- /.modal-content -->
                                      </div><!-- /.modal-dialog -->
                                  </div><!-- /.modal -->
                              </div>

                              <ul class="inbox-nav inbox-divider">

                                  <li>
                                      <a href="{{ action('AdminUserController@emailRequest', array(Session::get('userId'))) }}"><i class="fa fa-inbox"></i> Inbox <span class="label label-danger pull-right">{{ $unreadCount }}</span></a>

                                  </li>

                                  <li >
                                      <a href="{{ action('AdminUserController@emailSent', array(Session::get('userId'))) }}"><i class="fa fa-envelope-o"></i> Sent Mail<span class="label label-danger pull-right">{{ $sentEmails }}</span></a>
                                  </li>

                              </ul>

                          </aside>
                          <aside class="lg-side">
                              <div class="inbox-head">
                                  <h3>Inbox</h3>
                                  <form class="pull-right position" action="#">
                                      <div class="input-append">
                                          <input type="text"  placeholder="Search Mail" class="sr-input">
                                          <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
                                      </div>
                                  </form>
                              </div>
                              <div class="inbox-body">
                                 <div class="mail-option">
                                     <div class="chk-all">
                                         <input type="checkbox" class="mail-checkbox mail-group-checkbox">
                                         <div class="btn-group" >
                                             <a class="btn mini all" href="#" data-toggle="dropdown">
                                                 All
                                                 <i class="fa fa-angle-down "></i>
                                             </a>
                                             <ul class="dropdown-menu">
                                                 <li><a href="#"> None</a></li>
                                                 <li><a href="#"> Read</a></li>
                                                 <li><a href="#"> Unread</a></li>
                                             </ul>
                                         </div>
                                     </div>

                                     <div class="btn-group">
                                         <a class="btn mini tooltips" href="javascript:;" onclick="location.reload();" data-toggle="dropdown" data-placement="top" data-original-title="Refresh">
                                             <i class=" fa fa-refresh"></i>
                                         </a>
                                     </div>
                                     <div class="btn-group hidden-phone">
                                         <a class="btn mini blue" href="#" data-toggle="dropdown">
                                             More
                                             <i class="fa fa-angle-down "></i>
                                         </a>
                                         <ul class="dropdown-menu">
                                             <li><a href="#"><i class="fa fa-pencil"></i> Mark as Read</a></li>
                                             <li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                         </ul>
                                     </div>


                                 </div>

                                  <table class="table table-inbox table-hover">
                                    <tbody>
                                    @if(count($emails) > 0)
                                        @foreach($emails as $email)
                                            <tr class="{{ ($email->status == '0') ? 'unread' : '' }}">
                                              <td class="inbox-small-cells">
                                                  <input type="checkbox" class="mail-checkbox">
                                              </td>
                                              <td class="inbox-small-cells"><i class="fa fa-star"></i></td>
                                              <td class="view-message  dont-show" onclick="location.href= '{{ action('AdminUserController@emailDetail', array('id' => $email->id, 'email_id' =>  $coach->id)) }}'">{{ ucfirst($email->sender_name) }}</td>
                                              <td class="view-message " onclick="location.href= '{{ action('AdminUserController@emailDetail', array('id' => $email->id, 'email_id' =>  $coach->id)) }}'">{{ ucfirst(substr($email->message, 0, 60)) }}...</td>
                                              <td class="view-message  text-right">{{ date("F jS, Y",strtotime($email->created_at)); }}</td>
                                          </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="6" class="center">Emails are not founds</td></tr>
                                    @endif
                                  </tbody>
                                  </table>
                                   {{ $emails->links() }}
                              </div>
                          </aside>
                      </div>
                      <!--mail inbox end-->
    </section>    
</section>
<!--main content end-->
@stop
