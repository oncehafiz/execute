@extends('backend.layouts.default')

{{-- Web site Title --}}
@section('title')
Edit Coach
@stop

{{-- Content --}}
@section('content')
  @include('backend/_includes/admin_header_menu')
   @include('backend/_includes/left_side_bar')
       <!--main content start-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
         <!--mail inbox start-->
                      <div class="mail-box">
                          <aside class="sm-side">
                            <div class="user-head">
                                <a href="javascript:;" class="inbox-avatar">
                                    <img src="{{ asset($email->requestedCoach->profile_image) }}" alt="">
                                </a>
                                <div class="user-name">
                                    <h5><a href="#">{{ $email->requestedCoach->first_name.' '.$email->requestedCoach->last_name  }}</a></h5>
                                    <span><a href="#">{{ $email->requestedCoach->email  }}</a></span>
                                </div>
                                <a href="javascript:;" class="mail-dropdown pull-right">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>

                            <div class="inbox-body">
                                <a class="btn btn-compose" data-toggle="modal" href="#myModal">
                                    Compose
                                </a>
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Compose</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label  class="col-lg-2 control-label">To</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" name="email" value="{{ $email->sender_email }}" class="form-control" id="inputEmail1" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label  class="col-lg-2 control-label">Cc / Bcc</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control" id="cc" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">Subject</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control" id="inputPassword1" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">Message</label>
                                                        <div class="col-lg-10">
                                                            <textarea name="" id="" class="form-control" cols="30" rows="10"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-lg-offset-2 col-lg-10">
                                                            <span class="btn green fileinput-button">
                                                              <i class="fa fa-plus fa fa-white"></i>
                                                              <span>Attachment</span>
                                                              <input type="file" multiple=""  name="files[]">
                                                            </span>
                                                            <button type="submit" class="btn btn-send">Send</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </div>

                            <ul class="inbox-nav inbox-divider">

                                <li>
                                    <a href="{{ action('AdminUserController@emailRequest', array(Session::get('userId'))) }}"><i class="fa fa-inbox"></i> Inbox <span class="label label-danger pull-right">{{ (isset($unreadCount) ? $unreadCount : '') }}</span></a>

                                </li>

                                <li >
                                    <a href="{{ action('AdminUserController@emailSent', array(Session::get('userId'))) }}"><i class="fa fa-envelope-o"></i> Sent Mail<span class="label label-danger pull-right">{{ isset($sentEmails) ? $sentEmails : '' }}</span></a>
                                </li>

                            </ul>

                        </aside>
                          <aside class="lg-side">
                                                <div class="inbox-head">
                                                    <h3>View Mail</h3>
                                                    <form class="pull-right position" action="#">
                                                        <div class="input-append">
                                                            <input type="text"  placeholder="Search Mail" class="sr-input">
                                                            <button type="button" class="btn sr-btn"><i class="fa fa-search"></i></button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="inbox-body">
                                                        <div class="heading-inbox row">
                                                            <div class="col-md-8">
                                                                <div class="compose-btn">
                                                                    {{ Form::open(array('action' => 'AdminUserController@deleteEmail', 'class' => 'inline-form')) }}
                                                                        {{ Form::hidden('emails[id][]', $email->id) }}
                                                                         {{ Form::hidden('coach_id', $email->requestedCoach->id) }}
                                                                        <a class="btn btn-sm btn-primary" data-toggle="modal" href="#myModal"><i class="fa fa-reply"></i> Reply</a>
                                                                        <button type="submit" name="type" value="delete" title="" data-placement="top" data-toggle="tooltip" data-original-title="Trash" class="btn btn-sm tooltips" onclick="{{ action('AdminUserController@destroy', array($email->id)) }}"><i class="fa fa-trash-o"></i></button>
                                                                     {{ Form::close() }}
                                                                </div>

                                                            </div>
                                                            <div class="col-md-4 text-right">
                                                                <p class="date"> {{ $email->created_at }}</p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <h4> {{ $email->sender_name }}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="sender-info">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    @if($email == 'sent')
                                                                        <strong>{{ $email->requestedCoach->first_name.' '.$email->requestedCoach->last_name }}</strong>
                                                                        <span>[{{ $email->sender_email }}]</span>
                                                                        to
                                                                        <strong>{{ $email->sender_name }}</strong>
                                                                    @else
                                                                         <strong>{{ $email->sender_name }}</strong>
                                                                        <span>[{{ $email->sender_email }}]</span>
                                                                        to
                                                                        <strong>Me</strong>
                                                                    @endif
                                                                    <a class="sender-dropdown " href="javascript:;">
                                                                        <i class="fa fa-chevron-down"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="view-mail">
                                                            <p>{{ $email->message }}</p>
                                                        </div>
                                            </aside>
                      </div>
                      <!--mail inbox end-->
    </section>    
</section>
<!--main content end-->
<script src="{{URL::asset('backend/assets/js/advanced-form-components.js')}}"></script>
@stop
