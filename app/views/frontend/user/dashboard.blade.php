
@extends('layouts.logged_layout')

{{-- Web site Title --}}
@section('title')
@parent
Execyoution | Dashboard
@stop

{{-- Content --}}
@section('content')

<div class="main pages-main">
    <div class="container">
        <div class="m1 module">
            <nav class="breadcrumb m4 show-lg">Dashboard&nbsp;&nbsp;›&nbsp;&nbsp;<a href="{{ URL::to('/') }}/{{strtolower(str_replace(" ","-",Session::get('username')))}}/smartsheet" title="{{trans("pages.smartsheet")}}">{{trans("pages.smartsheet")}}</a></nav>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="section_box">
                        <h2>Videos</h2>
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <!--
  <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
  -->

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class='embed-container'>
                                        <iframe src='http://player.vimeo.com/video/66140585' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                    </div>

                                </div>
                                <div class="item">
                                    <div class='embed-container'>
                                        <iframe src='http://player.vimeo.com/video/66140123' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                                    </div>

                                </div>

                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                    <div class="section_box">
                        <h2>Form/html</h2>
                        <div id="carousel-2" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <!--
  <ol class="carousel-indicators">
                              <li data-target="#carousel-2" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-2" data-slide-to="1"></li>
                              <li data-target="#carousel-2" data-slide-to="2"></li>
                            </ol>
  -->

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="http://placehold.it/350x195" style="width: 100%;" alt="...">

                                </div>
                                <div class="item">
                                    <img src="http://placehold.it/350x195" style="width: 100%;" alt="...">

                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-2" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-2" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="section_box">
                        <h2>Reports</h2>
                        <div id="carousel-3" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <!--
  <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
  -->

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div id="chart-container" style="height:250px;"></div>
                                </div>
                                <div class="item">
                                    <div id="bar-chart-container" style="height:250px;"></div>

                                </div>

                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-3" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-3" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                    <div class="section_box">
                        <h2>Videos</h2>
                        <div id="carousel-4" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <!--
  <ol class="carousel-indicators">
                              <li data-target="#carousel-2" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-2" data-slide-to="1"></li>
                              <li data-target="#carousel-2" data-slide-to="2"></li>
                            </ol>
  -->

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="http://placehold.it/350x180" style="width: 100%;" alt="...">

                                </div>
                                <div class="item">
                                    <img src="http://placehold.it/350x180" style="width: 100%;" alt="...">

                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-4" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-4" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@stop