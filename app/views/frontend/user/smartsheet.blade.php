
@extends('layouts.logged_layout')

{{-- Web site Title --}}
@section('title')
@parent
Execyoution | Smartsheet
@stop

{{-- Content --}}
@section('content')

<div class="main pages-main">
    <div class="container">
        <div class="m1 module">
            <nav class="breadcrumb m4 show-lg">Dashboard&nbsp;&nbsp;›&nbsp;&nbsp;<a href="{{ URL::to('/') }}/{{strtolower(str_replace(" ","-",Session::get('username')))}}/smartsheet" title="{{trans("pages.smartsheet")}}">{{trans("pages.smartsheet")}}</a></nav>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="advance-table-container">
                        <div class="table-inner">
                            <div class="table-header">
                                <table class="table">
                                    <tr class="first">
                                        <td class="sr-no">&nbsp;</td>
                                        @if(Session::get('user_type')!=4)
                                        <td class="small"><i class="fa fa-bullseye"></i></td>
                                        @endif
                                        <td class="small"><i style="visibility: hidden;" class="fa fa-caret-square-o-down"></i></td>
                                        <td class="small"><i class="fa fa-link"></i></td>
                                        <td class="small"><i class="fa fa-comment-o"></i></td>

                                        <td class="large">Area / Goal / Measure</td>
                                        <td>Target</td>
                                        <td>Status</td>
                                        <td>Sponsor</td>
                                        <td>Project Links</td>
                                        <td>Actions</td>
                                    </tr>

                                </table>


                            </div>

                            <div class="table-body">
                                @if (isset($perspective) && !empty($perspective))
                                <?php $count = 1; ?>

                                <table class="table table-condensed">
                                    @foreach($perspective as $p)
                                    <tr data-toggle="collapse" data-target=".hidden{{$p->created_at}}">
                                        <td class="empty-td">&nbsp;</td>
                                        <td class="sr-no">{{$count}}</td>
                                        @if(Session::get('user_type')!=4)
                                        <td class="small">
                                            <a href="#"  data-toggle="modal" data-target=".add_goalds{{$p->id}}">
                                                <i class="fa fa-bullseye"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade add_goalds{{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">

                                                            <div class="panel-body">
                                                                <div class="adv-table">
                                                                    {{ Form::open(array('action' => 'GoalController@store')) }}
                                                                    <h2>Add goals</h2>

                                                                    <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                                                                        {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => trans('pages.goals_title'))) }}
                                                                        {{ ($errors->has('title') ? $errors->first('title') : '') }}
                                                                    </div>
                                                                    <div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
                                                                        {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description')) }}
                                                                        {{ ($errors->has('description') ? $errors->first('description') : '') }}
                                                                    </div>
                                                                    {{ Form::hidden('persp_id', $p->id) }}
                                                                    {{ Form::hidden('company_id', Session::get('company_id')) }}
                                                                    {{ Form::hidden('dtp_id', Session::get('dtp_id')) }}
                                                                    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                                                                    {{ Form::close() }}
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        @endif


                                        <td class="small">
                                            <div class="dropdown">
                                                <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                                    <i class="fa fa-caret-square-o-down"></i>
                                                </a>

                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                    <li><a href="#">Insert row before</a></li>
                                                    <li><a href="#">Insert row after</a></li>


                                                </ul>
                                            </div>
                                        </td>
                                        <td class="small">
                                            <a href="#"  data-toggle="modal" data-target=".{{$p->id}}">
                                                <i class="fa fa-link"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade {{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 class="modal-title" id="myModalLabel">Row 24 Attachments (0)</h4>
                                                            <div class="dropdown">
                                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Attach
                                                                    <span class="caret"></span>
                                                                </button>

                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                    <li><a href="#"><i class="fa fa-lg fa-upload"></i> Upload from computer</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Google Drive</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Box</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Dropbox</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Evernote</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Link (URL)</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="drop-files">
                                                                <div class="aligncenter">
                                                                    <i class="fa fa-upload fa-4x"></i>
                                                                </div>
                                                                <p>Drop here to attach them to this row</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                        <td class="small">

                                            <a href="#"  data-toggle="modal" data-target=".discussion-modal{{$p->id}}">
                                                <i class="fa fa-comment-o"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade discussion-modal{{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 class="modal-title" id="myModalLabel">New Discussion</h4>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Discussion Title">
                                                            </div>


                                                            <div class="form-group">
                                                                <textarea class="comment-box" placeholder="Add your new comment"></textarea>
                                                            </div>

                                                            <div class="dropdown">
                                                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-link"></i> Attach
                                                                    </button>

                                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                        <li><a href="#"><i class="fa fa-lg fa-upload"></i> Upload from computer</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Google Drive</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Box</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Dropbox</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Evernote</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Link (URL)</a></li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </td>
                                        <td class="large parent-node">{{$p->busp_name}}

                                        </td>
                                        <td>Target</td>
                                        <td>Status</td>
                                        <td>Sponsor</td>
                                        <td>Project Links</td>
                                        <td>Actions</td>
                                    </tr>
                                    <?php
                                    $company_goals = Goals::where('company_id', '=', Session::get('userId'))->where('persp_id', '=', $p->id)->get();
                                    if (!empty($company_goals)) :                                    
                                        ?>
                                        @foreach($company_goals as $goal)
                                        <tr>
                                            <td class="empty-td hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    &nbsp;
                                                </div>
                                            </td>
                                            <td class="sr-no hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    {{$count}}
                                                </div>
                                            </td>
                                            @if(Session::get('user_type')!=4)
                                            <td class="small hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    <a href="#"  data-toggle="modal" data-target=".add_goalds{{$p->id}}">
                                                        <i class="fa fa-bullseye"></i>
                                                    </a>
                                                    <!-- Modal -->
                                                    <div class="modal fade add_goalds{{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                </div>
                                                                <div class="modal-body">

                                                                    <div class="panel-body">
                                                                        <div class="adv-table">
                                                                            {{ Form::open(array('action' => 'GoalController@store')) }}
                                                                            <h2>Add goals</h2>

                                                                            <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                                                                                {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => trans('pages.goals_title'))) }}
                                                                                {{ ($errors->has('title') ? $errors->first('title') : '') }}
                                                                            </div>
                                                                            <div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
                                                                                {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description')) }}
                                                                                {{ ($errors->has('description') ? $errors->first('description') : '') }}
                                                                            </div>
                                                                            {{ Form::hidden('persp_id', $p->id) }}
                                                                            {{ Form::hidden('company_id', Session::get('company_id')) }}
                                                                            {{ Form::hidden('dtp_id', Session::get('dtp_id')) }}
                                                                            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                                                                            {{ Form::close() }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            @endif


                                            <td class="small hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    <div class="dropdown">
                                                    <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                                        <i class="fa fa-caret-square-o-down"></i>
                                                    </a>

                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                        <li><a href="#">Insert row before</a></li>
                                                        <li><a href="#">Insert row after</a></li>


                                                    </ul>
                                                </div>
                                                </div>
                                            </td>
                                            <td class="hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                     <a href="#"  data-toggle="modal" data-target=".{{$p->id}}">
                                                <i class="fa fa-link"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade {{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 class="modal-title" id="myModalLabel">Row 24 Attachments (0)</h4>
                                                            <div class="dropdown">
                                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Attach
                                                                    <span class="caret"></span>
                                                                </button>

                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                    <li><a href="#"><i class="fa fa-lg fa-upload"></i> Upload from computer</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Google Drive</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Box</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Dropbox</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Evernote</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Link (URL)</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="drop-files">
                                                                <div class="aligncenter">
                                                                    <i class="fa fa-upload fa-4x"></i>
                                                                </div>
                                                                <p>Drop here to attach them to this row</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                                </div>
                                            </td>
                                           <td class="small hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                            <a href="#"  data-toggle="modal" data-target=".discussion-modal{{$p->id}}">
                                                <i class="fa fa-comment-o"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade discussion-modal{{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 class="modal-title" id="myModalLabel">New Discussion</h4>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Discussion Title">
                                                            </div>


                                                            <div class="form-group">
                                                                <textarea class="comment-box" placeholder="Add your new comment"></textarea>
                                                            </div>

                                                            <div class="dropdown">
                                                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-link"></i> Attach
                                                                    </button>

                                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                        <li><a href="#"><i class="fa fa-lg fa-upload"></i> Upload from computer</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Google Drive</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Box</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Dropbox</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Evernote</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Link (URL)</a></li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                                </div>
                                        </td>
                                        <td class="large">
                                             <div class="hidden{{$p->created_at}}">
                                                 <strong>{{$goal->title}}</strong>
                                             </div>
                                        </td>
                                        <td class="">
                                             <div class="collapse hidden{{$p->created_at}}">
                                                 Target
                                             </div>
                                        </td>
                                        <td class="">
                                            <div class="collapse hidden{{$p->created_at}}">
                                                Status
                                            </div>
                                        </td>
                                        <td class="">
                                            <div class="collapse hidden{{$p->created_at}}">
                                                Sponsor
                                            </div>
                                        </td>
                                        <td class="">
                                            <div class="collapse hidden{{$p->created_at}}">
                                                Project Links
                                            </div>
                                        </td>
                                        <td class="">
                                             <div class="collapse hidden{{$p->created_at}}">
                                                 Actions
                                             </div>
                                        </td>
                                        </tr>
                                        
                                        <?php
                                    $target = Targets::where('goal_id', '=', $goal->id)->get();
                                    
                                    if (!empty($target)) :
                                        
                                        ?>
                                        @foreach($target as $t)
                                         <tr>
                                            <td class="empty-td hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    &nbsp;
                                                </div>
                                            </td>
                                            <td class="sr-no hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    {{$count}}
                                                </div>
                                            </td>
                                            @if(Session::get('user_type')!=4)
                                            <td class="small hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    <a href="#"  data-toggle="modal" data-target=".add_goalds{{$p->id}}">
                                                        <i class="fa fa-bullseye"></i>
                                                    </a>
                                                    <!-- Modal -->
                                                    <div class="modal fade add_goalds{{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                </div>
                                                                <div class="modal-body">

                                                                    <div class="panel-body">
                                                                        <div class="adv-table">
                                                                            {{ Form::open(array('action' => 'GoalController@store')) }}
                                                                            <h2>Add goals</h2>

                                                                            <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                                                                                {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => trans('pages.goals_title'))) }}
                                                                                {{ ($errors->has('title') ? $errors->first('title') : '') }}
                                                                            </div>
                                                                            <div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
                                                                                {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description')) }}
                                                                                {{ ($errors->has('description') ? $errors->first('description') : '') }}
                                                                            </div>
                                                                            {{ Form::hidden('persp_id', $p->id) }}
                                                                            {{ Form::hidden('company_id', Session::get('company_id')) }}
                                                                            {{ Form::hidden('dtp_id', Session::get('dtp_id')) }}
                                                                            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                                                                            {{ Form::close() }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            @endif


                                            <td class="small hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                    <div class="dropdown">
                                                    <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                                        <i class="fa fa-caret-square-o-down"></i>
                                                    </a>

                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                        <li><a href="#">Insert row before</a></li>
                                                        <li><a href="#">Insert row after</a></li>


                                                    </ul>
                                                </div>
                                                </div>
                                            </td>
                                            <td class="hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                                     <a href="#"  data-toggle="modal" data-target=".{{$p->id}}">
                                                <i class="fa fa-link"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade {{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 class="modal-title" id="myModalLabel">Row 24 Attachments (0)</h4>
                                                            <div class="dropdown">
                                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Attach
                                                                    <span class="caret"></span>
                                                                </button>

                                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                    <li><a href="#"><i class="fa fa-lg fa-upload"></i> Upload from computer</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Google Drive</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Box</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Dropbox</a></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Evernote</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="#"><i class="fa fa-upload"></i> Link (URL)</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="drop-files">
                                                                <div class="aligncenter">
                                                                    <i class="fa fa-upload fa-4x"></i>
                                                                </div>
                                                                <p>Drop here to attach them to this row</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                                </div>
                                            </td>
                                           <td class="small hiddenRow">
                                                <div class="collapse hidden{{$p->created_at}}">
                                            <a href="#"  data-toggle="modal" data-target=".discussion-modal{{$p->id}}">
                                                <i class="fa fa-comment-o"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade discussion-modal{{$p->id}}" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4 class="modal-title" id="myModalLabel">New Discussion</h4>

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Discussion Title">
                                                            </div>


                                                            <div class="form-group">
                                                                <textarea class="comment-box" placeholder="Add your new comment"></textarea>
                                                            </div>

                                                            <div class="dropdown">
                                                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-link"></i> Attach
                                                                    </button>

                                                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                                                        <li><a href="#"><i class="fa fa-lg fa-upload"></i> Upload from computer</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Google Drive</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Box</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Dropbox</a></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Evernote</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="fa fa-upload"></i> Link (URL)</a></li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                                </div>
                                        </td>
                                        <td class="large">
                                             <div class="hidden{{$p->created_at}}">
                                                 <i class="fa fa-arrow-circle-o-right"></i>{{$t->target_title}}
                                             </div>
                                        </td>
                                        <td class="">
                                             <div class="collapse hidden{{$p->created_at}}">
                                                 Target
                                             </div>
                                        </td>
                                        <td class="">
                                            <div class="collapse hidden{{$p->created_at}}">
                                                Status
                                            </div>
                                        </td>
                                        <td class="">
                                            <div class="collapse hidden{{$p->created_at}}">
                                                Sponsor
                                            </div>
                                        </td>
                                        <td class="">
                                            <div class="collapse hidden{{$p->created_at}}">
                                                Project Links
                                            </div>
                                        </td>
                                        <td class="">
                                             <div class="collapse hidden{{$p->created_at}}">
                                                 Actions
                                             </div>
                                        </td>
                                        
                                        </tr>
                                        @endforeach
                                        <?php endif; ?>
                                        @endforeach
                                    <?php endif; ?>
                                    <?php $count++; ?>
                                    @endforeach
                                </table>
                                @endif
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@stop