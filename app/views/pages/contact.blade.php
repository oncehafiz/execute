@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.helloworld')}}
@stop

{{-- Content --}}
@section('content')



         <!-- CONTACT -->
         <section id="main-body">

            <div id="gmap_canvas"></div>

             <div id="contact-icon" class="text-center">
                <img src="{{ asset('frontend/img/contact-title-icon.png') }}" alt="">
             </div>

             <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">

                        <div class="section-title">
                            <h2 class="uppercase text-center">contact us</h2>
                            <p class="subtitle text-center">Phasellus placerat, metus sed condimentum luctus, felis risus aliquam ipsum, quis. Nullam imperdiet iaculis mi, sagittis hendrerit tortor sollicitudin sit amet.</p>
                         </div>

                         <!-- form -->
                         <div id="form">
                            <span class="message-status"></span>
                        
                            {{ Form::open(array('id'=>'contact_email_request_form')) }}
                            {{ Form::hidden('type', 'contact', array('id' => 'type')) }}
                            <div class="row">
                                <div class="col-lg-4">
                                    {{ Form::text('contact_name', Input::old('contact_name'), array('class'=>'form-control', 'id' => 'contact_name', 'placeholder' => 'Your Name*')) }}
                                </div>
                                <div class="col-lg-4">
                                    {{ Form::text('contact_email', Input::old('contact_email'), array('class'=>'form-control', 'id' => 'contact_email', 'placeholder' => 'Your Email Address*')) }}
                                </div>
                                <div class="col-lg-4">
                                    {{ Form::text('contact_phone', Input::old('contact_phone'), array('class'=>'form-control', 'id' => 'contact_phone', 'placeholder' => 'Your Phone Number (optional)')) }}
                                </div>
                                <div class="col-lg-12 text-center">
                                    {{ Form::textarea('contact_message', Input::old('contact_message'), array('class'=>'form-control', 'id' => 'contact_message', 'placeholder' => 'Your Message*')) }}
                                    <button type="button" class="hexagon-button" onclick="modelBox.contact_send();">Send<span></span></button>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                     </div>
                 </div>
             </div>

         </section>
 
@stop