@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.helloworld')}}
@stop

{{-- Content --}}
@section('content')

 <!-- TOP -->


<!-- CONTACT -->
<section id="main-body">

 <div class="container">
 	<div class="row">
     	<div class="col-lg-12 col-md-12 col-sm-12">

				<div class="section-title">
					<h2 class="uppercase">{{ $page->title }}</h2>
					<p class="subtitle">{{ $page->content }}</p>
             </div>
         </div>
     </div>
 </div>

</section>
 
@stop