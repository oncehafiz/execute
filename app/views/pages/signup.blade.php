@extends('layouts.default')

{{-- Web site Title --}}
@section('title')
@parent
{{trans('pages.register')}}
@stop

{{-- Content --}}
@section('content')



         <!-- CONTACT -->
         <section id="main-body">        

             <div class="container">
                <div class="row">
                 @include('layouts.notifications')
                    <div class="col-lg-8 col-lg-offset-2">
                    {{ Breadcrumbs::render() }}
                        <div class="section-title">
                            <h2 class="uppercase text-center">{{trans('pages.register_new')}}</h2>
                            <p class="subtitle text-center">Enter below information to signup with us</p>
                         </div>

                         <!-- form -->
                         <div id="form">
                            <span class="message-status"></span>
                        
                           
                            {{ Form::open(array('action' => 'UserController@register', 'id' => 'contact_email_request_form')) }}                                
                                
                                <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
                                    {{ Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => trans('users.fname'))) }}
                                    {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                                </div>
                                
                                <div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
                                    {{ Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => trans('users.lname'))) }}
                                    {{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}
                                </div>

                                <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                                    {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => trans('users.email'))) }}
                                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                </div>

                                <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                                    {{ ($errors->has('password') ?  $errors->first('password') : '') }}
                                </div>

                                <div class="form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm Password')) }}
                                    {{ ($errors->has('password_confirmation') ?  $errors->first('password_confirmation') : '') }}
                                </div>
                           
                                 <div class="col-lg-12 text-center">
                                 <button type="submit" class="hexagon-button">Register<span></span></button>
                                </div>
                            {{ Form::close() }}
                    
                        </div>
                     </div>
                 </div>
             </div>

         </section>
 
@stop