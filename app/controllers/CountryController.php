<?php

class CountryController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public static $rules = [
        'name' => 'required|unique:country',
    ];

    public function index() {
        $country = Country::paginate(15);
        return View::make('backend.country.index')->with('country', $country);
    }

    public function edit($id) {
        $country = Country::find($id);
        return View::make('backend.country.edit')->with('country', $country);
    }

    public function destroy($id) {
        
        $country = Country::find($id);
        if(!empty($country->id))
        {
            
            $country->delete();
        }
        return Redirect::to('admin/countries');
    }

    public function update() {
        $validator = Validator::make(Input::all(), self::$rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::route('edit.country', array(Input::get('id')))
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput();
        } else {
            Country::where('id', '=', Input::get('id'))->update(array('name' => Input::get('name')));
            return Redirect::route('edit.country', array(Input::get('id')))->with('message','Updated!');
        }
    }

    /**
     * Ajax country
     */
    public function getCities($id) {


        $positions = DB::table('cities')->select('id', 'name')->where('country_id', '=', $id)->get();
        return Response::json($positions);
    }

    /**
     * save data to db
     */
    public function store() {

        $validator = Validator::make(Input::all(), self::$rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('admin/country/create')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $country = New Country;
            $country->name = Input::get('name');
            $country->save();
            return Redirect::route('edit.country', array($country->id));
        }
    }

}
