<?php

class ColorController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $colorcode = ColorCode::all();
	    return View::make('backend.colorcode.index')
                    ->with('colors', $colorcode);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('backend.colorcode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $colorcode = new ColorCode();
        $data = Input::All();

        // attempt validation
        if ($colorcode->validate($data)) {

            $last_id = ColorCode::create($data);
            //Session::flash('success', 'Updated');

            return Redirect::action('ColorController@index', array())
                            ->withInput($data)
                            ->with('message', 'Created');
        } else {


            return Redirect::action('ColorController@create', array())
                            ->withInput($data)
                            ->withErrors($colorcode->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if ($id == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        
       
       
        $code = ColorCode::find($id);
        
       
       Breadcrumbs::addCrumb('Home', url('admin'));
       Breadcrumbs::addCrumb('Color Code', URL::to('admin/colorcode'));
       Breadcrumbs::addCrumb('Edit Color Code', URL::to('admin/colorcode/'.$id.'/edit'));
       Breadcrumbs::setDivider('»');

        return View::make('backend.colorcode.edit')
                       
                        ->with('color', $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }
        $code = new ColorCode();
        $data = Input::All();
        // attempt validation
        if ($code->validate($data,true)) {
            //Session::flash('success', 'Updated');
            
            ColorCode::where('id', '=', Input::get('id'))->update(array(
                
                'name'     => Input::has('name') ? Input::get('name') : "#f9f9f9",
                
            ));

            return Redirect::action('ColorController@edit', array($id))
                            ->withInput($data)
                            ->with('message','Updated');
        } else {
            
            
            return Redirect::action('ColorController@edit', array($id))
                            ->withInput($data)
                            ->withErrors($code->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        DB::table('color_code')->where('id', '=', $id)->delete();
        return Redirect::action('ColorController@index', array())
                            
                            ->with('message', 'Deleted');
    }

    

}
