<?php

class BehaviorsTypeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected  $behaviorType;

	public function __construct(BehaviorType $behaviorType) {
		$this->behaviorType = $behaviorType;
	}

	public function index()
	{
		$behaviorType = $this->behaviorType->all();
      
        return View::make('backend.behaviorType.index')->with('behaviorType', $behaviorType);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.behaviorType.new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$this->behaviorType->fill($input);

		if( !$this->behaviorType->isValid() ) {
			return Redirect::back()->withInput()->withErrors($this->behaviorType->errors);
		}

		$this->behaviorType->create($input);

		return Redirect::route('admin.behaviorType.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$behaviorType = $this->behaviorType->find($id);

        if($behaviorType == null || !is_numeric($id))
        {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

		return View::make('backend.behaviorType.edit')->with('behaviorType', $behaviorType);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!is_numeric($id))
        {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $input = Input::only('name', 'description');

		$this->behaviorType->fill($input);

		if( !$this->behaviorType->isValid() ) {
			return Redirect::back()->withInput()->withErrors($this->behaviorType->errors);
		}

		$this->behaviorType->update($input);

		return Redirect::route('admin.behaviorType.index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(!is_numeric($id))
        {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

		if ($this->behaviorType->destroy($id))
		{
			Session::flash('success', 'Behavior Type has been deleted.');
            return Redirect::route('admin.behaviorType.index');
        }
        else 
        {
        	Session::flash('error', 'Unable to Delete Behavior Type');
            return Redirect::route('admin.behaviorType.index');
        }
	}


}
