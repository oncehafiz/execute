<?php

class ServiceWrapper extends \BaseController {
    /*
      |--------------------------------------------------------------------------
      | Service Wrapper Controller
      |--------------------------------------------------------------------------
      |
      | you can call remote server api's by using this controller
      |
     */

   public static function executeGet($url)
{
    $ch = curl_init();
    
    if(Session::has('token'))
    $url .= '?token='.Session::get('token');

    curl_setopt($ch,CURLOPT_URL,$url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    curl_close($ch);
     if ( $httpCode != 200 )
    {
        return $response;
    }
    else
    {
       return $response;
    }
    
}

public static function executePost($url, $data)
{
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    
    if(Session::has('token'))
    $data['token'] = Session::get('token');
    
    $requestData = http_build_query($data,'','&');
    curl_setopt($ch,CURLOPT_POSTFIELDS,$requestData);
    curl_setopt($ch,CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ( $httpCode != 200 )
    {
        return $response;
    }
    else
    {
       return $response;
    }
   
}

public static function executePut($url, $data)
{
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    
    if(Session::has('token'))
    $data['token'] = Session::get('token');
    
    $requestData = http_build_query($data,'','&');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS,$requestData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
     curl_close($ch);
     
    if ( $httpCode != 200 )
    {
        return $response;
    }
    else
    {
        return $response;
    }
  
}

public static function executeDelete($url,$data)
{
    
    if(Session::has('token'))
    $data['token'] = Session::get('token');
    
    $requestData = http_build_query($data,'','&');
        
    $ch = curl_init($url."?".$requestData);
    //echo PHP_EOL. 'URL(DELETE) = '.$url . PHP_EOL. PHP_EOL;
   /* curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_CUSTOMREQUEST,'DELETE');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);*/
    
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
        
    
    $httpCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    echo PHP_EOL;
    if ( $httpCode != 200 )
    {
        return $response;
        //echo 'Error code = '.$httpCode. PHP_EOL;
    }
    else
    {
        return $response;
    }
    curl_close($ch);
}

}
