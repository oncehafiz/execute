<?php

class PerspectiveController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $perspective;

	public function __construct(BusinessPerspectives $perspective) {

		$this->perspective  = $perspective;

	}

	public function index()
	{
		$perspective = $this->perspective->all();
      
        return View::make('backend.perspective.index')->with('perspective', $perspective);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.perspective.new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$this->perspective->fill($input);

		if( !$this->perspective->isValid() ) {
			return Redirect::back()->withInput()->withErrors($this->perspective->errors);
		}

		$this->perspective->create($input);

		return Redirect::route('admin.perspective.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$perspective = $this->perspective->find($id);

        if($perspective == null || !is_numeric($id))
        {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

		return View::make('backend.perspective.edit')->with('perspective', $perspective);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!is_numeric($id))
        {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $input = Input::only('busp_name', 'busp_description');

		$this->perspective->fill($input);

		if( !$this->perspective->isValid() ) {
			return Redirect::back()->withInput()->withErrors($this->perspective->errors);
		}

		$this->perspective->update($input);

		return Redirect::route('admin.perspective.index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		if(!is_numeric($id))
        {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

		if ($this->perspective->destroy($id))
		{
			Session::flash('success', 'Business Perspective has been deleted.');
            return Redirect::route('admin.perspective.index');
        }
        else 
        {
        	Session::flash('error', 'Unable to Delete Business Perspective');
            return Redirect::route('admin.perspective.index');
        }
	}


}
