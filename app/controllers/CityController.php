<?php

class CityController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public static $rules = [
        'name' => 'required|unique:cities',
    ];

    public function index() {
        $city = City::All();
        return View::make('backend.city.index')->with('city', $city);
    }

    public function edit($id) {
        $data['city'] = City::find($id);
        $data['country'] = Country::lists('name', 'id');
        return View::make('backend.city.edit')->with('data', $data);
    }

    public function delete($id) {
        $city = City::find($id);
        
        $city->delete();
        return Redirect::to('admin/cities');
    }

    public function update() {
        $validator = Validator::make(Input::all(), self::$rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::route('edit.city', array(Input::get('id')))
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput();
        } else {
            City::where('id', '=', Input::get('id'))->update(array(
                        'name'       => Input::get('name'),
                        'country_id' => Input::get('country_id'),
                ));
            return Redirect::route('edit.city', array(Input::get('id')))->with('message','Updated!');
        }
    }

    /**
     * Ajax city
     */
    public function getCities($id) {


        $positions = DB::table('cities')->select('id', 'name')->where('city_id', '=', $id)->get();
        return Response::json($positions);
    }

    /**
     * save data to db
     */
    public function store() {

        $validator = Validator::make(Input::all(), self::$rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('admin/city/create')
                            ->withErrors($validator) // send back all errors to the login form
                            ->withInput(); // send back the input (not the password) so that we can repopulate the form
        } else {
            $city = New City;
            $city->name = Input::get('name');
            $city->country_id = Input::get('country_id');
            $city->save();
            return Redirect::route('edit.city', array($city->id));
        }
    }

}
