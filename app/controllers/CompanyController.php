<?php

class CompanyController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if ($id == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        
       
       
        $company = Company::find($id);
        $country = Country::lists('name', 'id');
        $city = City::lists('name', 'id');
        
        $usercompany = UserCompany::where('company_id',$id)->get();
       Breadcrumbs::addCrumb('Home', url('admin'));
       Breadcrumbs::addCrumb('Companies', URL::to('admin/users/'.$usercompany[0]->user_id.'/company'));
       Breadcrumbs::addCrumb('Edit Company', URL::to('admin/company/'.$id.'/edit'));
       Breadcrumbs::setDivider('»');

        return View::make('backend.usercompany.edit')
                        ->with('company', $company)
                        ->with('country', $country)
                        ->with('city', $city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }
        $company = new Company();
        $data = Input::All();
        // attempt validation
        if ($company->validate($data,true)) {
            //Session::flash('success', 'Updated');
            
            Company::where('id', '=', Input::get('id'))->update(array(
                
                'country_id'     => Input::has('country_id') ? Input::get('country_id') : "",
                'city_id'     => Input::has('city_id') ? Input::get('city_id') : "",
                'post_code'        => Input::has('post_code') ? strtoupper(Input::get('post_code')) : "",
                'po_box'     => Input::has('po_box') ? strtoupper(Input::get('po_box')) : "",
                'phone_number'     => Input::has('phone_number') ? strtoupper(Input::get('phone_number')) : "",
                'address_1'     => Input::has('address_1') ? Input::get('address_1') : "",
                'address_2'     => Input::has('address_2') ? strtoupper(Input::get('address_2')) : "",
                'website'     => Input::has('website') ? strtolower(Input::get('website')) : "",
                
            ));

            return Redirect::action('CompanyController@edit', array($id))
                            ->withInput($data)
                            ->with('message','Updated');
        } else {
            
            
            return Redirect::action('CompanyController@edit', array($id))
                            ->withInput($data)
                            ->withErrors($company->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Ajax country
     */
    public function getCities($id) {


        $positions = DB::table('cities')->select('id', 'name')->where('country_id', '=', $id)->get();
        return Response::json($positions);
    }

}
