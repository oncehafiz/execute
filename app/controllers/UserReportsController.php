<?php

class UserReportsController extends \BaseController {

    protected $report;
    protected $userreports;
    protected $company;

    public function __construct() {
        $this->report = new \Reports();
        $this->userreports = new \UserReport();
        $this->company = new \Company();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Reports', URL::to('admin/departmental/reports'));

        Breadcrumbs::setDivider('»');
        $reports = $this->userreports->all();
        return View::make('backend.userreports.index')
                        ->with('reports', $reports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Reports', URL::to('admin/departmental/reports'));
        Breadcrumbs::addCrumb('Create Report', URL::to('admin/departmental/reports/create'));
        Breadcrumbs::setDivider('»');
        $companies = $this->company->lists('name', 'id');
        return View::make('backend.userreports.create')->with('companies', $companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $data = Input::All();


        // attempt validation
        if ($this->report->validate($data)) {
            /** TODO: should be in validation */
            if (Input::hasfile('file')) {

                $file = $data['file'];
                $destinationPath = public_path() . '/backend/reports/';
                $filename = "c" . $data['company_id'] . "_" . Str::random(6) . '_' . $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
            }
            $insert_data = array('report_name' => $data['report_name'], 'report_description' => $data['report_description'], 'file' => $filename);
            $last_id = $this->report->create($insert_data);
            $report_id = $last_id->id;

            foreach (Input::get('dpt_id') as $value) {

                $user_report = array('dpt_id' => $value, 'report_id' => $report_id);
                /* $this->userreports->dpt_id = $value;
                  $this->userreports->report_id = $report_id;
                  $this->userreports->save(); */
                $this->userreports->create($user_report);
            }

            //Session::flash('success', 'Updated');

            return Redirect::action('UserReportsController@index', array())
                            ->withInput(Input::except('file'))
                            ->with('message', 'Created');
        } else {


            return Redirect::action('UserReportsController@create', array())
                            ->withInput(Input::except('file', 'company_id'))
                            ->withErrors($this->report->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if ($id == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }
        $companies = $this->company->lists('name', 'id');
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Reports', URL::to('admin/departmental/reports'));
        Breadcrumbs::addCrumb('Edit Report', URL::to('admin/departmental/reports/' . $id . '/edit'));
        Breadcrumbs::setDivider('»');

        return View::make('backend.userreports.edit')
                        ->with('report', $this->report->find($id))
                        ->with('companies', $companies);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $data = Input::All();
        
        // attempt validation
        if ($this->report->validate($data, true)) {
            //Session::flash('success', 'Updated');
            if (!empty($data['company_id'])) {
                
                if (empty($data['dpt_id'])) {
                    return Redirect::action('UserReportsController@edit', array($id))
                                    ->withInput(Input::except('file', 'company_id'))
                                    ->with('message', 'Select Department');
                }
            }
            $get_file = $this->report->find($id);
            if (Input::hasFile('file')) {
                File::delete(public_path() . '/backend/reports/' . $get_file->file);
                $file = $data['file'];
                $destinationPath = public_path() . '/backend/reports/';
                $filename = "c" . $id . "_" . Str::random(6) . '_' . str_replace(" ", $id, $file->getClientOriginalName());
                $file->move($destinationPath, $filename);
            } else {
                $fiename = $get_file->file;
            }
            $this->report->where('id', '=', Input::get('id'))->update(array(
                'report_name' => Input::has('report_name') ? Input::get('report_name') : "",
                'report_description' => Input::has('report_description') ? Input::get('report_description') : "",
                'file' => strtolower($fiename)
            ));

            if (!empty($data['company_id'])) {
                DB::table('user_reports')->where('report_id', '=', $id)->delete();
                foreach (Input::get('dpt_id') as $value) {
                    $user_report = array('dpt_id' => $value, 'report_id' => $id);
                    /* $this->userreports->dpt_id = $value;
                      $this->userreports->report_id = $report_id;
                      $this->userreports->save(); */
                    $this->userreports->create($user_report);
                }
            }

            return Redirect::action('UserReportsController@edit', array($id))
                            ->with('message', 'Updated');
        } else {


            return Redirect::action('UserReportsController@edit', array($id))
                            ->withInput(Input::except('file', 'company_id'))
                            ->withErrors($this->report->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        
        $user = $this->userreports->find($id);
        DB::table('user_reports')->where('dpt_id', '=', $user->dpt_id)->where('report_id','=',$user->report_id)->delete();
        
        
        
        return Redirect::action('UserReportsController@index', array())
                        ->with('message', 'Deleted');
    }

    /**
     * Ajax departments
     */
    public function getDepartments($id) {
        $positions = DB::table('department')->select('id', 'dpt_name')->where('company_id', '=', $id)->get();
        return Response::json($positions);
    }

}
