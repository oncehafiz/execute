<?php

use Authority\Repo\Session\SessionInterface;
use Authority\Service\Form\Login\LoginForm;

class SessionController extends BaseController {

	/**
	 * Member Vars
	 */
	protected $session;
	protected $loginForm;

	/**
	 * Constructor
	 */
	public function __construct(SessionInterface $session, LoginForm $loginForm)
	{
		$this->session = $session;
		$this->loginForm = $loginForm;
	}

	/**
	 * Show the login form
	 */
	public function create()
	{
		return View::make('backend.sessions.login');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Form Processing
        $result = $this->loginForm->save( Input::all() );

        $json = array();

        if( $result['success'] )
        {

            Event::fire('user.login', array(
                'userId' => $result['sessionData']['userId'],
                'email' => $result['sessionData']['email'],
                'username' => $result['sessionData']['username'],
                'user_type' => $result['sessionData']['user_type'],
                'dtp_id'    => $result['sessionData']['dtp_id'],
                'company_id' => $result['sessionData']['company_id'],
                'company_name'  => $result['sessionData']['company_name'],
                'depart_name'   => $result['sessionData']['depart_name'],
            ));


            // Success!
            if(Request::ajax())
            {
                $json['status'] = 'OK';
                $json['redirect_url']   = URL::to('appUser');
                return Response::json($json);
            }

            return Redirect::to('admin');

        } else {

            if(Request::ajax())
            {
                $json['status'] = 'ERROR';
                /*TODO: Ajax Error should be gone into Helper class*/
                $errors = $this->loginForm->errors();
                $html = '<div class="alert alert-danger custom-alert" style="">';
                $html .= '<ul class="list">';
                if($errors) {

                    foreach ($errors->all() as $message)
                    {
                        $html .= '<li>'.$message.'</li>';
                    }

                } else {
                    $html .= '<li>'.$result['message'].'</li>';
                }

                $html .= '</ul></div>';

                $json['message'] = $html;


                return Response::json($json);
            }

            Session::flash('error', $result['message']);
            return Redirect::to('login')
                ->withInput()
                ->withErrors( $this->loginForm->errors() );
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		$this->session->destroy();
		Event::fire('user.logout');
		return Redirect::to('/');
	}


    /* Ajax based Functions for application users */






}
