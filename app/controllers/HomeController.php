<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    protected $request;

    public function __construct(EmailRequest $request) {
        $this->request = $request;
    }
    
    public function index()
    {
        $about_us = Page::where('seo_url', '=', 'about-us')->get();

        return View::make('home')->with('about_us', $about_us[0]);
    }
	
    
    // request for emails to coach

    public function subscribeEmailRequest()
    {

        if (Request::ajax())
        {           
            $json = array();

            $input = Input::all();

            $this->request->fill($input);

            if( !$this->request->isValid() ) {
                $errors = $this->request->errors;

                $json['status'] = 'ERROR';
                $html = '<div class="alert alert-danger custom-alert" style="">';                
                $html .= '<ul class="list">';

                foreach ($errors->all() as $message)
                {
                   $html .= '<li>'. $message . '</li>';

                }
                $html .= '</ul></div>';

                $json['message'] = $html;
            } else {

                $result = $this->request->create($input);
                if($result) {

                    Event::fire('user.subscribeEmailRequest', array(
                        'email' => $input['email_address'], 
                    ));

                    // Success!
                    $json['status'] = 'OK';
                    $json['message'] = '<div class="alert alert-success">email sent</div>';

                } else {
                    // Error
                    $json['status'] = 'ERROR';
                    $json['message'] = '<div class="alert alert-success custom-alert">Error: Server is not responding.</div>';
                }
                
                $json['status'] = 'OK';
                $json['message'] = "<div class='alert alert-success custom-alert'>Success: You have subscribed our newsletter.</div>";
            }

            return Response::json( $json );
        }
    }
 
}