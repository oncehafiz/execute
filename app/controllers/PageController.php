<?php

class PageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$pages = Page::get();
		return View::make('backend.pages.index')->with('pages', $pages);
	}

    public function home()
    {
        return View::make('backend.home');
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Static Page', URL::action('PageController@create'));
        Breadcrumbs::setDivider('»');

		return View::make('backend.pages.new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
            'page_title' => 'required',
            'seo_url' => 'required',
            'content' => 'required'
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            return Redirect::action('PageController@create', array())
                ->withInput(Input::all())
                ->withErrors($validation);
        }

        $page = new Page;

        $page->title 			= Input::get('page_title');
        $page->seo_url 			= Input::get('seo_url');
        $page->meta_title 		= Input::get('meta_title');
        $page->meta_description = Input::get('meta_description');
        $page->page_category	= Input::get('page_category');
        $page->meta_keywords 	= Input::get('meta_keywords');
        $page->content 			= Input::get('content');
        $page->status 			= Input::get('status');

        if($page->save()) {
			Session::flash('success', $page->title.' has been added successfully.');
        	return Redirect::action('PageController@index', array());
        }
		
		Session::flash('error', $page->title.' not Added.');
        return Redirect::action('PageController@create', array())
                ->withInput(Input::all())
                ->withErrors($validation);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($seo_url)
	{

		Breadcrumbs::addCrumb('Home', url('/'));
        Breadcrumbs::addCrumb($seo_url, action('PageController@show', array($seo_url)));
        Breadcrumbs::setDivider('»');

		if($seo_url == 'contact') {
			return View::make('pages.contact');
		}
        

		$page = Page::where('seo_url', '=', $seo_url)->get();


		return View::make('pages.show')->with('page', $page[0]);
	
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Page::find($id);

        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Static Page', URL::action('PageController@create'));
        Breadcrumbs::addCrumb($page->title, action('PageController@edit', array($id)));
        Breadcrumbs::setDivider('»');

		return View::make('backend.pages.edit')->with('page', $page);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array(
            'page_title' => 'required',
            'seo_url' => 'required',
            'content' => 'required'
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            return Redirect::action('PageController@edit', array($id))
                ->withInput(Input::all())
                ->withErrors($validation);
        }

        $page = Page::find($id);

        $page->title 			= Input::get('page_title');
        $page->seo_url 			= Input::get('seo_url');
        $page->meta_title 		= Input::get('meta_title');
        $page->meta_description = Input::get('meta_description');
        $page->page_category	= Input::get('page_category');
        $page->meta_keywords 	= Input::get('meta_keywords');
        $page->content 			= Input::get('content');
        $page->status 			= Input::get('status');

        if($page->update()) {
        	Session::flash('success', $page->title.' has been updated successfully.');
        	return Redirect::action('PageController@index', array());
        }
		Session::flash('error', $page->title.' not updated.');
        return Redirect::action('PageController@edit', array($id))
                ->withInput(Input::all())
                ->withErrors($validation);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$page = Page::find($id);
		if($page->delete()) {
			Session::flash('success', $page->title.' has been deleted.');
			return Redirect::action('PageController@index', array());
		}		
	}


}
