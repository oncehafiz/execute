<?php

class BannersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$banners = Banners::paginate(15);
	    return View::make('backend.banners.index')
                    ->with('banners', $banners);
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('backend.banners.new');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
    public function store()
    {

        $rules = array(
            'name' => 'required',
            'description' => 'required',
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            return Redirect::action('BannersController@create', array())
                ->withInput(Input::all())
                ->withErrors($validation);
        }

        $banner = new Banners;

        $banner->name 			= Input::get('name');
        $banner->description 	= Input::get('description');
        $banner->status 		= Input::get('status');

        if($banner->save()) {

            if(Input::has('banner')) {
                foreach(Input::get('banner') as $key => $image) {

                    $banner_image = new Images;

                    $banner_image->banner_id 	= $banner->id;
                    $banner_image->title 	    = $image['name'];
                    $banner_image->link 		= $image['link'];
                    $banner_image->image 		= $image['image'];
                    $banner_image->sort_order 	= $image['sort_order'];
                    $banner_image->save();
                }
            }
            Session::flash('success', $banner->name.' has been added successfully.');
            return Redirect::action('BannersController@index', array());
        }

        Session::flash('error', $banner->name.' not Added.');
        return Redirect::action('BannersController@create', array())
            ->withInput(Input::all())
            ->withErrors($validation);
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function edit($id)
    {
        $banner = Banners::find($id);

        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Banners', URL::action('BannersController@create'));
        Breadcrumbs::addCrumb($banner->name, action('BannersController@edit', array($id)));
        Breadcrumbs::setDivider('»');

        return View::make('backend.banners.edit')->with('banner', $banner);
    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = array(
            'name' => 'required',
            'description' => 'required',
        );

        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            return Redirect::action('BannersController@create', array())
                ->withInput(Input::all())
                ->withErrors($validation);
        }

        $banner = Banners::find($id);

        $banner->name 			= Input::get('name');
        $banner->description 	= Input::get('description');
        $banner->status 		= Input::get('status');

        if($banner->update()) {
            Images::where('banner_id', '=',$id)->delete();
            if(Input::has('banner')) {
                foreach(Input::get('banner') as $image) {

                    $banner_image = new Images;

                    $banner_image->banner_id 	= $banner->id;
                    $banner_image->title 	    = $image['name'];
                    $banner_image->link 		= $image['link'];
                    $banner_image->image 		= $image['image'];
                    $banner_image->sort_order 	= $image['sort_order'];
                    $banner_image->save();
                }
            }
            Session::flash('success', $banner->name.' has been Updated successfully.');
            return Redirect::action('BannersController@index', array());
        }

        Session::flash('error', $banner->name.' not Added.');
        return Redirect::action('BannersController@edit', array($id))
            ->withInput(Input::all())
            ->withErrors($validation);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {

        $banner = Banners::find($id);
        if($banner->delete()) {
            Images::where('banner_id', '=',$id)->delete();
            Session::flash('success', $banner->name.' has been deleted.');
            return Redirect::action('BannersController@index', array());
        }
    }


    public function upload() {

            $json = array();

            if(!is_array($_FILES["image"]['name'])) //single file
            {
                $file = Input::file('image');
                $destinationPath = 'banners/'.str_random(8);
                $filename = $file->getClientOriginalName();
                $uploadSuccess = $file->move($destinationPath, $filename);

                if( $uploadSuccess ) {
                    $json['images'][] = array(
                        'img'   => $destinationPath.'/'.$filename,
                        'src'   => $destinationPath.'/'.$filename,
                    );
                }

            }
            else {
                $fileCount = count($_FILES["image"]['name']);
                for ($i = 0; $i < $fileCount; $i++) {

                    $file = Input::file('image['.$i.']');
                    $destinationPath = 'banners/'.str_random(8);
                    $filename = $file->getClientOriginalName();
                    $uploadSuccess = $file->move($destinationPath, $filename);

                    if( $uploadSuccess ) {
                        $json['images'][] = array(
                            'img'   => $destinationPath.'/'.$filename,
                            'src'   => $destinationPath.'/'.$filename,
                        );
                    }

                }
            }

            echo json_encode($json);
    }

    public function delete() {

        $filename = public_path().'/'.Input::get('file');

        $json = array();
        $json['status'] = 'ERROR';
        if (File::exists($filename)) {
            File::delete($filename);
            $json['status'] = 'OK';
        }

        echo json_encode($json);
    }


}
