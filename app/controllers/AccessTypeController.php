<?php

class AccessTypeController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $colorcode = UserType::all();
	    return View::make('backend.usertype.index')
                    ->with('types', $colorcode);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('backend.usertype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $colorcode = new UserType();
        $data = Input::All();

        // attempt validation
        if ($colorcode->validate($data)) {

            $last_id = UserType::create($data);
            //Session::flash('success', 'Updated');

            return Redirect::action('AccessTypeController@index', array())
                            ->withInput($data)
                            ->with('message', 'Created');
        } else {


            return Redirect::action('AccessTypeController@create', array())
                            ->withInput($data)
                            ->withErrors($colorcode->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if ($id == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        
       
       
        $code = UserType::find($id);
        
       
       Breadcrumbs::addCrumb('Home', url('admin'));
       Breadcrumbs::addCrumb('User Type', URL::to('admin/usertypes'));
       Breadcrumbs::addCrumb('Edit Color Code', URL::to('admin/usertypes/'.$id.'/edit'));
       Breadcrumbs::setDivider('»');

        return View::make('backend.usertype.edit')
                       
                        ->with('color', $code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }
        $code = new UserType();
        $data = Input::All();
        // attempt validation
        if ($code->validate($data,true)) {
            //Session::flash('success', 'Updated');
            
            UserType::where('id', '=', Input::get('id'))->update(array(
                
                'type'     => Input::has('type') ? Input::get('type') : "Normal User",
                
            ));

            return Redirect::action('AccessTypeController@edit', array($id))
                            ->withInput($data)
                            ->with('message','Updated');
        } else {
            
            
            return Redirect::action('AccessTypeController@edit', array($id))
                            ->withInput($data)
                            ->withErrors($code->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        DB::table('user_types')->where('id', '=', $id)->delete();
        return Redirect::action('AccessTypeController@index', array())
                            
                            ->with('message', 'Deleted');
    }

    

}
