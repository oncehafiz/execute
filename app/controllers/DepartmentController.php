<?php
use Authority\Repo\User\UserInterface;
class DepartmentController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    protected $user;


    public function __construct(UserInterface $user) {
        $this->user = $user; 
    }
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        return View::make('backend.companydpt.create');
    }

    public function newone($id) {
        $user_company = Company::where('id', $id)->lists('name', 'id');

        return View::make('backend.companydpt.create')
                        ->with('id', $id)
                        ->with('company', $user_company);
    }

    /**
     * Departmental User
     */
    public function userdepartment($id) {
        $user_type = UserType::lists('type', 'id');
        
        return View::make('backend.dptuser.create')->with('type', $user_type)->with('id', $id);
    }

    /**
     * Store Departmental User
     */
    public function storeuserdepartment() {
        /** TODO: should be separate from controllers */
        $rules = array('email' => 'required|unique:users|email', 'first_name' => 'required', 'password' => 'required|Confirmed', 'password_confirmation' => 'required');
        $data = Input::all();
       
        $v = Validator::make($data, $rules);
        if ($v->fails()) {

            // set errors and return false
            return Redirect::action('DepartmentController@userdepartment', array(Input::get('redirect')))
                            ->withInput($data)
                            ->withErrors($v);
        }
        else
        {
            //$user = New User;
            /*$user->first_name = Input::has('first_name') ? Input::get('first_name') : "";
            $user->email = Input::has('email') ? Input::get('email') : "";
            $user->last_name = Input::has('last_name') ? Input::get('last_name') : "";
            $user->user_type = Input::has('user_type') ? Input::get('user_type') : "";
            $user->dpt_id = Input::has('dpt_id') ? Input::get('dpt_id') : "";
            $user->password = Input::has('password') ? Hash::make(Input::get('password')) : "";*/
            if(Input::get('is_admin')==1){
                
                $data['user_type'] = 3;
            }
            $last_id = $this->user->store($data);
            
            return Redirect::action('UserController@departments', array(Input::get('redirect')))
                            ->withInput($data)
                            ->with('message',$last_id['message']);
        }
    }
    
    /**
     * Show Departmental User
     */
    public function showuserdepartment($dpt_id,$user_id){
        $user = $this->user->byId($user_id);

        if ($user == null || !is_numeric($user_id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }
        
        $getDepart = Department::find($dpt_id);
        $getCompany = UserCompany::where('company_id','=', $getDepart->company_id)->get();
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Companies', URL::to('admin/users/' . $getCompany[0]->user_id . '/company'));
        Breadcrumbs::addCrumb('Departments', URL::to('admin/company/' . $getDepart->company_id . '/department/' . $getCompany[0]->user_id));
        Breadcrumbs::addCrumb('Departmental User', URL::to('admin/users/' . $dpt_id . '/departments'));
        Breadcrumbs::addCrumb('Profile User', URL::action('UserController@departments', array($dpt_id)));
        Breadcrumbs::setDivider('»');


        return View::make('backend.dptuser.show')->with('user', $user);
    }
    /**
     * Delete Departmental Users
     */
    public function deleteuserdepartment($dpt_id,$user_id){
        
        $this->user->destroy($user_id);
        return Redirect::action('UserController@departments', array($dpt_id))
                            
                            ->with('message','Deleted');
        
    }
    
    /**
     * Ban a Departmental user
     * @param  int $id 
     * @return Redirect     
     */
    public function ban($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->ban($id);
        $user = $this->user->byId($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);
            return Redirect::to('admin/users/'.$user->dpt_id.'/departments');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::to('admin/users/'.$user->dpt_id.'/departments');
        }
    }

    public function unban($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->unBan($id);
        $user = $this->user->byId($id);
        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);
            return Redirect::to('admin/users/'.$user->dpt_id.'/departments');
        } else {
            Session::flash('error', $result['message']);
            return Redirect::to('admin/users/'.$user->dpt_id.'/departments');
        }
    }

    /**
     * Edit Departmental User
     */
    public function edituserdepartment($dpt_id,$user_id) {
        if ($user_id == null || !is_numeric($dpt_id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $users = $this->user->byId($user_id);
        $country = Country::lists('name', 'id');
        $city = City::lists('name', 'id');
        $getDepart = Department::find($dpt_id);
        $getCompany = UserCompany::where('company_id','=', $getDepart->company_id)->get();
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Companies', URL::to('admin/users/' . $getCompany[0]->user_id . '/company'));
        Breadcrumbs::addCrumb('Departments', URL::to('admin/company/' . $getDepart->company_id . '/department/' . $getCompany[0]->user_id));
        Breadcrumbs::addCrumb('Departmental User', URL::to('admin/users/' . $dpt_id . '/departments'));
        Breadcrumbs::addCrumb('Edit Departmental User', URL::action('UserController@departments', array($dpt_id)));
        Breadcrumbs::setDivider('»');


        return View::make('backend.dptuser.edit')
                        ->with('user', $users)
                        ->with('country', $country)
                        ->with('city', $city)
                        ->with('department_id', $dpt_id)
                        ->with('user_id',$user_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $department = new Department();
        $data = Input::All();

        // attempt validation
        if ($department->validate($data)) {

            $last_id = Department::create($data);
            //Session::flash('success', 'Updated');

            return Redirect::action('DepartmentController@edit', array($last_id->id))
                            ->withInput($data)
                            ->with('message', 'Updated');
        } else {


            return Redirect::action('DepartmentController@newone', array(Input::get('company_id')))
                            ->withInput($data)
                            ->withErrors($department->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        if ($id == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $company = Department::find($id);
        $country = Country::lists('name', 'id');
        $city = City::lists('name', 'id');
        //$getcompany = $company->company()->get();
        $usercompany = UserCompany::where('company_id',$company->company_id)->get();
        
        Breadcrumbs::addCrumb('Home', url('admin'));
        Breadcrumbs::addCrumb('Companies', URL::to('admin/users/'.$usercompany[0]->user_id.'/company'));
        Breadcrumbs::addCrumb('Departments', URL::to('admin/company/'.$company->company_id.'/department/'.$usercompany[0]->user_id));
        Breadcrumbs::addCrumb('Edit Department', URL::to('admin/company/'.$id.'/edit'));
        Breadcrumbs::setDivider('»');
        return View::make('backend.companydpt.edit')
                        ->with('department', $company)
                        ->with('country', $country)
                        ->with('city', $city);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }
        $company = new Department();
        $data = Input::All();
        // attempt validation
        if ($company->validate($data, true)) {
            //Session::flash('success', 'Updated');

            Department::where('id', '=', Input::get('id'))->update(array(
                //'dpt_name' => Input::has('dpt_name') ? Input::get('dpt_name') : "",
                // 'company_id'     => Input::has('company_id') ? Input::get('company_id') : "",
                'dpt_description' => Input::has('dpt_description') ? Input::get('dpt_description') : ""
            ));

            return Redirect::action('DepartmentController@edit', array($id))
                            ->withInput($data)
                            ->with('message', 'Updated');
        } else {


            return Redirect::action('DepartmentController@edit', array($id))
                            ->withInput($data)
                            ->withErrors($company->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $user = Department::find($id);
        DB::table('users')->where('dpt_id', '=', $id)->delete();
        $redirect = $user->company_id;
        $user_id = UserCompany::where('company_id',$redirect)->get();
        $user->delete();
        return Redirect::route('companydepartmentForm', array($redirect,$user_id[0]->user_id));
    }

}
