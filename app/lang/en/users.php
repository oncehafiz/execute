<?php

return array(
    /*
      |--------------------------------------------------------------------------
      | User Repositiory Messages
      |--------------------------------------------------------------------------
     */

    'created' => "Your account has been created. Check your email for the confirmation link.",
    'loginreq' => "Login field required.",
    'exists' => "User already exists.",
    'notfound' => "User not found",
    'noaccess' => "You are not allowed to do that.",
    'updated' => "Profile updated",
    'notupdated' => "Unable to update profile",
    'activated' => "Activation complete. <a href=':url' class='alert-link'>You may now login</a>",
    'notactivated' => "Activation could not be completed.",
    'alreadyactive' => "That account has already been activated.",
    'emailconfirm' => "Check your email for the confirmation link.",
    'emailinfo' => "Check your email for instructions.",
    'emailpassword' => "Your password has been changed. Check your email for the new password.",
    'problem' => "There was a problem. Please contact the system administrator.",
    'passwordchg' => "Your password has been changed.",
    'passwordprob' => "Your password could not be changed.",
    'oldpassword' => "You did not provide the correct original password.",
    'suspended' => "User has been suspended for :minutes minutes.",
    'unsuspended' => "Suspension removed.",
    'banned' => "User has been banned.",
    'unbanned' => "User has been unbanned.",
    'forgot_instruction' => "Enter your email address to reset your password. You may need to check your spam folder or unblock no-reply@coaches-network.com.",
    'forgotupword' => "Forgot your password?",
    'forgot' => "Forgot Password",
    'email' => "User Email",
    'resendpword' => "Resend Password",
    'pword' => "Password",
    'coach_dir' => "Coaches Directory",
    'search_coach_dir' => "Search Directory",
    'resend' => "Resend",
    'lname' => 'Last Name',
    'newpassword_lbl' => 'New Password',
    'profile' => 'Users List',
    'fname' => 'First Name',
    'group_membership' => "User Groups",
    'change_passwort' => "Change Password",
    'oldpassword_lbl' => "Old Password",
    "newcompassword_lbl" => "New Repeat Password",
    "type" => "User Type",
    "company_name_signup" => "Company Name",
    "fname_empty" => "John",
    "lname_empty" => "Smith",
    "email_empty" => "info@domain.com",
    "passwords" => "Password",
    "cpasswords" => "Confirm Password",
    "CandD" => "Company & Department",
    "passwords_empty" => "Don't use your credit card etc"
);
