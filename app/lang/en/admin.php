<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | User Repositiory Messages
    |--------------------------------------------------------------------------
    */

    'site_name'     => "exec<span>you</span>tion",
    'created' 		=>  "Your account has been created. Check your email for the confirmation link.",

    'email'         => "Email",

    'admin_email'   => "nadeemakhtar.se@gmail.com",

    'admin_name'    => "SmartSheet",

    'pword'         => "User Password",

    'remember'      => "Remember me",

    'dashboard'     => 'Admin Dashboard',

    'loginreq' 		=>	"Login field required.",

    'exists'		=>	"User already exists.",

    'notfound'		=>	"User not found",

    'noaccess'		=>	"You are not allowed to do that.",

    'updated'		=>	"Profile updated",

    'notupdated'	=>	"Unable to update profile",

    'activated'		=>	"Activation complete. <a href=':url' class='alert-link'>You may now login</a>",

    'notactivated'	=>	"Activation could not be completed.",

    'alreadyactive'	=>	"That account has already been activated.",

    'emailconfirm'	=>	"Check your email for the confirmation link.",

    'emailinfo'		=>	"Check your email for instructions.",

    'emailpassword'	=>	"Your password has been changed. Check your email for the new password.",

    'problem'		=>	"There was a problem. Please contact the system administrator.",

    'passwordchg'	=> 	"Your password has been changed.",

    'passwordprob'	=>	"Your password could not be changed.",

    'oldpassword'	=>	"You did not provide the correct original password.",

    'suspended'		=>	"User has been suspended for :minutes minutes.",

    'unsuspended'	=>	"Suspension removed.",

    'banned'		=>	"User has been banned.",

    'unbanned'		=>	"User has been unbanned.",

    'coaches'       => "Coaches types",

    'coach_new'     => "New Coach Type",

    'type_edit'     => "Edit Type Coach type",

    'packages'      => "Packages",

    'package_new'   => "New Package",

    'currencies'    => array('US Dollars','Canadian Dollars','British Pounds','Euros','Australian Dollars','New Zealand Dollars'),

);