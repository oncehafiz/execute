<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Group Repositiory Messages
	|--------------------------------------------------------------------------
	*/

	'created' 		=> "Group Created.",	

	'loginreq' 		=> "Login field required.",

	'userexists' 	=> "User already exists.",

	'updated' 		=> "Group has been updated.",

	'updateproblem' => "There was a problem updating the group.",

	'namereq' 		=> "You must provide a group name.",

	'groupexists' 	=> "That group already exists.",

	'notfound' 		=> "Group not found.",
    
    'groups'        =>  "User Groups List",
    
    'create'        =>  "Create Groups",
    
    'name'          =>  "Group Name",
    
    'permissions'   =>  "Group Permissions",
    
    'group'         =>  "Group",

);