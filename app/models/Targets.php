<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class Targets extends Eloquent {

    protected $table = 'targets';

    public function goal()
    {
        return $this->belongsTo('Goals', 'busp_id');
    }

    public function targetsHistory()
    {
        return $this->hasMany('TargetsHistory', 'target_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

}