<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class MileStonesHistory extends Eloquent {

    protected $table = 'project_milestone_history';

    public function mileStone()
    {
        return $this->belongsTo('MileStones', 'project_id');
    }
}