<?php



class UserCompany extends Eloquent{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_company';
    public $timestamps = false;
    
    public function user() {
        return $this->belongsTo('User', 'user_id');
    }
    public function company() {
        return $this->belongsTo('Company', 'company_id');
    }
    
  

}
