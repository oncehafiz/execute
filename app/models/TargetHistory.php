<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class TargetsHistory extends Eloquent {

    protected $table = 'targets_history';

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function target()
    {
        return $this->belongsTo('Targets', 'target_id');
    }

}