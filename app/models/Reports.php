<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class Reports extends Eloquent {

    protected $table = 'reports';
    protected $guarded = array('_token');
    protected $fillable = ['report_name', 'report_description','file'];
    public function userHasReports()
    {
        return $this->hasMany('UserReport','report_id');
    }
   
    public $timestamps = false;
    protected $errors;
    private $rules = array(
        'report_name' => 'required',
        'report_description' => 'required',
        'file' => 'required|mimes:txt,pdf,doc',
        'dpt_id' => 'required',
        'company_id' => 'required'
    );
    private $update_rule = array(
        'report_name' => 'required',
        'report_description' => 'required',
        'file' => 'mimes:pdf,doc',
        
        
        
    );

    public function errors() {
        return $this->errors;
    }

    public function validate($data, $update = false) {
        if ($update) {
            $v = Validator::make($data, $this->update_rule);
            if ($v->fails()) {

                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        } else {
            // make a new validator object
            $v = Validator::make($data, $this->rules);
            if ($v->fails()) {
                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        }
    }
}