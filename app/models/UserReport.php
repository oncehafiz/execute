<?php

/**
 * Created by PhpStorm.
 * User: ching
 * Date: 
 * Time: 
 */
class UserReport extends Eloquent {

    protected $guarded = array('_token');
    public $timestamps = false;
    protected $table = 'user_reports';
     protected $fillable = ['report_id', 'dpt_id'];
    

    public function department() {
        return $this->belongsTo('Department', 'dpt_id');
    }

    public function reports() {
        return $this->belongsTo('Reports', 'report_id');
    }

}
