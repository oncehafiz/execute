<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class Attachments extends Eloquent {

    protected $table = 'attachement';

    public function project()
    {
        return $this->belongsTo('Projects', 'object_id');
    }

    public function target()
    {
        return $this->belongsTo('Targets', 'object_id');
    }
}