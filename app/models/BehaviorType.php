
<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class BehaviorType extends Eloquent {

    protected $table = 'behavior_types';

    public static $rules = [
                'name' => 'Required',
                'description' => 'Required'
       		];

	protected $fillable = ['name', 'description'];

	public $error;


    public function isValid() {        

        $validation = Validator::make($this->attributes, static::$rules);

        if(!$validation->fails()) return true;

        $this->errors = $validation->messages();

        return false;
	}

}