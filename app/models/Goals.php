<?php
/**
 * Created by PhpStorm.
 * User: nadee_000
 * Date: 11/1/2014
 * Time: 9:26 AM
 */
class Goals extends Eloquent {

    protected $table = 'goals';


    public function targets()
    {
        return $this->hasMany('Targets', 'goal_id');
    }

    public function businessPerspectives()
    {
        return $this->belongsTo('BusinessPerspectives', 'persp_id');
    }

}