<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class Projects extends Eloquent {

    protected $table = 'projects';

    public function milestones()
    {
        return $this->hasMany('MileStones', 'project_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}