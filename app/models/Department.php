<?php

/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */
class Department extends Eloquent {

    protected $guarded = array('_token');
    protected $table = 'department';
    public $timestamps = false;
    protected $errors;
    private $rules = array(
        'dpt_name' => 'required|unique:department',
    );
    private $update_rule = array(
        'dpt_name' => 'min:2',
    );

    public function errors() {
        return $this->errors;
    }

    public function validate($data, $update = false) {
        if ($update) {
            $v = Validator::make($data, $this->update_rule);
            if ($v->fails()) {

                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        } else {
            // make a new validator object
            $v = Validator::make($data, $this->rules);
            if ($v->fails()) {
                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        }
    }

    public function company() {
        return $this->belongsTo('Company', 'company_id');
    }

    public function user() {
        return $this->belongsTo('User', 'user_id');
    }

    public function reports() {
        return $this->hasMany('Reports', 'dpt_id');
    }

}
