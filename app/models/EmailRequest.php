<?php

class EmailRequest extends Eloquent {

    protected $table = 'newsletter';

    /**
	 * Returns a formatted post content entry,
	 * this ensures that line breaks are returned.
	 *
	 * @return string
	 */

	public static $rules = [
                'email_address' => 'required|email|unique:newsletter',
                'subscription_list' => 'required',
       		];

	protected $fillable = ['email_address', 'subscription_list'];

	public $error;

	public function isValid() {        

        $validation = Validator::make($this->attributes, static::$rules);

        if(!$validation->fails()) return true;

        $this->errors = $validation->messages();

        return false;
	}

}