<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class MileStones extends Eloquent {

    protected $table = 'project_milestones';

    public function project()
    {
        return $this->belongsTo('Projects', 'project_id');
    }   

    public function MileStonesHistory()
    {
        return $this->hasMany('MileStonesHistory', 'milestone_id');
    }
}