<?php
/**
 * Created by PhpStorm.
 * User: nadee_000
 * Date: 11/1/2014
 * Time: 9:26 AM
 */

class Reviews extends Eloquent {

    protected $table = 'coach_reviews';

    public function reviewCoach()
    {
        return $this->belongsTo('Coach', 'coach_id');
    }


}