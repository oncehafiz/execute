<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Country extends Eloquent implements UserInterface, RemindableInterface{

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'country';
    protected $guarded = array('id', 'name');
    public $timestamps = false;
    
    /**
     * Relationship
     * @return type
     */

    public function company() {
        return $this->hasMany('Company');
    }
    public function city() {
        return $this->hasMany('City');
    }
    
    
    

}
