
<?php

/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */
class UserType extends Eloquent {

    protected $table = 'user_types';
    protected $guarded = array('_token');
    public $timestamps = false;
    protected $errors;
    private $rules = array(
        'type' => 'required|unique:user_types',
    );
    private $update_rule = array(
        'type' => 'min:3',
    );

    public function errors() {
        return $this->errors;
    }

    public function validate($data, $update = false) {
        if ($update) {
            $v = Validator::make($data, $this->update_rule);
            if ($v->fails()) {

                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        } else {
            // make a new validator object
            $v = Validator::make($data, $this->rules);
            if ($v->fails()) {
                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        }
    }

}
