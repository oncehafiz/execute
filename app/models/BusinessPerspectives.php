<?php
/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */

class BusinessPerspectives extends Eloquent {

    protected $table = 'business_perspectives';

    public static $rules = [
                'busp_name' => 'Required',
                'busp_description' => 'Required'
       		];

	protected $fillable = ['busp_name', 'busp_description'];

	public $error;


    public function goals()
    {
        return $this->hasMany('Goals', 'persp_id');
    }


    public function isValid() {        

        $validation = Validator::make($this->attributes, static::$rules);

        if(!$validation->fails()) return true;

        $this->errors = $validation->messages();

        return false;
	}

}