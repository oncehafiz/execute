<?php
/**
 * Created by PhpStorm.
 * User: nadee_000
 * Date: 12/27/2014
 * Time: 9:31 PM
 */

class Images extends Eloquent {

    protected $table = 'images';

    /*One flate belong to one user */

    public function banner()
    {
        return $this->belongsTo('Banners', 'banner_id');
    }

}