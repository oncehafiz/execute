<?php

/**
 * Created by PhpStorm.
 * User: nadee_000
 * Date: 11/1/2014
 * Time: 9:26 AM
 */
class Page extends Eloquent {

    protected $table = 'pages';

    /**
	 * Returns a formatted post content entry,
	 * this ensures that line breaks are returned.
	 *
	 * @return string
	 */
	public function content()
	{
		return nl2br($this->content);
	}

}