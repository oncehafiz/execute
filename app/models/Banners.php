<?php
/**
 * Created by PhpStorm.
 * User: nadee_000
 * Date: 11/1/2014
 * Time: 9:26 AM
 */
class Banners extends Eloquent {

    protected $table = 'banners';

    /*One flate belong to one user */

    public function images()
    {
        return $this->hasMany('images', 'banner_id');
    }

}