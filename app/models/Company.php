<?php

/**
 * Created by PhpStorm.
 * User: hunny
 * Date: 1/23/15
 * Time: 9:05 AM
 */
class Company extends Eloquent {

    protected $table = 'company';
    public $errors;
    public $timestamps = false;
    private $rules = array(
        'name' => 'required|company:unique',
    );
    private $update_rule = array(
        'post_code' => 'integer',
    );

    public function errors() {
        return $this->errors;
    }

    public function validate($data, $update = false) {
        if ($update) {
            $v = Validator::make($data, $this->update_rule);
            if ($v->fails()) {
               
                // set errors and return false
                $this->errors = $v;
                return false;
            }
            return true;
        }
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v;
            return false;
        }
        return true;
    }

    /* public function user() {
      return $this->belongsTo('User', 'user_id');
      } */

    public function country() {
        return $this->belongsTo('Country', 'country_id');
    }

    public function city() {
        return $this->belongsTo('City', 'city_id');
    }

    public function departments() {
        return $this->hasMany('Department');
    }

    public function getOrSetCompany($data) {
       
        if (isset($data['department_id'])) {
            $dpt_id = $data['department_id'];
        } else if (Session::has('dtp_id')) {
            $dpt_id = Session::get('dtp_id');
        } else if (isset($data['company_name'])) {
            $this->name = $data['company_name'];
            if ($this->save()) {
                $dpt_obj = new Department();

                $dpt_obj->dpt_name = 'Casual';
                $dpt_obj->dpt_description = 'Casual';
                $dpt_obj->company_id = $this->id;

                $dpt_obj->save();

                $dpt_id = $dpt_obj->id;
                $return_data['company_id'] = $this->id;
            }
        }

        $return_data['dpt_id'] = $dpt_id;
        return $return_data;
    }

}
