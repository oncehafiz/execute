<?php
/**
 * Created by PhpStorm.
 * User: nadee_000
 * Date: 11/1/2014
 * Time: 9:26 AM
 */
class Members extends Eloquent {

    protected $table = 'users';

    /*User has many flates*/

    public function flats()
    {
        return $this->hasMany('Flats');
    }

    public function targets()
    {
        return $this->hasMany('Targets', 'user_id');
    }
}