<?php

// User Login event
Event::listen('user.login', function($userId, $email, $username, $user_type, $dtp_id, $company_id, $company_name, $depart_name)
{
    Session::put('userId', $userId);
    Session::put('email', $email);
    Session::put('username', $username);
    Session::put('dtp_id', $dtp_id);
    Session::put('user_type', $user_type);
    Session::put('company_id', $company_id);
    Session::put('company_name', $company_name);
    Session::put('depart_name', $depart_name);
});

// User logout event
Event::listen('user.logout', function()
{
	Session::flush();
});

// Subscribe to User Mailer events
Event::subscribe('Authority\Mailers\UserMailer');