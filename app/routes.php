<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


// Session Routes
Route::get('login',  array('as' => 'login', 'uses' => 'SessionController@create'));
Route::get('logout', array('as' => 'logout', 'uses' => 'SessionController@destroy'));



Route::resource('sessions', 'SessionController', array('only' => array('create', 'store', 'destroy')));

// User Routes
Route::get('users/{id}/activate/{code}', 'UserController@activate')->where('id', '[0-9]+');
Route::get('resend', array('as' => 'resendActivationForm', function()
{
	return View::make('backend.users.resend');
}));

Route::get('register',  array('as' => 'register', 'uses' => 'UserController@registerForm'));
Route::post('register',  array('as' => 'register', 'uses' => 'UserController@register'));

Route::post('resend', 'UserController@resend');
Route::get('forgot', array('as' => 'forgotPasswordForm', function()
{
	return View::make('backend.users.forgot');
}));

Route::post('forgot', 'UserController@forgot');
Route::get('users/{id}/reset/{code}', 'UserController@reset')->where('id', '[0-9]+');

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));

//email subscriber
Route::post('subscribeEmailRequest', array('as' => 'user/email_request', 'uses' => 'HomeController@subscribeEmailRequest'));

//page show

 Route::get('page/{seo_url}', array('as' => 'page', 'uses' => 'PageController@show'));

/*Frontend Application routes*/

Route::group(array('before' => 'frontAdminAuth'), function()
{


    //perspective
    Route::resource('appUser', 'FrontAdminController');
    Route::get('{username}/smartsheet', array('as' => 'smartsheet', function($username)
    {
        $perspective = BusinessPerspectives::All();
        
        //$company_goals = Goals::where('company_id','=',Session::get('company_id'))->where('persp_id','=',$p->id)->get();
        return View::make('frontend.user.smartsheet')->with('perspective',$perspective);
    }));
    
    //Goals
    Route::resource('company_goals', 'GoalController');
    Route::post('company_goals/new', array('uses' => 'GoalController@store'));

});



/*Application Admin Routes */

Route::group(array('before' => 'adminAuth'), function()
{
    // CMS pages
    Route::get('admin', array('as' => 'admin', 'uses' => 'PageController@home'));

    Route::resource('admin/pages', 'PageController');

    //Setting History
    Route::resource('admin/setting', 'SettingController');

    Route::post('admin/upload', array('as' => 'upload', 'uses' => 'BannersController@upload'));

    Route::post('admin/file/delete', array('as' => 'file/delete', 'uses' => 'BannersController@delete'));

    //Banners
    Route::resource('admin/banners', 'BannersController');
    
    //Color Code
    Route::resource('admin/colorcode', 'ColorController');
    
    //User Type Access
    Route::resource('admin/usertypes', 'AccessTypeController');

    //perspective
    Route::resource('admin/perspective', 'PerspectiveController');

    // behavior type
    Route::resource('admin/behaviorType', 'BehaviorsTypeController');
    
    // User Company
    Route::get('admin/users/{user_id}/company', array('as' => 'usercompanyForm', function($user_id)
    {
        $user_company = UserCompany::where('user_id','=',$user_id)->get();
        return View::make('backend.usercompany.index')->with('user_id', $user_id)->with('company',$user_company);
    }));
    
    Route::resource('admin/companies', 'CompanyController');
    
    //Company Department
    Route::get('admin/company/{user_id}/department/{dpt_id}', array('as' => 'companydepartmentForm', function($user_id,$dpt_id)
    {
       Breadcrumbs::addCrumb('Home', url('admin'));
       Breadcrumbs::addCrumb('Companies', URL::to('admin/users/'.$dpt_id.'/company'));
       Breadcrumbs::addCrumb('Departments', URL::to('admin/company/'.$user_id.'/department'));
       Breadcrumbs::setDivider('»');
        $user_company = Department::where('company_id','=',$user_id)->get();
        return View::make('backend.companydpt.index')->with('redirect',$dpt_id)->with('user_id', $user_id)->with('department',$user_company);
    }));
    Route::get('admin/department/{id}/create', 'DepartmentController@newone');
    Route::resource('admin/departments', 'DepartmentController');
    
    //Department User
    Route::get('admin/users/{id}/departments', array('as' => 'departmentusersForm','uses' => 'UserController@departments'));
    Route::get('admin/department/{user_id}/users/create', array('uses' => 'DepartmentController@userdepartment'));
    Route::get('admin/department/{dpt_id}/users/{user_id}/edit', array('uses' => 'DepartmentController@edituserdepartment'));
    Route::get('admin/department/{dpt_id}/users/{user_id}/show/user', array('uses' => 'DepartmentController@showuserdepartment'));
    Route::post('admin/department/{dpt_id}/users/{user_id}/delete/user', array('uses' => 'DepartmentController@deleteuserdepartment'));
    Route::post('admin/department/user/store', array('uses' => 'DepartmentController@storeuserdepartment'));
    Route::get('admin/department/{id}/user/ban', 'DepartmentController@ban')->where('id', '[0-9]+');
    Route::get('admin/department/{id}/user/unban', 'DepartmentController@unban')->where('id', '[0-9]+');
    
    //Admin Operation

    Route::post('admin/users/{id}/change', 'UserController@change');
    Route::get('admin/users/{id}/suspend', array('as' => 'suspendUserForm', function($id)
    {
        return View::make('backend.users.suspend')->with('id', $id);
    }));

     Route::get('getCities/{id}', array('uses' => 'CompanyController@getCities'), function($id) {
        
    })->where('id', '[0-9]+');
    Route::get('getDepartments/{id}', array('uses' => 'UserReportsController@getDepartments'), function($id) {
        
    })->where('id', '[0-9]+');
    
    Route::post('admin/users/{id}/suspend', 'UserController@suspend')->where('id', '[0-9]+');
    Route::get('admin/users/{id}/unsuspend', 'UserController@unsuspend')->where('id', '[0-9]+');
    Route::get('admin/users/{id}/ban', 'UserController@ban')->where('id', '[0-9]+');
    Route::get('admin/users/{id}/unban', 'UserController@unban')->where('id', '[0-9]+');
    Route::resource('admin/users', 'UserController');

// Group Routes
    Route::resource('admin/groups', 'GroupController');

     /**
     * Country Route
     */
    Route::get('admin/countries', array('as' => 'countries', 'uses' => 'CountryController@index'));
    Route::post('country/store', array('uses' => 'CountryController@store'));
    Route::post('country/update', array('uses' => 'CountryController@update'));
    Route::get('admin/country/edit/{id}', array('as' => 'edit.country', 'uses' => 'CountryController@edit'), function($id) {
        
    })->where('id', '[0-9]+');
    Route::post('admin/country/destroy/{id}', array('as' => 'country/delete', 'uses' => 'CountryController@destroy'), function($id) {
        
    })->where('id', '[0-9]+');
    Route::get('admin/country/create', function() {
        return View::make('backend.country.create');
    });
    /**
     * City Route
     */
    Route::get('admin/cities', array('as' => 'cities', 'uses' => 'CityController@index'));
    Route::post('city/store', array('uses' => 'CityController@store'));
    Route::post('city/update', array('uses' => 'CityController@update'));
    Route::get('admin/city/edit/{id}', array('as' => 'edit.city', 'uses' => 'CityController@edit'), function($id) {
        
    })->where('id', '[0-9]+');
    Route::post('admin/city/delete/{id}', array('as' => 'city/delete', 'uses' => 'CityController@delete'), function($id) {
        
    })->where('id', '[0-9]+');
    Route::get('admin/city/create', function() {

        return View::make('backend.city.create')->with('country', Country::lists('name', 'id'));
    });
    
    /**
     * Departmental User Report
     */
    Route::resource('admin/departmental/reports', 'UserReportsController');
    
});


// App::missing(function($exception)
// {
//     App::abort(404, 'Page not found');
//     //return Response::view('errors.missing', array(), 404);
// });





