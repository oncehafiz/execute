

ALTER TABLE `reports`
  DROP `user_id`;

--
-- Table structure for table `user_reports`
--

CREATE TABLE IF NOT EXISTS `user_reports` (
`id` int(11) NOT NULL,
  `report_id` int(10) unsigned NOT NULL,
  `dpt_id` int(10) unsigned NOT NULL COMMENT 'Department ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_ar` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;