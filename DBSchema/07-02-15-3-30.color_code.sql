--
-- Table structure for table `color_code`
--

CREATE TABLE IF NOT EXISTS `color_code` (
`id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `color_code`
--
ALTER TABLE `color_code`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `color_code`
--
ALTER TABLE `color_code`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `execyoution`.`color_code` (`id`, `name`) VALUES (NULL, '#800080');
