-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2015 at 10:07 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `execyoution`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=258 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `code`) VALUES
(1, 'Afghanistan', 'AFG'),
(2, 'Albania', 'ALB'),
(3, 'Algeria', 'DZA'),
(4, 'American Samoa', 'ASM'),
(5, 'Andorra', 'AND'),
(6, 'Angola', 'AGO'),
(7, 'Anguilla', 'AIA'),
(8, 'Antarctica', 'ATA'),
(9, 'Antigua and Barbuda', 'ATG'),
(10, 'Argentina', 'ARG'),
(11, 'Armenia', 'ARM'),
(12, 'Aruba', 'ABW'),
(13, 'Australia', 'AUS'),
(14, 'Austria', 'AUT'),
(15, 'Azerbaijan', 'AZE'),
(16, 'Bahamas', 'BHS'),
(17, 'Bahrain', 'BHR'),
(18, 'Bangladesh', 'BGD'),
(19, 'Barbados', 'BRB'),
(20, 'Belarus', 'BLR'),
(21, 'Belgium', 'BEL'),
(22, 'Belize', 'BLZ'),
(23, 'Benin', 'BEN'),
(24, 'Bermuda', 'BMU'),
(25, 'Bhutan', 'BTN'),
(26, 'Bolivia', 'BOL'),
(27, 'Bosnia and Herzegovina', 'BIH'),
(28, 'Botswana', 'BWA'),
(29, 'Bouvet Island', 'BVT'),
(30, 'Brazil', 'BRA'),
(31, 'British Indian Ocean Territory', 'IOT'),
(32, 'Brunei Darussalam', 'BRN'),
(33, 'Bulgaria', 'BGR'),
(34, 'Burkina Faso', 'BFA'),
(35, 'Burundi', 'BDI'),
(36, 'Cambodia', 'KHM'),
(37, 'Cameroon', 'CMR'),
(38, 'Canada', 'CAN'),
(39, 'Cape Verde', 'CPV'),
(40, 'Cayman Islands', 'CYM'),
(41, 'Central African Republic', 'CAF'),
(42, 'Chad', 'TCD'),
(43, 'Chile', 'CHL'),
(44, 'China', 'CHN'),
(45, 'Christmas Island', 'CXR'),
(46, 'Cocos (Keeling) Islands', 'CCK'),
(47, 'Colombia', 'COL'),
(48, 'Comoros', 'COM'),
(49, 'Congo', 'COG'),
(50, 'Cook Islands', 'COK'),
(51, 'Costa Rica', 'CRI'),
(52, 'Cote D''Ivoire', 'CIV'),
(53, 'Croatia', 'HRV'),
(54, 'Cuba', 'CUB'),
(55, 'Cyprus', 'CYP'),
(56, 'Czech Republic', 'CZE'),
(57, 'Denmark', 'DNK'),
(58, 'Djibouti', 'DJI'),
(59, 'Dominica', 'DMA'),
(60, 'Dominican Republic', 'DOM'),
(61, 'East Timor', 'TLS'),
(62, 'Ecuador', 'ECU'),
(63, 'Egypt', 'EGY'),
(64, 'El Salvador', 'SLV'),
(65, 'Equatorial Guinea', 'GNQ'),
(66, 'Eritrea', 'ERI'),
(67, 'Estonia', 'EST'),
(68, 'Ethiopia', 'ETH'),
(69, 'Falkland Islands (Malvinas)', 'FLK'),
(70, 'Faroe Islands', 'FRO'),
(71, 'Fiji', 'FJI'),
(72, 'Finland', 'FIN'),
(74, 'France, Metropolitan', 'FRA'),
(75, 'French Guiana', 'GUF'),
(76, 'French Polynesia', 'PYF'),
(77, 'French Southern Territories', 'ATF'),
(78, 'Gabon', 'GAB'),
(79, 'Gambia', 'GMB'),
(80, 'Georgia', 'GEO'),
(81, 'Germany', 'DEU'),
(82, 'Ghana', 'GHA'),
(83, 'Gibraltar', 'GIB'),
(84, 'Greece', 'GRC'),
(85, 'Greenland', 'GRL'),
(86, 'Grenada', 'GRD'),
(87, 'Guadeloupe', 'GLP'),
(88, 'Guam', 'GUM'),
(89, 'Guatemala', 'GTM'),
(90, 'Guinea', 'GIN'),
(91, 'Guinea-Bissau', 'GNB'),
(92, 'Guyana', 'GUY'),
(93, 'Haiti', 'HTI'),
(94, 'Heard and Mc Donald Islands', 'HMD'),
(95, 'Honduras', 'HND'),
(96, 'Hong Kong', 'HKG'),
(97, 'Hungary', 'HUN'),
(98, 'Iceland', 'ISL'),
(99, 'India', 'IND'),
(100, 'Indonesia', 'IDN'),
(101, 'Iran (Islamic Republic of)', 'IRN'),
(102, 'Iraq', 'IRQ'),
(103, 'Ireland', 'IRL'),
(104, 'Israel', 'ISR'),
(105, 'Italy', 'ITA'),
(106, 'Jamaica', 'JAM'),
(107, 'Japan', 'JPN'),
(108, 'Jordan', 'JOR'),
(109, 'Kazakhstan', 'KAZ'),
(110, 'Kenya', 'KEN'),
(111, 'Kiribati', 'KIR'),
(112, 'North Korea', 'PRK'),
(113, 'Korea, Republic of', 'KOR'),
(114, 'Kuwait', 'KWT'),
(115, 'Kyrgyzstan', 'KGZ'),
(116, 'Lao People''s Democratic Republic', 'LAO'),
(117, 'Latvia', 'LVA'),
(118, 'Lebanon', 'LBN'),
(119, 'Lesotho', 'LSO'),
(120, 'Liberia', 'LBR'),
(121, 'Libyan Arab Jamahiriya', 'LBY'),
(122, 'Liechtenstein', 'LIE'),
(123, 'Lithuania', 'LTU'),
(124, 'Luxembourg', 'LUX'),
(125, 'Macau', 'MAC'),
(126, 'FYROM', 'MKD'),
(127, 'Madagascar', 'MDG'),
(128, 'Malawi', 'MWI'),
(129, 'Malaysia', 'MYS'),
(130, 'Maldives', 'MDV'),
(131, 'Mali', 'MLI'),
(132, 'Malta', 'MLT'),
(133, 'Marshall Islands', 'MHL'),
(134, 'Martinique', 'MTQ'),
(135, 'Mauritania', 'MRT'),
(136, 'Mauritius', 'MUS'),
(137, 'Mayotte', 'MYT'),
(138, 'Mexico', 'MEX'),
(139, 'Micronesia, Federated States of', 'FSM'),
(140, 'Moldova, Republic of', 'MDA'),
(141, 'Monaco', 'MCO'),
(142, 'Mongolia', 'MNG'),
(143, 'Montserrat', 'MSR'),
(144, 'Morocco', 'MAR'),
(145, 'Mozambique', 'MOZ'),
(146, 'Myanmar', 'MMR'),
(147, 'Namibia', 'NAM'),
(148, 'Nauru', 'NRU'),
(149, 'Nepal', 'NPL'),
(150, 'Netherlands', 'NLD'),
(151, 'Netherlands Antilles', 'ANT'),
(152, 'New Caledonia', 'NCL'),
(153, 'New Zealand', 'NZL'),
(154, 'Nicaragua', 'NIC'),
(155, 'Niger', 'NER'),
(156, 'Nigeria', 'NGA'),
(157, 'Niue', 'NIU'),
(158, 'Norfolk Island', 'NFK'),
(159, 'Northern Mariana Islands', 'MNP'),
(160, 'Norway', 'NOR'),
(161, 'Oman', 'OMN'),
(162, 'Pakistan', 'PAK'),
(163, 'Palau', 'PLW'),
(164, 'Panama', 'PAN'),
(165, 'Papua New Guinea', 'PNG'),
(166, 'Paraguay', 'PRY'),
(167, 'Peru', 'PER'),
(168, 'Philippines', 'PHL'),
(169, 'Pitcairn', 'PCN'),
(170, 'Poland', 'POL'),
(171, 'Portugal', 'PRT'),
(172, 'Puerto Rico', 'PRI'),
(173, 'Qatar', 'QAT'),
(174, 'Reunion', 'REU'),
(175, 'Romania', 'ROM'),
(176, 'Russian Federation', 'RUS'),
(177, 'Rwanda', 'RWA'),
(178, 'Saint Kitts and Nevis', 'KNA'),
(179, 'Saint Lucia', 'LCA'),
(180, 'Saint Vincent and the Grenadines', 'VCT'),
(181, 'Samoa', 'WSM'),
(182, 'San Marino', 'SMR'),
(183, 'Sao Tome and Principe', 'STP'),
(184, 'Saudi Arabia', 'SAU'),
(185, 'Senegal', 'SEN'),
(186, 'Seychelles', 'SYC'),
(187, 'Sierra Leone', 'SLE'),
(188, 'Singapore', 'SGP'),
(189, 'Slovak Republic', 'SVK'),
(190, 'Slovenia', 'SVN'),
(191, 'Solomon Islands', 'SLB'),
(192, 'Somalia', 'SOM'),
(193, 'South Africa', 'ZAF'),
(194, 'South Georgia &amp; South Sandwich Islands', 'SGS'),
(195, 'Spain', 'ESP'),
(196, 'Sri Lanka', 'LKA'),
(197, 'St. Helena', 'SHN'),
(198, 'St. Pierre and Miquelon', 'SPM'),
(199, 'Sudan', 'SDN'),
(200, 'Suriname', 'SUR'),
(201, 'Svalbard and Jan Mayen Islands', 'SJM'),
(202, 'Swaziland', 'SWZ'),
(203, 'Sweden', 'SWE'),
(204, 'Switzerland', 'CHE'),
(205, 'Syrian Arab Republic', 'SYR'),
(206, 'Taiwan', 'TWN'),
(207, 'Tajikistan', 'TJK'),
(208, 'Tanzania, United Republic of', 'TZA'),
(209, 'Thailand', 'THA'),
(210, 'Togo', 'TGO'),
(211, 'Tokelau', 'TKL'),
(212, 'Tonga', 'TON'),
(213, 'Trinidad and Tobago', 'TTO'),
(214, 'Tunisia', 'TUN'),
(215, 'Turkey', 'TUR'),
(216, 'Turkmenistan', 'TKM'),
(217, 'Turks and Caicos Islands', 'TCA'),
(218, 'Tuvalu', 'TUV'),
(219, 'Uganda', 'UGA'),
(220, 'Ukraine', 'UKR'),
(221, 'United Arab Emirates', 'ARE'),
(222, 'United Kingdom', 'GBR'),
(223, 'United States', 'USA'),
(224, 'United States Minor Outlying Islands', 'UMI'),
(225, 'Uruguay', 'URY'),
(226, 'Uzbekistan', 'UZB'),
(227, 'Vanuatu', 'VUT'),
(228, 'Vatican City State (Holy See)', 'VAT'),
(229, 'Venezuela', 'VEN'),
(230, 'Viet Nam', 'VNM'),
(231, 'Virgin Islands (British)', 'VGB'),
(232, 'Virgin Islands (U.S.)', 'VIR'),
(233, 'Wallis and Futuna Islands', 'WLF'),
(234, 'Western Sahara', 'ESH'),
(235, 'Yemen', 'YEM'),
(237, 'Democratic Republic of Congo', 'COD'),
(238, 'Zambia', 'ZMB'),
(239, 'Zimbabwe', 'ZWE'),
(242, 'Montenegro', 'MNE'),
(243, 'Serbia', 'SRB'),
(244, 'Aaland Islands', 'ALA'),
(245, 'Bonaire, Sint Eustatius and Saba', 'BES'),
(246, 'Curacao', 'CUW'),
(247, 'Palestinian Territory, Occupied', 'PSE'),
(248, 'South Sudan', 'SSD'),
(249, 'St. Barthelemy', 'BLM'),
(250, 'St. Martin (French part)', 'MAF'),
(251, 'Canary Islands', 'ICA'),
(252, 'Ascension Island (British)', 'ASC'),
(253, 'Kosovo, Republic of', 'UNK'),
(254, 'Isle of Man', 'IMN'),
(255, 'Tristan da Cunha', 'SHN'),
(256, 'Guernsey', 'GGY'),
(257, 'Jersey', 'JEY');
SET FOREIGN_KEY_CHECKS=1;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_country_id_foreign` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=303 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `name`, `code`) VALUES
(1, 223, 'Alaska', 'AK'),
(2, 223, 'American Samoa', 'AS'),
(3, 223, 'Arizona', 'AZ'),
(4, 223, 'Arkansas', 'AR'),
(5, 223, 'Armed Forces Africa', 'AE'),
(6, 223, 'Armed Forces Americas', 'AA'),
(7, 223, 'Armed Forces Canada', 'AE'),
(8, 223, 'Armed Forces Europe', 'AE'),
(9, 223, 'Armed Forces Middle East', 'AE'),
(10, 223, 'Armed Forces Pacific', 'AP'),
(11, 223, 'California', 'CA'),
(12, 223, 'Colorado', 'CO'),
(13, 223, 'Connecticut', 'CT'),
(14, 223, 'Delaware', 'DE'),
(15, 223, 'District of Columbia', 'DC'),
(16, 223, 'Federated States of Micronesia', 'FM'),
(17, 223, 'Florida', 'FL'),
(18, 223, 'Georgia', 'GA'),
(19, 223, 'Hawaii', 'HI'),
(20, 223, 'Idaho', 'ID'),
(21, 223, 'Illinois', 'IL'),
(22, 223, 'Indiana', 'IN'),
(23, 223, 'Iowa', 'IA'),
(24, 223, 'Kansas', 'KS'),
(25, 223, 'Kentucky', 'KY'),
(26, 223, 'Louisiana', 'LA'),
(27, 223, 'Maine', 'ME'),
(28, 223, 'Marshall Islands', 'MH'),
(29, 223, 'Maryland', 'MD'),
(30, 223, 'Massachusetts', 'MA'),
(31, 223, 'Michigan', 'MI'),
(32, 223, 'Minnesota', 'MN'),
(33, 223, 'Mississippi', 'MS'),
(34, 223, 'Missouri', 'MO'),
(35, 223, 'Montana', 'MT'),
(36, 223, 'Nebraska', 'NE'),
(37, 223, 'Nevada', 'NV'),
(38, 223, 'New Hampshire', 'NH'),
(39, 223, 'New Jersey', 'NJ'),
(40, 223, 'New Mexico', 'NM'),
(41, 223, 'New York', 'NY'),
(42, 223, 'North Carolina', 'NC'),
(43, 223, 'North Dakota', 'ND'),
(44, 223, 'Northern Mariana Islands', 'MP'),
(45, 223, 'Ohio', 'OH'),
(46, 223, 'Oklahoma', 'OK'),
(47, 223, 'Oregon', 'OR'),
(48, 223, 'Palau', 'PW'),
(49, 223, 'Pennsylvania', 'PA'),
(50, 223, 'Puerto Rico', 'PR'),
(51, 99, 'ANDAMAN & NICOBAR', ' '),
(52, 99, 'ANDHRA PRADESH', ' '),
(53, 99, 'ARUNACHAL PRADESH', ' '),
(54, 99, 'ASSAM', ' '),
(55, 99, 'BIHAR', ' '),
(56, 99, 'CHANDIGARH', ' '),
(57, 99, 'CHATTISGARH', ' '),
(58, 99, 'DADRA & NAGAR', ' '),
(59, 99, 'DAMAN & DIU', ' '),
(60, 99, 'DELHI', ' '),
(61, 99, 'GOA', ' '),
(62, 99, 'GUJRAT', ' '),
(63, 99, 'HARYANA', ' '),
(64, 99, 'HIMACHAL PRADESH', ' '),
(65, 99, 'JAMMU & KASHMIR', ' '),
(66, 99, 'JHARKHAND', ' '),
(67, 99, 'KARNATAKA', ' '),
(68, 99, 'KERALA', ' '),
(69, 99, 'LAKSHDWEEP', ' '),
(70, 99, 'MADHYA PRADESH', ' '),
(71, 99, 'MAHARASHTRA', ' '),
(72, 99, 'MANIPUR', ' '),
(73, 99, 'MEGHALAYA', ' '),
(74, 99, 'MIZORAM', ' '),
(75, 99, 'NAGALAND', ' '),
(76, 99, 'ORISSA', ' '),
(77, 99, 'PONDICHERY', ' '),
(78, 99, 'PUNJAB', ' '),
(79, 99, 'RAJASTHAN', ' '),
(80, 99, 'SIKKIM', ' '),
(81, 99, 'TAMIL NADU', ' '),
(82, 99, 'TRIPURA', ' '),
(83, 99, 'UTTAR PRADESH', ' '),
(84, 99, 'UTTARANCHAL', ' '),
(85, 99, 'WEST BENGAL', ' '),
(86, 38, 'Alberta', 'AB'),
(87, 38, 'British Columbia', 'BC'),
(88, 38, 'Manitoba', 'MB'),
(89, 38, 'New Brunswick', 'NB'),
(90, 38, 'Newfoundland and Labrador', 'NL'),
(91, 38, 'Northwest Territories', 'NT'),
(92, 38, 'Nova Scotia', 'NS'),
(93, 38, 'Nunavut', 'NU'),
(94, 38, 'Ontario', 'ON'),
(95, 38, 'Prince Edward Island', 'PE'),
(96, 38, 'Quebec', 'QC'),
(97, 38, 'Saskatchewan', 'SK'),
(98, 38, 'Yukon Territory', 'YT'),
(99, 221, 'Abu Dhabi', ''),
(100, 221, 'Ajman', ''),
(101, 221, 'Al Ain', ''),
(102, 221, 'Aweer', ''),
(103, 221, 'Chennai', 'mannually'),
(104, 221, 'Dhayd', ''),
(105, 221, 'Dibba', ''),
(106, 221, 'Dubai', ''),
(107, 221, 'Falaj-Al-Moalla', ''),
(108, 221, 'Fujairah', ''),
(109, 221, 'Khawanij', ''),
(110, 221, 'Ras-Al-Khaimah', ''),
(111, 221, 'Sharjah', ''),
(112, 221, 'Tarif', ''),
(113, 221, 'Umm-Al-Quwain', ''),
(114, 17, 'A''ali', ''),
(115, 17, 'Adliya', ''),
(116, 17, 'Al-Malikiyah', ''),
(117, 17, 'Budaiya', ''),
(118, 17, 'Hamad Town', ''),
(119, 17, 'Isa Town-', ''),
(120, 17, 'Jidhafs', ''),
(121, 17, 'Manama', ''),
(122, 17, 'Muharraq', ''),
(123, 17, 'Riffa', ''),
(124, 17, 'Sanabis', ''),
(125, 17, 'Sitra', ''),
(126, 17, 'Tubli', ''),
(127, 173, 'Abu az Zuluf', ''),
(128, 173, 'Abu Thaylah', ''),
(129, 173, 'Ad Dawhah al Jadidah', ''),
(130, 173, 'Al `Adhbah', ''),
(131, 173, 'Al `Arish', ''),
(132, 173, 'Al Bida` ash Sharqiyah', ''),
(133, 173, 'Al Ghanim', ''),
(134, 173, 'Al Ghuwariyah', ''),
(135, 173, 'Al Hilal al Gharbiyah', ''),
(136, 173, 'Al Hilal ash Sharqiyah', ''),
(137, 173, 'Al Hitmi', ''),
(138, 173, 'Al Jasrah', ''),
(139, 173, 'Al Jumaliyah', ''),
(140, 173, 'Al Ka`biyah', ''),
(141, 173, 'Al Khalifat', ''),
(142, 184, 'Abha', ''),
(143, 184, 'Abqaiq', ''),
(144, 184, 'Al Khobar', ''),
(145, 184, 'Al Markazi', ''),
(146, 184, 'Al Ulaya', ''),
(147, 184, 'Al-Ahsa', ''),
(148, 184, 'Al-Baha', ''),
(149, 184, 'Al-Khobar', ''),
(150, 184, 'Al-Qassim', ''),
(151, 184, 'Buraydah', ''),
(152, 184, 'Dammam', ''),
(153, 184, 'Dhahran', ''),
(154, 184, 'Hafer Al Batin', ''),
(155, 184, 'Hofuf', ''),
(156, 184, 'Jeddah', ''),
(157, 184, 'Jubail', ''),
(158, 184, 'Khamis Mushaait', ''),
(159, 184, 'Madinah Al Munawwara', ''),
(160, 184, 'Mecca (Makkah)', ''),
(161, 184, 'Medina (Albalad)', ''),
(162, 184, 'Najran', ''),
(163, 184, 'Qatif', ''),
(164, 184, 'Ras Tanura', ''),
(165, 184, 'Riyadh', ''),
(166, 184, 'Sayhat', ''),
(167, 184, 'Tabuk', ''),
(168, 184, 'Taif Wajj (Central)', ''),
(169, 184, 'Yenbu', ''),
(170, 114, 'Ali al Salem', ''),
(171, 114, 'Jahrah', ''),
(172, 114, 'Kuwait City', ''),
(173, 114, 'Safat Kuwait', ''),
(174, 114, 'Salmiya', ''),
(175, 235, 'Abyan', ''),
(176, 235, 'Ad Dali', ''),
(177, 235, 'Adan', ''),
(178, 235, 'Al Bayda''', ''),
(179, 235, 'Al Hudaydah', ''),
(180, 235, 'Al Jawf', ''),
(181, 235, 'Al Mahrah', ''),
(182, 235, 'Al Mahwit', ''),
(183, 235, 'Amran', ''),
(184, 235, 'Dhamar', ''),
(185, 235, 'Hadramawt', ''),
(186, 235, 'Hajjah', ''),
(187, 235, 'Ibb', ''),
(188, 235, 'Lahij', ''),
(189, 235, 'Ma''rib', ''),
(190, 235, 'Sa''dah', ''),
(191, 235, 'San''a''', ''),
(192, 235, 'Shabwah', ''),
(193, 235, 'Taizz', ''),
(194, 102, 'Al Anbar', ''),
(195, 102, 'Al Basrah', ''),
(196, 102, 'Al Muthanna', ''),
(197, 102, 'Al Qadisiyah', ''),
(198, 102, 'An Najaf', ''),
(199, 102, 'Arbil', ''),
(200, 102, 'As Sulaymaniyah', ''),
(201, 102, 'At Ta''mim', ''),
(202, 102, 'Babil', ''),
(203, 102, 'Baghdad', ''),
(204, 102, 'Dahuk', ''),
(205, 102, 'Dhi Qar', ''),
(206, 102, 'Diyala', ''),
(207, 102, 'Karbala''', ''),
(208, 102, 'Maysan', ''),
(209, 102, 'Ninawa', ''),
(210, 102, 'Salah ad Din', ''),
(211, 102, 'Wasit', ''),
(212, 168, 'ARMM', ''),
(213, 168, 'Bicol Region', ''),
(214, 168, 'Cagayan Valley', ''),
(215, 168, 'CAR', ''),
(216, 168, 'Caraga', ''),
(217, 168, 'Central Luzon', ''),
(218, 168, 'Central Mindanao', ''),
(219, 168, 'Central Visayas', ''),
(220, 168, 'Eastern Visayas', ''),
(221, 168, 'National Capital Region', ''),
(222, 121, 'Agelat', ''),
(223, 121, 'Benghazi', ''),
(224, 121, 'Benina', ''),
(225, 121, 'Derna', ''),
(226, 121, 'Misurata (Misuratha)', ''),
(227, 121, 'Sabratha', ''),
(228, 121, 'Sebha', ''),
(229, 121, 'Taigura', ''),
(230, 121, 'Tripoli', ''),
(231, 121, 'Tripoli International Airport', ''),
(232, 121, 'Zawia', ''),
(233, 121, 'Zuara', ''),
(234, 63, 'Alexandria', ''),
(235, 63, 'Aswan', ''),
(236, 63, 'Asyut', ''),
(237, 63, 'Benha', ''),
(238, 63, 'Cairo', ''),
(239, 63, 'Damanhour', ''),
(240, 63, 'El Mahallah (El Kubra)', ''),
(241, 63, 'El Mansoura', ''),
(242, 63, 'Luxor', ''),
(243, 63, 'Mahalah El Kobra', ''),
(244, 63, 'Oena', ''),
(245, 63, 'Port Said', ''),
(246, 63, 'Shebin El Kom', ''),
(247, 63, 'Sohag', ''),
(248, 63, 'Suez', ''),
(249, 63, 'Tanta', ''),
(250, 144, 'Chaouia-Ouardigha', ''),
(251, 144, 'Doukkala-Abda', ''),
(252, 144, 'Fes-Boulemane', ''),
(253, 144, 'Gharb-Chrarda-Beni Hssen', ''),
(254, 144, 'Grand Casablanca', ''),
(255, 144, 'Guelmim-Es Smara', ''),
(256, 144, 'Marrakech-Tensift-Al Haouz', ''),
(257, 144, 'Meknes-Tafilalet', ''),
(258, 144, 'Oriental', ''),
(259, 144, 'Rabat-Sale-Zemmour-Zaer', ''),
(260, 144, 'Tadla-Azilal', ''),
(261, 144, 'Tanger-Tetouan', ''),
(262, 144, 'Taza-Al Hoceima-Taounate', ''),
(263, 118, 'Ajaltoun - Keserwan', ''),
(264, 118, 'Aley', ''),
(265, 118, 'Baabdat', ''),
(266, 118, 'Bartoun - North', ''),
(267, 118, 'Beirut', ''),
(268, 118, 'Beit Mery', ''),
(269, 118, 'Bhamdoun', ''),
(270, 118, 'Bikfaya - Metn', ''),
(271, 118, 'Broumana', ''),
(272, 118, 'Faraya', ''),
(273, 118, 'Jounieh', ''),
(274, 118, 'Jubail', ''),
(275, 118, 'North Bartoun', ''),
(276, 118, 'Saidon', ''),
(277, 118, 'South Tyre', ''),
(278, 118, 'Tripoli', ''),
(279, 118, 'Tyre - South', ''),
(280, 118, 'Zahle - Bekaa', ''),
(281, 118, 'Zghorta', ''),
(282, 118, 'Zouk Mikael', ''),
(283, 162, 'Karachi', ''),
(284, 162, 'Lahore', 'LHR'),
(285, 162, 'Faisalabad', 'FSD'),
(286, 162, 'Bahawalpur', ''),
(287, 162, 'Sheikhupura', ''),
(288, 162, 'Quetta', ''),
(289, 162, 'Mardan', ''),
(290, 162, 'Islamabad', ''),
(291, 149, 'Kathmandu', ''),
(292, 149, 'Lalitpur', ''),
(293, 149, 'Dhangadhi', ''),
(294, 149, 'Madhyapur Thimi', ''),
(295, 149, 'Damak', ''),
(296, 149, 'Dang Deukhuri', ''),
(297, 149, 'Gadhimai', ''),
(298, 149, 'Hariwan', ''),
(299, 30, 'Guarulhos', ''),
(300, 30, 'Campo Grande', ''),
(301, 30, 'Fortaleza', ''),
(302, 30, 'Contagem', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE;
SET FOREIGN_KEY_CHECKS=1;

--
-- Table structure for table `attachement`
--

CREATE TABLE IF NOT EXISTS `attachement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `object_type` enum('project','target') NOT NULL,
  `file` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Demo', 'Demo', 0, '2015-01-29 13:20:06', '2015-01-29 13:20:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `behaviors`
--

CREATE TABLE IF NOT EXISTS `behaviors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `behavior_type` int(11) NOT NULL,
  `assessment_type` enum('observed','assessed') NOT NULL,
  `color_code` varchar(100) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `behavior_types`
--

CREATE TABLE IF NOT EXISTS `behavior_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `business_perspectives`
--

CREATE TABLE IF NOT EXISTS `business_perspectives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `busp_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `busp_description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `busp_name` (`busp_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `business_perspectives`
--

INSERT INTO `business_perspectives` (`id`, `busp_name`, `busp_description`, `created_at`, `updated_at`) VALUES
(4, 'Demo sadasd', 'Demo sadasda', '2015-01-30 20:45:45', '2015-01-30 16:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address_1` varchar(150) NOT NULL,
  `address_2` varchar(150) NOT NULL,
  `city` int(11) NOT NULL,
  `emirate` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `post_code` varchar(10) NOT NULL,
  `po_box` varchar(10) NOT NULL,
  `website` varchar(255) NOT NULL,
  `total_headcount` double NOT NULL,
  `wc_hc` varchar(100) NOT NULL,
  `bc_hc` varchar(100) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `address_1`, `address_2`, `city`, `emirate`, `country`, `post_code`, `po_box`, `website`, `total_headcount`, `wc_hc`, `bc_hc`, `phone_number`) VALUES
(1, 'Wisdom', '', '', 0, 0, 0, '', '', '', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `company_logs`
--

CREATE TABLE IF NOT EXISTS `company_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `solution` enum('Y','N') NOT NULL,
  `solution_color_code` enum('red','amber','green') NOT NULL,
  `implemented` enum('Y','N') NOT NULL,
  `implemented_color_code` enum('red','amber','green') NOT NULL,
  `date_entry` date NOT NULL,
  `date_solution` date NOT NULL,
  `date_implementation` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`,`user_id`,`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `dpt_name` varchar(100) NOT NULL,
  `dpt_description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dpt_name` (`dpt_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `company_id`, `dpt_name`, `dpt_description`) VALUES
(1, 1, 'Casual', 'Casual');

-- --------------------------------------------------------

--
-- Table structure for table `discussion`
--

CREATE TABLE IF NOT EXISTS `discussion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `object_type` enum('project','target') NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_request`
--

CREATE TABLE IF NOT EXISTS `email_request` (
  `id` int(11) NOT NULL,
  `sender_name` varchar(100) NOT NULL,
  `sender_email` varchar(255) NOT NULL,
  `sender_phone` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` enum('contact','recommendation') NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `goals`
--

CREATE TABLE IF NOT EXISTS `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persp_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `persp_id` (`persp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Users', '{"users":1}', '2014-12-10 22:53:31', '2014-12-10 22:53:31'),
(2, 'Admins', '{"admin":1,"users":1}', '2014-12-10 22:53:31', '2014-12-10 22:53:31'),
(3, 'Normal User', '{"users":1}', '2014-12-25 13:54:12', '2014-12-25 13:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `banner_id`, `title`, `link`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Demo', '#', 'banners/F6fPNb1l/Screenshot from 2015-01-19 17:07:57.png', 1, '2015-01-29 13:20:06', '2015-01-29 13:20:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `log_type`
--

CREATE TABLE IF NOT EXISTS `log_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('lpg_0_0_1_create_albums_table', 2),
('lpg_0_0_2_create_photos_table', 2),
('lpg_0_0_3_add_soft_deleting', 2),
('lpg_0_0_4_add_ordering', 2);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_category` enum('meeting','regulation','page') NOT NULL,
  `title` varchar(255) NOT NULL,
  `seo_url` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `seo_url` (`seo_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_category`, `title`, `seo_url`, `content`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(22, 'page', 'Terms of Service', 'terms-of-service', '<h1 class="page-header" style="box-sizing: border-box; margin: 12px 0px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); padding: 0px 0px 11px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(238, 238, 238);">Terms of Service&nbsp;<small style="box-sizing: border-box; font-size: 20.7999992370605px; line-height: 1; color: rgb(153, 153, 153);"><em style="box-sizing: border-box;">Last revised on July 26, 2011</em></small></h1>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Your relationship with Noomii</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">NOOMII (www.noomii.com or &ldquo;the Website&rdquo; or &ldquo;the Site&rdquo;) is an online resource for coaches and for people looking to work with and hire life and business coaches. Noomii offers a coach directory and collaborative tools that enable coaches to manage their client information online. The Noomii Website Service, hereafter referred to as &ldquo;the Service&rdquo; or &ldquo;the Services&rdquo;, is operated by PairCoach Enterprises Inc. If you continue to browse and use the Website, you are agreeing to comply with and be bound by the following terms and conditions of use (the &ldquo;Terms&rdquo;), which together with our&nbsp;<a href="http://www.noomii.com/privacy-policy" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;">privacy policy</a>&nbsp;govern the Company&rsquo;s relationship with you in relation to the Service.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">The term &ldquo;Noomii&rdquo; or &ldquo;PairCoach&rdquo; or &ldquo;us&rdquo; or &ldquo;we&rdquo; or &ldquo;the Company&rdquo; or &ldquo;our Company&rdquo; refers to the owner of the Website, PairCoach Enterprises Inc., whose principle place of business is 305 - 896 Cambie St., Vancouver, British Columbia, V6B 2P6, Canada. The Company is incorporated under the Canadian Business Corporations Act, corporation number 676403-7, and registered extra-provincially in British Columbia under number A0071080. The term &ldquo;you&rdquo; refers to the user or viewer of the Website.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Accepting the Terms</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">In order to use the Website, you must first agree to the Terms. You may not use the Service if you do not accept the Terms. You can accept the Terms by: (a) clicking to accept or agree to the Terms, where this option is made available to you by Noomii in the user interface; or (b) by actually using the Website. In this case, you understand and agree that Noomii will treat your use of the Website as acceptance of the Terms from that point onwards.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">You may not use the Service and may not accept the Terms if (a) you are not of legal age in your jurisdiction of primary residency to form a binding contract with Noomii, or (b) you are a person barred from receiving the Services under any relevant laws of any jurisdiction that affects you.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">You may not use a false e-mail address, impersonate any person or entity, or otherwise mislead as to the origin of yourself or of any content you may contribute to the Service. Noomii reserves the right (but not the obligation) to terminate such accounts, and in cases of misappropriation of identity or fraud, take all further legal action as we deem appropriate.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Changing the Terms</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We reserve the right, at our sole discretion, to change, modify, add, or delete portions of these Terms at any time without further notice. If we do this, we will post the changes to these Terms on this page and will indicate at the top of this page the date these Terms were last revised. Your continued use of the Service after any such changes constitutes your acceptance of the new Terms. If you do not agree to abide by these or any future Terms, do not use or access (or continue to use or access) the Service. It is your responsibility to regularly check the Website to determine if there have been changes to these Terms of Service and to review such changes.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Using the Service</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii grants you a limited license to access and make personal use of this Website. This license does not include: (a) any resale or commercial use of this Site or its contents, except in the case of professional life coaches who sign up and become &ldquo;Pro Coach&rdquo; members (as hereinafter defined) under one of the packages offered to professional life coaches hereunder, in which case the Professional Coach Terms of Use (as hereinafter attached) shall govern permitted commercial uses; (b) any collection and use of any postings; (c) any derivative commercial use of this Site or its contents; (d) or any commercial use of data mining, robots, or similar data gathering and extraction tools. No portion of this Site may be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of Noomii. You may not frame or utilize framing techniques to enclose any of our trademarks, logos, or other proprietary information (including images, text, page layout, or form). You may not use any meta tags or any other &ldquo;hidden text&rdquo; utilizing our name, trademarks or other proprietary information.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">If you have registered with Noomii, you are responsible for maintaining the confidentiality of log-in information and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password regardless of who actually uses your account name or password.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Once you have registered with the Service you may choose to be a recipient of coaching services offered by a Pro Coach (as hereinafter defined). At any one time, you can have an unlimited number of Pro Coaches. You agree that all interaction with your Pro Coach(es) (as applicable) shall be respectful and positive, and shall under no circumstances be abusive, derogatory, defamatory, sexist, racist, illegal or false. You shall also keep all personal information that you learn or discover from any member or Pro Coach as completely confidential, and shall not disclose this information to anyone without the permission of the member or the Pro Coach. Exceptions to this are situations where: (a) you have a relationship with more than one Pro Coaches in which case your Pro Coach(es) may all communicate with each other but to no other party; (b) where required by law; (c) in situations where an imminent risk to someone may be involved; or (d) with express permission from us.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">The Website and the Services offered herein are provided solely as a forum and technical means to connect two people interested in having a coaching conversation and does not represent itself as being licensed to provide counseling, psychotherapy, or psychiatric evaluation, psychiatric therapy, or any other service requiring professional accreditation.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">You are advised that there are clear boundaries surrounding the responsibilities between coaching and psychotherapy, counseling, or other medical treatments in psychology and psychiatry. Specifically, coaching involves providing education and guidance to those seeking to learn and implement a self-directed discovery process respecting their own lives. However, it is important to understand that this is different than psychotherapy, counseling, or other medical treatments in psychology and psychiatry. While both utilize knowledge of human behavior, motivation and behavioral change and interactive techniques, the major differences are in the goals, focus, and level of professional responsibility. Psychotherapy, counseling, or other medical treatments in psychology and psychiatry are health care services and are usually reimbursable through health insurance policies and require licensed health care professionals with accredited training to render. This is not true for coaching. The focus of coaching is development and implementation of strategies to reach client-identified goals of enhanced performance and personal satisfaction. Coaching utilizes personal strategic planning, values clarification, brainstorming, motivational skills, and other similar techniques. However, coaching does not encroach into the areas involving use of skills that only licensed professionals are permitted to deliver. Coaching does not diagnose or treat mental health disorders. It is your responsibility to manage your own mental health, including obtaining necessary exams and ongoing support as needed. Coaching should be seen as a supplement to such a support system but not as a replacement for it.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">In situations where you are under the care of a professional counselor or medical practitioner and conflict arises between the recommendations of those professionals and your coach, you have the responsibility to follow the guidance of the professionals.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We do not sponsor, endorse, recommend or approve of any coach, therapist or other related service provider who offers services through this Site. Specifically, we do not endorse any Pro Coach members merely because they have signed up as a Pro Coach member. You should use your independent judgment of the merits of any individual, entity or information that you find on or through this Site. The Company cannot and does not represent or warrant that any coach, therapist or other related service provider who offers services through this Site is licensed, qualified, or capable of performing any such service.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Neither Noomii nor any third parties provide any warranty or guarantee as to the timeliness, accuracy, performance, completeness or suitability of the information or material found or offered on the Website provided by any coach, therapist or other related service provider. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Your use of any information or materials on this Website is entirely at your own risk, for which Noomii shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this Website, its partners, or affiliates, meet your specific requirements.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">From time to time this Website may also include links to other websites. These links are provided for your convenience to provide further information which may include offers to buy or use other products or services. We have no responsibility for the content of the linked websites or any products or services used or purchased though them.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Ownership of Content</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Although Noomii owns the code, databases, and all rights to the Noomii website application, you retain all rights to your data.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">You acknowledge and agree that any questions, comments, ideas, suggestions for new features, suggestions for improvements to existing features, feedback or other information about the Site or the Service (&ldquo;Submissions&rdquo;), provided by you to the Website or to the Company are non-confidential and shall become the sole property of the Company. The Company shall own exclusive rights, including all intellectual property rights, and shall be entitled to the unrestricted use and dissemination of these Submissions for any purpose, commercial or otherwise, without acknowledgment or compensation to you.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Disclaimer and Limitations on Liability</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">The Website, the Service, and the Website content are provided &ldquo;as-is&rdquo; and the company disclaims any and all representations and warranties, whether express or implied, including implied warranties of title, merchantability, fitness for a particular purpose or non-infringement. The Company cannot guarantee and does not promise any specific results from use of the Website or the Service. Company does not represent or warrant that the content on the Website is accurate, complete, reliable, current or error-free or that the Website is free of viruses or other harmful components. Without limiting the foregoing, you understand and agree that if you download or otherwise obtain content, material, or data from the Website or through the Service, you do so at your own discretion and risk. In no event will the company or its directors, officers, advisors, employees, contractors, affiliates or agents be liable to you or any third person for any indirect, consequential, exemplary, incidental, special or punitive damages, including for any lost profits or lost data arising from your use of the Site or the Service.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">For greater clarity, Noomii shall not be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, liability arising out of or in any way connected with or otherwise arising out of the use of the Website or Services, whether based on contract, tort, negligence, strict liability or otherwise. Under no circumstances will the Company be responsible for any loss or damage or personal injury or death resulting from anyone&rsquo;s use of the Site or the Service or any interaction between users of the Site, whether online or offline.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Miscellaneous Terms</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">The Site and the Service may be temporarily unavailable from time to time for maintenance or other reasons.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">The Company reserves the right to change any and all content contained in the Site and any Services offered through the Site at any time without notice.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Reference to any products, Services, processes or other information, by trade name, trademark, manufacturer, supplier or otherwise does not constitute or imply endorsement, sponsorship or recommendation thereof, or any affiliation therewith, by Company.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">The Company may terminate your membership, delete your profile and any content or information that you have posted on the Website or prohibit you from using or accessing the Service or the Website (or any portion, aspect or feature of the Service or the Website) for any reason, or no reason, at any time in its sole discretion, with or without notice.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">These Terms of Service constitute the entire agreement between you and Company regarding the use of the Site or the Service, superseding any prior agreements between you and Company relating to your use of the Site or the Service. The failure of Company to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision in that or any other instance. If any provision of this Agreement is held invalid, the remainder of these Terms shall continue in full force and effect. If any provision of these Terms of Service shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms of Service and shall not affect the validity and enforceability of any remaining provisions.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">By visiting or using the Site and the Service, you agree that the laws of the Province of British Columbia, Canada, without regard to principles of conflict of laws, will govern these Terms and any dispute of any sort that might arise between you and the Company or any of our affiliates. With respect to any disputes or claims, you agree not to commence or prosecute any action in connection therewith other than in the provincial and federal courts of British Columbia, and you hereby consent to, and waive all defenses of lack of personal jurisdiction and forum non conveniens with respect to, venue and jurisdiction in the provincial and federal courts of British Columbia.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Professional Coach Terms of Use</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii also provides a set of professional coach features that enable professional life coaches to market their services, provide expert advice to Noomii members, and manage one or more on-going coaching client relationships.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">This section applies to all members who apply for, or have received approval, of their membership as a member in this section, and will be referred to herein as a &ldquo;Pro Coach&rdquo; or &ldquo;Pro Coaches&rdquo;. This section applies in addition to, not in place of, the preceding general Terms of Use.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Pro Coach services are offered in the following packages:</p>\r\n\r\n<ul style="box-sizing: border-box; margin-top: 0px; margin-bottom: 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">\r\n	<li style="box-sizing: border-box;">Pro Coach Free</li>\r\n	<li style="box-sizing: border-box;">Pro Coach Basic</li>\r\n	<li style="box-sizing: border-box;">Pro Coach Premium</li>\r\n	<li style="box-sizing: border-box;">Pro Coach Premium Plus</li>\r\n</ul>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">A Pro Coach member chooses to subscribe to ONE (1) of the above at a time.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Details of what each package contains is set forth in more detail in our website.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Pro Coach Representations and Warranties</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Throughout the time during which you are applying for or are registered for a Pro Coach package, you represent, warrant and agree to the following:</p>\r\n\r\n<ul style="box-sizing: border-box; margin-top: 0px; margin-bottom: 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">\r\n	<li style="box-sizing: border-box;">You will not make any misrepresentations nor create a misleading name or listing.</li>\r\n	<li style="box-sizing: border-box;">You will at all times provide correct and accurate representations of your skills, degrees, qualifications, background and other information and will maintain and update all such information to keep it true, accurate, current and complete.</li>\r\n	<li style="box-sizing: border-box;">You will not provide any medical or mental health advice or other advice or information which may only be lawfully rendered or provided by a licensed professional who has established a physician-patient relationship. You will not provide any medical or legal information unless you are a licensed professional in good standing in the relevant field of expertise and you abide by all relevant laws, rules and regulations, including, without limitation rules of ethics and professional responsibility.</li>\r\n	<li style="box-sizing: border-box;">You must exercise a very high standard of care, equal to that of an experienced, professional coach would in a similar transaction not conducted through the internet, or to the standard of care mandated by the professional associations for which you are a member, whichever is higher.</li>\r\n</ul>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii makes no representation or warranty whatsoever as to the willingness or ability of Members to pay for any coaching services given by the Pro Coach. Therefore, you, the Pro Coach, are encouraged to verify the details and credit-worthiness of those Members to whom you give or contemplate giving life coaching services.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Pro Coach Disclaimer of Warranty and Limitation of Liability</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">All users of Pro Coach services, whether it is the Pro Coach member or the recipient of the Pro Coach&rsquo;s services (the &ldquo;Recipient&rdquo;) do so at their own respective risks. Under no circumstances will Noomii, PairCoach, their affiliates and respective officers, directors, shareholders, employees, sub-contractors or agents be liable for any indirect, incidental, consequential, special, punitive or exemplary damages (including but not limited to loss of business, revenue, profits, use, data or other economic advantage), however it arises, whether in action of contract, negligence or other tortious action, or arising from the Services or any other use of this Website.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">As a Pro Coach, you acknowledge and agree that Noomii disclaims any liability with respect to any claim, suit or action brought by a member in connection with the provision of any life coaching services by you through Website and you agrees to indemnify and hold us harmless in connection with any such claim and any damages or expenses arising therefrom. We will not be liable for enforcing any Agreement that was made between you and any Recipient or other member, including fee agreements. You further acknowledge that you will solely be responsible and liable for any damages to any member to whom you provide life coaching services and where that member suffers damages arising from or connected to such services. In the event of a dispute regarding any transaction conducted through the Website, you hereby releases Noomii and its affiliates, and their respective officers, directors, shareholders, employees, sub-contractors and agents from all manner of actions, claims or demands and from any and all losses (direct, indirect, incidental or consequential), damages, costs or expenses, including, without limitation, court costs and attorney&#39;s fees, which you may have against one or more of the above.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Pro Coach Confidentiality</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Once you have registered as a Pro Coach, you will be able to offer your services to Recipients. Recipients can only have one Partner at a time and multiple Pro Coaches at a time. You agree that all interaction with the Recipient and the Recipient&rsquo;s Partner (as applicable) shall be respectful and positive, and shall under no circumstances be abusive, derogatory, defamatory, sexist, racist, illegal or false. You shall also keep all information that you learn or discover from any Recipient, Recipient&rsquo;s Partner, or Recipient&rsquo;s other Pro Coaches as completely confidential, and shall not disclose this information to anyone without the permission of the Recipient, the Recipient&rsquo;s Partner, or the Recipient&rsquo;s other Pro Coaches. Exceptions to this are situations where: (a) you are legitimately using the Pro Coaching services hereinafter defined in which case you, the Recipient, the Recipient&rsquo;s Partner, and the Recipient&rsquo;s other Pro Coaches may all communicate with each other but to no other party; (b) where required by law; (c) in situations where an imminent risk to someone may be involved; or (d) with express permission from us.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Charges and Payment of Fees</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">You shall pay all fees or charges to your account in accordance with the fees, charges, and billing terms in effect at the time a fee or charge is due and payable. Payments are collected monthly in advance of the professional coach services being provided. We will automatically bill your credit card and issue a billing statement each month. We reserves the right to modify the fees and charges and to introduce new charges at any time, upon at least THIRTY (30) DAYS prior notice to you, which notice may be provided by e-mail.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">If you upgrade from a paid package to a more expensive package, you will be charged the new rate immediately and your recurring billing date will be adjusted to the date you last upgraded. For example, if you upgrade on January 1st and then upgrade to a more expensive package on January 15th, your next automatic credit card charge will occur on February 15th (or the next business day should the 15th fall on a weekend or statutory holiday). If you cancel your account or downgrade to a non-paying account, the changes to your account will take effect immediately and you won&#39;t be charged again. Noomii does not require long-term contracts or provide refunds. Please read our&nbsp;<a href="http://www.noomii.com/refund-policy" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;">refund policy</a>&nbsp;for more details.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Fees for other services will be charged on an as-quoted basis. All prices quoted are in US dollars.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">You agree to provide us with complete and accurate billing and contact information. You agree to update this information within THIRTY (30) DAYS of any change to it. If the contact information you have provided is false or fraudulent, we reserve the right to terminate your Pro Coach membership.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Credit System for Bidding on Potential Recipients of Coaching Services</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii has developed a credit system for bidding on individuals who have expressed an interest in becoming a Recipient of coaching services. Pro Coaches are provided a monthly allotment a credits each month as long as they remain on one of Noomii&#39;s paid packages.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii credits do not have cash value. They can not be redeemed for money, traded, or given away.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii reserves the right to change or remove its credit system at any time without notice, including the removal of any unused credits.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Non-Payment and Suspension</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">If your account becomes delinquent (falls into arrears) for more than TWO (2) WEEKS, we will restrict access to your paid Pro Coach membership features and downgrade you to the Pro Coach Free account. To reactivate access to your data and subscribed Pro Coach features, you may do so at any time by providing updated payment information and paying all outstanding charges.</p>\r\n', 'Terms of Service', 'Terms of Service', 'Terms of Service', 1, '2014-12-19 00:37:06', '2014-10-27 03:15:12');
INSERT INTO `pages` (`id`, `page_category`, `title`, `seo_url`, `content`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(23, '', 'Privacy Policy', 'Privacy-Policy', '<h1 class="page-header" style="box-sizing: border-box; margin: 12px 0px; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); padding: 0px 0px 11px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(238, 238, 238);">Noomii Privacy Policy&nbsp;<small style="box-sizing: border-box; font-size: 20.7999992370605px; line-height: 1; color: rgb(153, 153, 153);"><em style="box-sizing: border-box;">Effective May 26, 2014</em></small></h1>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Noomii&#39;s Privacy Policy</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii.com (http://www.noomii.com or &ldquo;Noomii&rdquo; or the &ldquo;Website&rdquo; or the &ldquo;Site&rdquo;) is a directory of professional life, career, and business coaches (hereinafter referred to as a Coach or Coaches). As a Coach, Noomii helps you get new clients. As an individual or potential coaching client, Noomii helps you find a coach based on your goals and needs. This Privacy Policy (the &ldquo;Policy&rdquo;) is designed to help you understand how we collect and use personal information, and how we handle privacy issues.</p>\r\n\r\n<div class="pull-right" style="box-sizing: border-box; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; float: right !important;"><a href="http://privacy-policy.truste.com/click-with-confidence/ctv/en/www.noomii.com/seal_m" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;" target="_blank"><img alt="Validate TRUSTe privacy certification" src="http://info.truste.com/rs/truste/images/CP_EN_M.png" style="box-sizing: border-box; border: 0px; vertical-align: middle;" /></a></div>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii has been awarded TRUSTe&#39;s Privacy Seal signifying that this privacy policy and practices have been reviewed by TRUSTe for compliance with&nbsp;<a href="http://www.truste.com/privacy_seals_and_services/consumer_privacy/privacy-programs-requirements.html" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;" target="_blank">TRUSTe&#39;s program requirements</a>&nbsp;including transparency, accountability and choice regarding the collection and use of your personal information. TRUSTe&#39;s mission, as an independent third party, is to accelerate online trust among consumers and organizations globally through its leading privacy trustmark and innovative trust solutions. If you have questions or complaints regarding our privacy policy or practices, please contact us at<a href="mailto:privacy@noomii.com" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;">privacy@noomii.com.</a>&nbsp;If you are not satisfied with our response you can contact&nbsp;<a href="http://watchdog.truste.com/pvr.php?page=complaint" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;" target="_blank">TRUSTe here.</a></p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Changes to our Privacy Policy</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We reserve the right to modify this Policy at any time without notice, so please review it frequently. The date at the top of this page indicates when this Policy was last revised. This Policy applies to all information that we have about you, and to your account with us.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">If we decide to change this Policy, we will post those changes to this privacy statement and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">If we make material changes to this Policy, we will notify you here, by email, or by means of a prominent notice when you log into the Site.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">The Information We Collect</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">When you visit Noomii, you provide us with two types of information: A) personal information (such as your name and email address) that you have voluntarily disclosed to us, and B) non-personally identifiable information that is generated when you interact with the Website.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">A) Personal Information</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">When you register with Noomii as a Coach, you provide us with your name, e-mail address, phone number, location (city, state, province), and other details about your coaching credentials and services. This information is collected for the purpose of being publically displayed on the website for prospective coaching clients to see. Apart for your email address, all of the information that you voluntarily submit to the website may be publically displayed.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We may occasionally use your name and email address to send you notifications regarding new services offered by Noomii that we think you may find valuable. We will send you our newsletter if you opt to receive this communication from Noomii. If you wish to unsubscribe from promotional emails including newsletters, you may do so by following the unsubscribe instructions included in each promotional email.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii may send you service-related announcements from time to time through the general operation of the service. For instance, if a prospective client sends you a consultation request, you will receive an email alerting you to that fact. Generally, you may opt out of such emails which are not promotional in nature by editing your notifications settings, though Noomii reserves the right to send you notices about your account even if you opt out of all voluntary e-mail notifications.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">When you use Noomii as a potential coaching client, you may request a consultation directly from one of the coaches listed in our directory, or participate in our coach referral program. If you request a consultation, we will ask for your name and email address to automatically send a one-time email to the coach on your behalf. We do not send you other emails unless you expressly agree to receive more information from Noomii. If you participate in the Noomii coach referral program (i.e. get a coach recommendation), we ask for your name, email address, phone number (optional), location, your goal for hiring a coach, and any additional details that pertain to your request for a coach. By default, we will publish your request (not including personally identifiable details) as a means for communicating to Coaches the value of our service and upon request, we can unpublish it. After submitting your referral request, we contact you to collect more information about your coaching needs, send you up to five recommended coaches, and periodically follow-up with you to ensure that we have delivered a high quality service. In all cases, Noomii stores personal information to track the success of our services.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Testimonials</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">From time to time, we post testimonials on our Website. We receive permission to post testimonials that include personally identifiable information prior to posting.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Surveys or Contests</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">From time to time, we may provide you the opportunity to participate in contests or surveys on our Website. If you participate, we may request certain personally identifiable information from you. Participation in these surveys or contests is completely voluntary and you therefore have a choice whether or not to disclose this information. The requested information typically includes contact information (such as name and shipping address), and demographic information (such as zip code).</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We use this information to notify contest winners and award prizes, to monitor site traffic or personalize the Website.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Service Providers</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We use third parties to provide various services on our behalf such as an email service provider and credit card processor. When you sign up to receive our newsletter we will share your email address with our email service provider. When you subscribe to a paid membership on the site, we use a credit card processor to store your credit card details in a secure location and to charge your credit card when payments are due.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Third parties are prohibited from using your personally identifiable information for any other purpose including their own marketing.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Legal Disclaimer</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our rights and/or to comply with a judicial proceeding, court order, or legal process served on our Website.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">B) Non-Personally Identifiable Information / Cookies</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">When you enter Noomii, we collect non-personally-identifiable information including IP address, profile information, aggregate user data, and browser type, from users and visitors to the Website. This data is used to manage the Website, track usage and improve the Website services. This non-personally-identifiable information may be shared with third parties to provide Noomii advertisements on other sites. For example, after visiting Noomii, you may see Noomii advertisements on YouTube. User IP addresses are recorded for security and monitoring purposes. In addition, we store certain information from your browser using &ldquo;cookies.&rdquo; A cookie is a piece of data stored on the user&rsquo;s computer tied to information about the user. We use session ID cookies to store visitors&rsquo; preferences and to record session information. These cookies terminate once the user closes the browser. You may be able to configure your browser to accept or reject all or some cookies, or notify you when a cookie is set &mdash; each browser is different, so check the &ldquo;Help&rdquo; menu of your browser to learn how to change your cookie preferences. Our cookies are not tied to personally identifiable information.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Please note that Noomii allows third-party advertisers that are presenting advertisements on some of our pages to set and access their cookies on your computer, and that by using Noomii, you consent to the setting and accessing of such cookies on your computer. Advertisers&rsquo; use of cookies is subject to their own privacy policies, not this Policy.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Third Party Advertising</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Ads appearing on this Website may be delivered to users by Noomii or one of our Web advertising partners. Our Web advertising partners may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement. In this way, ad servers may compile information about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to deliver targeted advertisements that they believe will be of most interest to you. This Policy covers the use of cookies by Noomii only and does not cover the use of cookies by any advertisers. We have no access or control of these third party cookies.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Clear Gifs (Web Beacons/Web Bugs)</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Our third party advertising partner employs a software technology called clear gifs (a.k.a. Web Beacons/Web Bugs), that help us better manage content on our site by informing us what content is effective. Clear gifs are tiny graphics with a unique identifier, similar in function to cookies, and are used to track the online movements of Web users. In contrast to cookies, which are stored on a users computer hard drive, clear gifs are embedded invisibly on Web pages and are about the size of the period at the end of this sentence. We do not tie the information gathered by clear gifs to our customers personally identifiable information.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Links to Other Sites</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">This Website contains links to other sites that are not owned or controlled by Noomii. Please be aware that we, Noomii, are not responsible for the privacy practices of such other sites.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">We encourage you to be aware when you leave our site and to read the privacy statements of each and every website that collects personally identifiable information.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">This privacy statement applies only to information collected by this Website.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Co-Branding</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Please note that when you use our Feedback Page and other Feedback features, you will be on a third party site. The pages will be branded as ours but powered by the third party. Please note that the third parties&rsquo; privacy policy governs the collection and use of information provided on the site powered by them. Their privacy policy link is included on each of the co-branded pages.</p>\r\n\r\n<h3 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; margin: 6px 0px; font-size: 24px;">Blog</h3>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">If you comment on posts published on the Noomii blog, you should be aware that any personally identifiable information you submit can be read, collected, or used by other users of this forum and could be used to send you unsolicited messages. We are not responsible for the personally identifiable information you choose to submit in this forum.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Changing or Removing Information</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Access and control over most personal information on Noomii is readily available through the profile editing tools. You may modify or delete any of your profile information at any time by logging into your account. Your Pro Coaches or coaching Partner may also modify most of your profile information at any time. Individuals who wish to deactivate their Noomii account may do so on the Account Settings page. Removed information may persist in backup copies for a reasonable period of time but will not be generally available. Where you make use of the communication features of the service to share information with your coaching Partner, however, (e.g., posting a comment on their goal) you generally cannot remove such communications. For their record keeping purposes, your Pro Coaches retain the ability to view coaching sessions previously scheduled with you, their notes associated with those sessions, and any other personal notes they may have written about you.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">Security</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Noomii takes appropriate precautions to protect our users&rsquo; information. Your account information is located on a secured server behind a firewall. When we collect sensitive information such as your credit card we use SSL encryption to protect this data. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security.</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Because email and instant messaging are not recognized as secure communications, we request that you not send private information to us by e-mail or instant messaging services.</p>\r\n\r\n<h2 style="box-sizing: border-box; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; line-height: 1.5; color: rgb(239, 119, 0); margin: 6px 0px;">How to Contact Us</h2>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">Should you have any questions or concerns about the security on our Website or these privacy policies, please email us at:</p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;"><a href="mailto:info@noomii.com" style="box-sizing: border-box; color: rgb(12, 117, 173); text-decoration: none; background: transparent;">info@noomii.com.</a></p>\r\n\r\n<p style="box-sizing: border-box; margin: 0px 0px 12px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">or contact us at the following address:</p>\r\n\r\n<address style="box-sizing: border-box; margin-bottom: 24px; font-style: normal; line-height: 24px; font-family: HelveticaNeue-Light, ''Helvetica Neue Light'', ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 16px;"><strong style="box-sizing: border-box;">PairCoach Enterprises Inc.</strong><br style="box-sizing: border-box;" />\r\n305 - 896 Cambie Street<br style="box-sizing: border-box;" />\r\nVancouver, BC, Canada<br style="box-sizing: border-box;" />\r\nV6B 2P6</address>\r\n', 'Privacy Policy', 'Privacy Policy', 'Privacy Policy', 1, '2014-12-20 14:51:30', '2014-12-20 10:51:30'),
(24, 'page', 'About Us', 'about-us', '<p>About us contents</p>\r\n', 'about us ', 'about us ', 'about,us', 1, '2014-12-25 12:21:26', '2014-12-25 12:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL,
  `status_code` enum('green','orange','red') NOT NULL,
  `sponser_id` int(11) NOT NULL COMMENT 'the person who is responsoble to manage this project on top phirarcy ',
  `type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_milestones`
--

CREATE TABLE IF NOT EXISTS `project_milestones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `milestone` varchar(255) NOT NULL,
  `date_completed` date NOT NULL,
  `status_color` enum('red','green','yellow') NOT NULL,
  `status` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `milestone` (`milestone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_milestone_history`
--

CREATE TABLE IF NOT EXISTS `project_milestone_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `milestone_id` int(11) NOT NULL,
  `milestone` varchar(255) NOT NULL,
  `date_completed` date NOT NULL,
  `status_color` enum('red','green','yellow') NOT NULL,
  `status` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `milestone_id` (`milestone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(100) NOT NULL,
  `report_description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `gm_email` varchar(100) NOT NULL,
  `recommnedation_email` varchar(255) NOT NULL,
  `contact_email` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `targets`
--

CREATE TABLE IF NOT EXISTS `targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goal_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_title` varchar(255) NOT NULL,
  `measure_title` varchar(100) NOT NULL,
  `target_pol` int(11) NOT NULL,
  `color_status` enum('green','orange','red') NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table is used for both targets and measures' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `targets_history`
--

CREATE TABLE IF NOT EXISTS `targets_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_title` varchar(255) NOT NULL,
  `measure_title` varchar(100) NOT NULL,
  `target_pol` int(11) NOT NULL,
  `color_status` enum('green','orange','red') NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table is used for both targets and measures' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, NULL, 0, 0, 0, NULL, NULL, NULL),
(2, 2, NULL, 0, 0, 0, NULL, NULL, NULL),
(3, 3, NULL, 0, 0, 0, NULL, NULL, NULL),
(4, 4, NULL, 0, 0, 0, NULL, NULL, NULL),
(5, 5, NULL, 0, 0, 0, NULL, NULL, NULL),
(6, 6, NULL, 0, 0, 0, NULL, NULL, NULL),
(7, 7, NULL, 0, 0, 0, NULL, NULL, NULL),
(8, 8, NULL, 0, 0, 0, NULL, NULL, NULL),
(9, 9, NULL, 0, 0, 0, NULL, NULL, NULL),
(10, 10, NULL, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dpt_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1 = App admin, 2 = Company Admin, 3 = Depart Admin, 4 = Normal User',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `dpt_id`, `email`, `designation`, `user_type`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `address`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin@admin.com', '', 0, '$2y$10$klOxy35UfDNMhqNsnV1fi.N6wwqpwgLLkD/C5HcHigYx1MAYKiGPe', NULL, 1, NULL, NULL, '2015-02-05 02:00:06', '$2y$10$abD.44yZoT8KPaHpztt0dugCBMls26AP2xOOlTnoGr9L0U0N.ZJRO', NULL, 'Nadeem', 'Akhtar', '', '2014-12-10 22:53:32', '2015-02-05 02:00:06'),
(3, 1, 'nadeemakhtar.se@gmail.com', '', 0, '$2y$10$nl20xt9iVM1S664VC/FxVuIPM4Y2gKEz7cSRtNBgZ8S62Q7PnAmZS', NULL, 0, '4bOLpSLeKVNQy2nNU77kuzepPi1xAIp0Gtr4BvPhe6', NULL, NULL, NULL, NULL, 'Nadeem', 'Akhtar', '', '2015-02-05 01:19:01', '2015-02-05 01:19:01');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(1, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_behaviors`
--

CREATE TABLE IF NOT EXISTS `user_behaviors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_to` int(11) NOT NULL COMMENT 'rating to a person',
  `rating_by` int(11) NOT NULL COMMENT 'rating by a person',
  `comment` varchar(255) NOT NULL,
  `color_code` varchar(100) NOT NULL,
  `access_type` int(11) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rating_to` (`rating_to`,`rating_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
